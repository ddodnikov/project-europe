<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><spring:message code="showallusers.title" /></title>
		
		<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
		<link href="<c:url value="/css/navigation.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/main.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/inner-page.css"></c:url>" rel="stylesheet" type="text/css"/>
		
		<style>	
			div.col-md-12 .row{
				padding:20px 25px;	
			}
			@media only screen and (max-device-width: 400px) {
				.content, .header, .navbar{
					width:400px;
				}
			}
		</style>
	</head>
	<body>
		<jsp:include page="simple_header.jsp"></jsp:include>
		<div class="row content">											
			<div class="col-md-12 no-top-margin">
			
				<div class="row" style="padding:0 25px;">
					<h2 class="left"><spring:message code="showallusers.form-title" /></h2>
					
					<div class="col-md-12" style="margin: 0px;padding:0 0">
						<label style="float:left;margin-top:33px;margin-bottom:21px">Users per page &nbsp</label>
						<c:url value="/showallusers" var="selectgetlink"></c:url>
						<select  id="selectUsers" style="float:left;width:100px;margin-top:33px;margin-bottom:21px">
							<c:choose>
								<c:when test="${usersPerPage == 15}">
									<option value="15" selected>15</option>
								</c:when>
								<c:otherwise>
									<option value="15">15</option>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${usersPerPage == 25}">
									<option value="25" selected>25</option>
								</c:when>
								<c:otherwise>
									<option value="25">25</option>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${usersPerPage == 50}">
									<option value="50" selected>50</option>
								</c:when>
								<c:otherwise>
									<option value="50">50</option>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${usersPerPage == 100}">
									<option value="100" selected>100</option>
								</c:when>
								<c:otherwise>
									<option value="100">100</option>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${usersPerPage == 0}">
									<option value="All" selected>All</option>
								</c:when>
								<c:otherwise>
									<option value="All">All</option>
								</c:otherwise>
							</c:choose>
						</select>
					
						<c:if test="${usersPerPage != 0 && usersPerPage < usersCount}">
							<nav aria-label="Page navigation" style="float:right">
								<ul class="pagination">
									<c:forEach items="${nextPrevPages.entrySet()}" begin="0" end="0" varStatus="loop" var="entry">
										<li id="idprev" class="page-item">
						    				<a class="page-link" href="<c:url value="/showallusers?page=${entry.getKey()}&show=${usersPerPage}"></c:url>" aria-label="Previous">
						        				<span aria-hidden="true">&laquo;</span>
						       					<span class="sr-only">Previous</span>
						      				</a>
						    			</li>										
								    </c:forEach>
								    
								    <c:forEach items="${pages.entrySet()}" begin="0" end="${pages.entrySet().size()-1}" step="1" varStatus="loop" var="entry">
								    	<c:choose>
											<c:when test="${entry.getValue() == true}">
												<li id="idpage${loop.index}" class="page-item active"><a class="page-link" href="<c:url value="/showallusers?page=${entry.getKey()}&show=${usersPerPage}"></c:url>">${entry.getKey()}</a></li>
											</c:when>
											<c:otherwise>
												<li id="idpage${loop.index}" class="page-item"><a class="page-link" href="<c:url value="/showallusers?page=${entry.getKey()}&show=${usersPerPage}"></c:url>">${entry.getKey()}</a></li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									
									<c:forEach items="${nextPrevPages.entrySet()}" begin="1" end="1" varStatus="loop" var="entry">    
										<li id="idnext" class="page-item">
							      			<a class="page-link" href="<c:url value="/showallusers?page=${entry.getKey()}&show=${usersPerPage}"></c:url>" aria-label="Next">
							        			<span aria-hidden="true">&raquo;</span>
							        			<span class="sr-only">Next</span>
							      			</a>
						    			</li>
									</c:forEach>
								</ul>
							</nav>
						</c:if>
					</div>
				</div>

				<div class="row" style="padding:0 25px;">
					<table class="table table-default">
						<thead>
							<tr>
								<th><spring:message code="showallusers.name" /></th>
								<th><spring:message code="showallusers.email" /></th>
								<th><spring:message code="showallusers.role" /></th>
							</tr>
						</thead>
						<tbody>

							<c:forEach items="${users}" var="user">	
								<c:if test="${user.role  == 'USER'}">
									<tr>
										<td>${user.name}</td>
										<td class="blue-font">${user.email}</td>	
										<td>${user.role}</td>
									</tr>
								</c:if>
							</c:forEach>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
		<script>
			var selectgetlinkurl = '${selectgetlink}';
		</script>
		<script src="<c:url value="/js/showallusers.js"></c:url>"></script>
	</body>
</html>