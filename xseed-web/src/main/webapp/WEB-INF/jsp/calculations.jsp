<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><spring:message code="calculations.title" /></title>
	
		<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
		<link href='css/navigation.css' rel="stylesheet" type="text/css"/>
		<link href='css/main.css' rel="stylesheet" type="text/css"/>
		<link href='css/inner-page.css' rel="stylesheet" type="text/css"/>
		<link href='css/calculations.css' rel="stylesheet" type="text/css"/>
	</head>
	<body>	
		<jsp:include page="simple_header.jsp"></jsp:include>				
		<div class="row content">											
			<div class="col-md-12">
				<div class="container">
					<div class="row">	
						<h2 class="h2"><spring:message code="calculations.title" /></h2>
						<div class="next">
							<a href="calculate"><spring:message code="calculations.new" /></a>
						</div>
						<br><br>
		
						<c:if test="${sessionScope.currentUser.role == 'ADMIN'}">
							<c:forEach items="${calculations}" var="calculation">
								<div class="row tablerow">
									<h4 class="h4"><div><a href="calculation-details?calculationid=${calculation.id}">${calculation.name}</a></div></h4>
									<table class="table table-default maintable">
										<thead>
											<tr class="nohover">
												<th><spring:message code="calculations.name" /></th>
												<th><spring:message code="calculations.date" /></th>
												<th><spring:message code="calculations.user" /></th>
												<th><spring:message code="calculations.number" /></th>
												<th class="detailhead"><spring:message code="calculations.details" /></th>
											</tr>
										</thead>
										<tbody>
											<tr class="nohover mainrow" >
												<td class="mainrowtd1" ><span>${calculation.name}</span></td>
												<td class="mainrowtd2" ><span>${calculation.date}</span></td>
												<td class="mainrowtd3" ><span>${sessionScope.currentUser.role} - ${sessionScope.currentUser.name}</span></td>
												<td class="mainrowtd4" ><span>${calculation.numberOfStartups}</span></td>
												<td rowspan="2" class="blue-font mainrowtd5" >
													<span><div style="margin-top:50%">
														<a href="calculation-details?calculationid=${calculation.id}"><spring:message code="calculations.details" /></a>
													</div></span>
												</td>
											</tr>
											<tr class="nohover tablerow">
												<td colspan="4" >
													<table class="table table-default innertable" >
														<thead>
															<tr class="nohover headrowrowtable">
																<td>T</td>
																<td>Tknown</td>
																<td>Tknown-straight</td>
																<td>Tknown-interaction</td>
																<td>Tunknown</td>
															</tr>
														</thead>
														<tbody>
															<tr class="nohover" >
																<td>${calculation.t}%</td>
																<td>${calculation.tk}%</td>
																<td>${calculation.tks}%</td>
																<td>${calculation.tki}%</td>
																<td>${calculation.tu}%</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</c:forEach>
						</c:if>				
					</div>
				</div>
				<c:if test="${calculationError != null}">
					<h4 style="color:red">${calculationError}</h4>
				</c:if>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>	
	</body>
</html>