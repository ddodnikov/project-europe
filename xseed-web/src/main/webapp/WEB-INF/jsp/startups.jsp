<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><spring:message code="startupuser.title"/></title>
		
		<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
		
		<link href="<c:url value="/css/navigation.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/main.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/inner-page.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/startup.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/startups.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/table-mobile.css"></c:url>" rel="stylesheet" type="text/css"/>
	
		<style>
		
		th, .td-value{
			word-break:break-all;
		}
		
		 button[type="submit"]{
		 	white-space: normal;
		 }
		/*Table design*/
		@media screen and (min-width:250px ) and (max-width:480px) {				
			tr td:nth-child(1) p:first-child:before {
				content: "Name:";
			}
			
			tr td:nth-child(2) p:first-child:before{
				content: "Email: ";
			}
			
			tr td:nth-child(3) p:first-child:before {
				content: "Website: ";
			}
			
			tr td:nth-child(4) p:first-child:before {
				content: "Phone: ";
			}
			
			tr td:nth-child(5) p:first-child:before {
				content: "Vat: ";
			}
			
			tr td:nth-child(6) p:first-child:before {
				content: "Country: ";
			}
			
			tr td:nth-child(7) p:first-child:before {
				content: "Current: ";
			}
			
			tr td:nth-child(8) p:first-child:before {
				content: "Default: ";
			}			
			
			 td label{
				margin-left:47%;
			}
			
			h2.left{
				text-align:center;
			}
		}
		
		</style>
	</head>
	<body>
		<jsp:include page="simple_header.jsp"></jsp:include>	
		<div class="row content">											
			<div class="col-md-12">
				<p class="message" style="display:none"></p>
				<div class="messagespace" style="height:30px;display:none" ></div>
				<div class="container containerstartups">				 
					<div class="row">
					
						<!-- Tabs -->
						<div>
							<div id="tabs" style="display: inline-block;">
								<ul class="nav nav-pills" style="align:center">
									<c:if test="${sessionScope.currentUser.membership.role == 'ADMIN'}">
										<!-- Startups -->
										<li class="active"><a href="startups" class="tablinks"><spring:message code="editstartup.startups-link"/></a></li>
									
										<!-- Edit Startup -->
									  	<li><a href="<c:url value="/editstartup"></c:url>" class="tablinks"><spring:message code="editstartup.edit-link"/></a></li>
									  	
									  	<!-- Add user -->
									  	<li><a href="<c:url value="/add-user-to-startup"></c:url>" class="tablinks"><spring:message code="editstartup.add-link"/></a></li>
									  	
									  	<!-- Show users -->
									  	<li><a href="<c:url value="/showusers"></c:url>" class="tablinks"><spring:message code="editstartup.show-link"/></a></li>
									
										<!-- Remove users -->
										<li><a href="<c:url value="/remove-user-from-startup"></c:url>" class="tablinks"><spring:message code="editstartup.remove-link"/></a></li>
									</c:if>
								</ul>
							</div>
						</div>
						
						<!-- Startups -->
						<div class="no-top-margin col-md-12">
							<h2 class="left"><spring:message code="startups.title"/></h2>
							
							<c:url value="/setstartups" var="link"></c:url>
							<form action="${link}" accept-charset="UTF-8" method="post">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								<table class="table table-default">
									<thead>
										<tr>
											<th><spring:message code="startups.name"/></th>
											<th><spring:message code="startups.email"/></th>
											<th><spring:message code="startups.website"/></th>
											<th><spring:message code="startups.phone"/></th>
											<th><spring:message code="startups.vat"/></th>
											<th><spring:message code="startups.country"/></th>
											<th><spring:message code="startups.current"/></th>
											<th><spring:message code="startups.default"/></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${userStartups}" var="startup">
											<tr>
												<td><p class="th-label-mobile"></p><p class="td-value">${startup.name}</p></td>
												<td class="blue-font"><p class="th-label-mobile"></p><p class="td-value">${startup.email}</p></td>
												<td class="blue-font"><p class="th-label-mobile"></p><p class="td-value">${startup.website}</p></td>
												<td class="blue-font"><p class="th-label-mobile"></p><p class="td-value">${startup.phone}</p></td>
												<td><p class="th-label-mobile"></p><p class="td-value">${startup.vat}</p></td>
												<td><p class="th-label-mobile"></p><p class="td-value">${startup.country.name}</p></td>
												<c:choose>
													<c:when test="${startup.id == currentStartup.id}">
														<td><p class="th-label-mobile"></p><label><input type="radio" name="current" checked="checked" value="${startup.id}"><span></span></label></td>
													</c:when>
													<c:otherwise>
														<td><p class="th-label-mobile"></p><label><input type="radio" name="current" value="${startup.id}"><span></span></label></td>
													</c:otherwise>
												</c:choose>
												<c:choose>
													<c:when test="${defaultStartup == startup.id}">
														<td><p class="th-label-mobile"></p><label><input type="radio" name="default" checked="checked" value="${startup.id}"><span></span></label></td>
													</c:when>
													<c:otherwise>
														<td><p class="th-label-mobile"></p><label><input type="radio" name="default" value="${startup.id}"><span></span></label></td>
													</c:otherwise>
												</c:choose>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<br>
								<div class="form-group">
							    	<div class="col-sm-offset-9 col-sm-3" style="padding:0">
							    		<button type="submit" class="btn btn-default" style="width:100%"><spring:message code="startups.save"/></button>
							    	</div>
							    </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
		<script>
			/*  $(window).resize(function(){
				if($(window).width() <= 990){
					document.getElementsByClassName('table')[0].className += " table-scroll";
				} else {
					document.getElementsByClassName('table')[0].className = "table table-default";
				}
			});  */
		</script>
		<c:if test="${message!=null}">
			<script>
				var message = ${message};
				var cssClass = "blue-font";
				$('.messagespace').css('display', 'block').fadeOut(4000);
				$('.message').text(message["message"]).css('display', 'inline').css('font-size','20px').addClass(cssClass).fadeOut(4000);
			</script>
		</c:if>
	</body>
</html>