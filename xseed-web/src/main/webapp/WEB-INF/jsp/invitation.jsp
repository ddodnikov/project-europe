<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><spring:message code="index.title"/></title>
		
		<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
		<link href="<c:url value="/css/main.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/index.css"></c:url>" rel="stylesheet" type="text/css"/>
	</head>
	<body>	
		<jsp:include page="index_header.jsp"></jsp:include>		
		<div class="row content">											
			<div class="col-md-12" style="margin-bottom:5%">
				<div class="row">
					<h2><spring:message code="invitation.question"/></h2><br>
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-3">
							<c:url value="/activation" var="link1"></c:url>
							<form method="GET" action="${link1}" id="activation-form">
								<input type="hidden" name="token" value="${token}">
								<input type="hidden" name="email" value="${email}">
								<input type="hidden" name="referer" value="${referer}">
							</form>
							<button type="submit" form="activation-form" class="btn btn-default" style="width:100%"><spring:message code="invitation.logout"/></button>
						</div>
						<div class="col-md-3">
							<c:url value="/home" var="link2"></c:url>
							<form method="GET" action="${link2}" id="home-form"></form>
							<button type="submit" form="home-form" class="btn btn-default" style="width:100%"><spring:message code="invitation.continue"/></button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</div>
		</div>		
		<jsp:include page="footer.jsp"></jsp:include>	
	</body>
</html>