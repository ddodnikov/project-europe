<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix='c' uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><spring:message code="startupuser.title"/></title>
		
		<link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
		
		<link href="<c:url value="/css/navigation.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/main.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/inner-page.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/startup.css"></c:url>" rel="stylesheet" type="text/css"/>
		<link href="<c:url value="/css/table-mobile.css"></c:url>" rel="stylesheet" type="text/css"/>
		
		<style>		
			div.col-md-12 .row{
				padding:0px 25px;	
			}
						
			@media screen and (max-width:370px){
				
				div.col-md-12 .row{
					padding:0px;	
				}
			}
			
			@media screen and (min-width:250px) and (max-width:480px){	
				tr td:nth-child(1) p:first-child:before {
					content: "Name:";
				}
				
				tr td:nth-child(2) p:first-child:before {
					content: "Email: ";
				}
				
				tr td:nth-child(3) p:first-child:before {
					content: "Role: ";
				}
			}
						
		</style>
	</head>
	<body>
		<jsp:include page="simple_header.jsp"></jsp:include>	
		<div class="row content">											
			<div class="col-md-12">
				<div class="container">				 
					<div class="row">
					
						<!-- Tabs -->
						<div>
							<div id="tabs" style="display: inline-block;">
								<ul class="nav nav-pills" style="align:center">
									<!-- Startups -->
									<li><a href="startups" class="tablinks"><spring:message code="editstartup.startups-link"/></a></li>

									<!-- Edit Startup -->
								  	<li><a href="<c:url value="/editstartup"></c:url>" class="tablinks"><spring:message code="editstartup.edit-link"/></a></li>
								  	
								  	<!-- Add user -->
								  	<li><a href="<c:url value="/add-user-to-startup"></c:url>" class="tablinks"><spring:message code="editstartup.add-link"/></a></li>
								  	
								  	<!-- Show users -->
								  	<li class="active"><a href="<c:url value="/showusers"></c:url>" class="tablinks"><spring:message code="editstartup.show-link"/></a></li>
								
									<!-- Remove users -->
									<li><a href="<c:url value="/remove-user-from-startup"></c:url>" class="tablinks"><spring:message code="editstartup.remove-link"/></a></li>
								</ul>
							</div>
						</div>
						
						<!-- Users -->
						<div class="no-top-margin col-md-12">
							<h2 class="left"><spring:message code="startupuser.form-title"/></h2>
						 	<table class="table table-default">
								<thead>
									<tr>
										<th><spring:message code="startupuser.name"/></th>
										<th><spring:message code="startupuser.email"/></th>
										<th><spring:message code="startupuser.role"/></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${sessionScope.currentStartup.users}" var="user">
									<tr>
										<td><p class="th-label-mobile"></p><p class="td-value">${user.name}</p></td>
										<td class="blue-font"><p class="th-label-mobile"></p><p class="td-value">${user.email}</p></td>
										<td><p class="th-label-mobile"></p><p class="td-value">${user.membership.role}</p></td>
									</tr>
									</c:forEach>
								</tbody>
							</table>		
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="footer.jsp"></jsp:include>
	</body>
</html>