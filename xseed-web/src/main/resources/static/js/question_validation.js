/**
 * 
 * Front-end question validation
 */

function getTextQuestionsByPage(page){
	var textQuestions = document.getElementById("content-" +page).getElementsByClassName("page" + page + "_TEXTquestion");
	return textQuestions;
}

function getSingleQuestionByPage(page) {
	var singleQuestions = document.getElementById("content-" +page).getElementsByClassName("page" + page + "_SINGLE_CHOICEquestion");
	return singleQuestions;
}

function getMultipleQuestionsByPage(page) {
	var multipleQuestions = document.getElementById("content-" +page).getElementsByClassName("page" + page + "_MULTIPLE_CHOICEquestion");
	return multipleQuestions;
}


//GLOBAL QUESTION ARRAY FOR ERRORS
var questionTitles = new Array();

function isPageFilled(page) {
	
	var noErrorOthers = document.getElementsByClassName("")
	 
	 var allQuestions = [];
	 var textQuestions = getTextQuestionsByPage(page);
	 var singleQuestions = getSingleQuestionByPage(page);
	 var multipleQuestions = getMultipleQuestionsByPage(page);
	 
	 allQuestions = allQuestions.concat([].slice.call(textQuestions));
	 allQuestions = allQuestions.concat([].slice.call(singleQuestions));
	 allQuestions = allQuestions.concat([].slice.call(multipleQuestions));

	 //if there is validation for text questions just uncomment this validation.
	 //if there is validation for text questions except feedback text questions 
	 //we should add new category in the question_calculations table in DB i.e. FEEDBACK
	 //validateTextQuestions(textQuestions);
	 validateSingleQuestions(singleQuestions, page);
	 validateMultipleQuestions(multipleQuestions, page);
	 
	 clearOldErrors(allQuestions);
	 
	 if(questionTitles.length != 0){
		 printErrors();
		 questionTitles = [];
		 return false;
	 }else{
		 return true;
	 }	
}

function goToNextStep() {
	document.getElementById("error-message-page1").style.display = 'none';
	var errors = document.getElementsByClassName("error-message-page2");
	for (var j = 0; j < errors.length; j++) {
		errors[j].style.display = 'none';
	}
}

function clearOldErrors(questionTitles){
	for(var i = 0; i<questionTitles.length; i++){
		questionTitles[i].getElementsByClassName('question-title')[0].style.color='#545e79';
	 }
}

function printErrors(){
	for(var i = 0; i<questionTitles.length; i++){
		questionTitles[i].getElementsByClassName('question-title')[0].style.color='red';
	 }
	
	//scroll
	questionTitles.sort(function(a,b) {
	    if( a === b) return 0;
	    if( !a.compareDocumentPosition) {
	        // support for IE8 and below
	        return a.sourceIndex - b.sourceIndex;
	    }
	    if( a.compareDocumentPosition(b) & 2) {
	        // b comes before a
	        return 1;
	    }
	    return -1;
	});
	
	$('html, body').animate({
		scrollTop: $(questionTitles[0].getElementsByClassName('question-title')[0]).offset().top-20
	}, 500);
}

function validateTextQuestions(questions){
	if(questions.length == 0){
		return;
	}else{
		for(var i = 0; i<questions.length; i++){
			var question = questions[i];
			var answers = question.getElementsByClassName(question.className.split("_")[0]+"_textanswer");
			for(var j = 0; j<answers.length; j++){
				if(answers[j].value == null || answers[j].value == ""){
					questionTitles.push(question);
				}
			}
		}
	}
}

function validateSingleQuestions(questions, page){
	if(questions.length == 0){
		return;
	}else{
		for(var i = 0; i<questions.length; i++){
			var question = questions[i];
			var answers = question.getElementsByClassName(question.className.split("_")[0]+"_singleanswer");
			
			var anySelected = false;
			var isOtherSelected = false;
			var otherAnswer = "";
			
			if(answers.length == 0){
				anySelected = true;
			}else{
				for(var j = 0; j<answers.length; j++){
					if(answers[j].checked){
						anySelected = true;
						var value = answers[j].parentElement.getElementsByTagName("div")[0].innerText.trim();
						var questionId = answers[j].name.split("_")[1];
						var id = "radio_" + questionId + "_other";
						var otherrer = document.getElementsByClassName("otherlenghterror_page" + page + "_question" + questionId)[0];
						if(otherrer != null){
							otherrer.style.display = 'none';
						}
						if(value == 'Other'){
							isOtherSelected = true;
							otherAnswer = document.getElementById(id).value;
							if(otherAnswer == null){
								anySelected = false;
								break;
							} else {
								var otherError = question.getElementsByClassName("otherlenghterror_page" + page + "_question" + questionId)[0];
								if(otherAnswer.length < 1 || otherAnswer.length > 100){
									otherError.style.display = 'block';
									console.log("other single on");
									anySelected = false;
									break;
								} else {
									otherError.style.display = 'none';
									console.log("other single on");
								}
							}
						}
						break;
					}
				}
			}
			
			if(!anySelected){
				questionTitles.push(question);
			}else{
				continue;
			}
		}
	}
}

function validateMultipleQuestions(questions, page){
	console.log("Validating multiple questions");
	if(questions.length == 0){
		return;
	}else{
		console.log("There are multiple questions");
		for(var i = 0; i<questions.length; i++){
			var question = questions[i];
			var answers = question.getElementsByClassName(question.className.split("_")[0]+"_multipleanswer");
			
			var anySelected = false;
			var isOtherSelected = false;
			var otherAnswer = "";
			
			for(var j = 0; j<answers.length; j++){
				var questionId = answers[j].name.split("_")[1];
				var otherrer = document.getElementsByClassName("otherlenghterror_page" + page + "_question" + questionId)[0];
				if(otherrer != null){
					otherrer.style.display = 'none';
				}
				if(answers[j].checked){
					console.log("Has a checked answer");
					anySelected = true;
					var value = answers[j].parentElement.getElementsByTagName("div")[0].innerText.trim();
					console.log("Value : " + value);
					var id = "multiple_" + questionId + "_other";
					if(value == 'Other'){
						console.log("other multi on on");
						isOtherSelected = true;
						otherAnswer = document.getElementById(id).value;
						if(otherAnswer == null){
							anySelected = false;
							break;
						} else {
							var otherError = question.getElementsByClassName("otherlenghterror_page" + page + "_question" + questionId)[0];
							if(otherAnswer.length < 1 || otherAnswer.length > 100){
								otherError.style.display = 'block';
								console.log("other multi on");
								anySelected = false;
								break;
							} else {
								otherError.style.display = 'none';
								console.log("other multi off");
							}
						}
					}
					
				}
			}
			
			if(!anySelected){
				questionTitles.push(question);
			}else{
				continue;
			}
			
		}
	}
	
}