package com.theoryx.xseed.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.csrf.DefaultCsrfToken;

public class BeforeAuthenticationFilter implements Filter{
	
	

	private static final String CSRF_TOKEN = "org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN";
	private static final String URIS = "login,editprofile,algoquestions,addadmin,calculate,updatenewuser";

	@Override
	public void destroy() {
		
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;
		if (needsToBeChecked(request)) {
			String formToken = request.getParameter("_csrf");
			DefaultCsrfToken actualCsrfToken = (DefaultCsrfToken) request.getSession()
					.getAttribute(CSRF_TOKEN);
			if (formToken != null && actualCsrfToken != null) {
				if (!formToken.equals(actualCsrfToken.getToken())) {
					response.sendRedirect("./login");
				} else {
					chain.doFilter(request, response);
				}
			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
		
	}

	private boolean needsToBeChecked(HttpServletRequest request){
		if(!request.getMethod().equalsIgnoreCase("post")){
			return false;
		}
		String uri = request.getRequestURI();
		String[] urislist = URIS.split(",");
		for (String string : urislist) {
			if(uri.contains(string)){
				return true;
			}
		}
		return false;
	}

}
