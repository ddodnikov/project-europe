package com.theoryx.xseed.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		System.out.println(authentication.getName());
		System.out.println("Custom Auth Success handler request email: " + request.getParameter("email"));
		//System.out.println("Custom Auth Success handler request password: " + request.getParameter("password"));
		response.sendRedirect("/xseed-web/home");
	}

}
