package com.theoryx.xseed.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		System.out.println("failure: " + exception.getMessage());
		System.out.println("Custom Auth Failure handler request email: " + request.getParameter("email"));
		//System.out.println("Custom Auth Failure handler request password: " + request.getParameter("password"));
		response.sendRedirect("/xseed-web/login?status=error");
	}

}
