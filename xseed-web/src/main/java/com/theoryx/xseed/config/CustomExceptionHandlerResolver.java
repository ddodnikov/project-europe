package com.theoryx.xseed.config;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

public class CustomExceptionHandlerResolver extends DefaultHandlerExceptionResolver{
	
	@Override
	protected ModelAndView handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
		
		System.out.println("Errorrrrr");
		
		return super.handleHttpRequestMethodNotSupported(ex, request, response, handler);
	}
}
