package com.theoryx.xseed.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class LoginInterceptor implements HandlerInterceptor {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("LoginInterceptor");
		System.out.println(request.getRequestURL());
		System.out.println(request.getMethod());
		if (request.getRequestURL().toString().contains("login")) {
			String formToken = request.getParameter("_csrf");
			System.out.println(formToken);
			DefaultCsrfToken actualCsrfToken = (DefaultCsrfToken) request.getSession()
					.getAttribute("org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN");
			if (formToken != null && actualCsrfToken != null) {
				System.out.println(actualCsrfToken.getToken());
				if (!formToken.equals(actualCsrfToken.getToken())) {
					response.sendRedirect("/login");
					return false;
				}
			}
		} else {
			System.out.println("nottt");
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
}
