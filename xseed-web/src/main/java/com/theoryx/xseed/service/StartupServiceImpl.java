package com.theoryx.xseed.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.theoryx.xseed.dto.CountryDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.Country;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.repository.StartupRepository;
import com.theoryx.xseed.utils.ValidationUtils;

@Service
public class StartupServiceImpl implements StartupService {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private StartupRepository startupRepository;
	@Autowired
	private CountryService countryService;
	@Autowired
	private UserService userService;
	@Autowired
	private MembershipService membershipService;
	@Autowired
	private MailService mailService;

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public Startup createStartup(StartupDTO startupDto) {
		Startup startup = convertStartupDTOToStartup(startupDto);
		startup = startupRepository.save(startup);
		if (startup != null) {
			return startup;
		} else {
			return null;
		}
	}

	@Override
	public Startup convertStartupDTOToStartup(StartupDTO startupDto) {
		if (startupDto != null) {
			Integer id = startupDto.getId();
			String name = startupDto.getName();
			String email = startupDto.getEmail();
			String vat = startupDto.getVat();
			String website = startupDto.getWebsite();
			String phone = startupDto.getPhone();
			CountryDTO country = startupDto.getCountry();

			Startup startup = new Startup(name, email, phone, website, vat);
			Country c = null;
			if (country != null) {
				c = countryService.convertCountryDTOToCountry(country);
			}
			startup.setCountry(c);
			startup.setId(id);
			return startup;
		} else {
			return null;
		}
	}

	@Override
	public StartupDTO convertStartupToStartupDTO(Startup startup) {
		if (startup != null) {
			Integer id = startup.getId();
			String name = startup.getName();
			String email = startup.getEmail();
			String vat = startup.getVat();
			String website = startup.getWebsite();
			String phone = startup.getPhone();
			Country country = startup.getCountry();

			List<User> users = startup.getUsers();
			List<UserDTO> userdtos = null;
			if (users != null) {
				userdtos = new ArrayList<UserDTO>();
				for (User user : users) {
					UserDTO dto = userService.getUserDTOfromUser(user);
					Membership mem = membershipService.getMembershipByUserIdAndStartupId(user.getId(), startup.getId());
					dto.setMembership(membershipService.convertMembershipToMembershipDTO(mem));
					userdtos.add(dto);
				}
			}
			if (userdtos != null && !userdtos.isEmpty()) {
				for (int i = 0; i < userdtos.size(); i++) {
					UserDTO dto = userdtos.get(i);
					if (dto.getMembership().getRole().toString().equals("ADMIN")) {
						userdtos.remove(i);
						userdtos.add(0, dto);
					}
				}
			}
			StartupDTO startupDTO = new StartupDTO(name, email, phone, website, vat);
			startupDTO.setCountry(countryService.convertCountryToCountryDTO(country));
			startupDTO.setId(id);
			startupDTO.setUsers(userdtos);
			return startupDTO;
		} else {
			return null;
		}
	}

	@Override
	public List<Error> validateStartupInfo(StartupDTO startupDTO) {
		List<Error> errors = new LinkedList<Error>();
		if (startupDTO == null) {
			errors.add(new Error(messageSource.getMessage("error.null-startup", null, Locale.getDefault())));
			return errors;
		}
		String name = startupDTO.getName();
		String email = startupDTO.getEmail();
		String phone = startupDTO.getPhone();
		String website = startupDTO.getWebsite();
		String vat = startupDTO.getVat();

		if (name != null) {
			name = name.trim();
			if (!ValidationUtils.isValidStartupName(name)) {
				errors.add(
						new Error(messageSource.getMessage("error.invalid-startup-name", null, Locale.getDefault())));
			}
		} else {
			errors.add(new Error(messageSource.getMessage("error.invalid-startup-name", null, Locale.getDefault())));
		}
		if (email != null) {
			email = email.trim();
			if (!ValidationUtils.isValidStartupEmail(email)) {
				errors.add(
						new Error(messageSource.getMessage("error.invalid-startup-email", null, Locale.getDefault())));
			}
		}
		if (phone != null) {
			phone = phone.trim();
			if (!ValidationUtils.isValidStartupPhone(phone)) {
				errors.add(
						new Error(messageSource.getMessage("error.invalid-startup-phone", null, Locale.getDefault())));
			}
		}
		if (website != null) {
			website = website.trim();
			if (!ValidationUtils.isValidStartupWebsite(website)) {
				errors.add(new Error(
						messageSource.getMessage("error.invalid-startup-website", null, Locale.getDefault())));
			}
		}
		if (vat != null) {
			vat = vat.trim();
			if (!ValidationUtils.isValidStartupVAT(vat)) {
				errors.add(new Error(messageSource.getMessage("error.invalid-startup-vat", null, Locale.getDefault())));
			}
		}
		return errors;
	}

	@Override
	public Map<StartupDTO, List<Error>> normalizeStartup(StartupDTO startupDTO) {
		List<Error> errors = new LinkedList<Error>();
		if (startupDTO == null) {
			errors.add(new Error(messageSource.getMessage("error.null-startup", null, Locale.getDefault())));
		} else {
			CountryDTO countryDTO = startupDTO.getCountry();
			if (countryDTO == null) {
				errors.add(new Error(messageSource.getMessage("error.null-country", null, Locale.getDefault())));
			} else {
				String name = startupDTO.getName();
				String email = startupDTO.getEmail();
				String phone = startupDTO.getPhone();
				String website = startupDTO.getWebsite();
				String vat = startupDTO.getVat();
				if (name != null) {
					name = name.trim();
				}
				if (email != null) {
					email = email.trim();
				}
				if (phone != null) {
					phone = phone.trim();
				}
				if (website != null) {
					website = website.trim();
				}
				if (vat != null) {
					vat = vat.trim();
				}
				startupDTO.setName(name);
				startupDTO.setEmail(email);
				startupDTO.setPhone(phone);
				startupDTO.setWebsite(website);
				startupDTO.setVat(vat);

				if (countryDTO.getName() == null) {
					errors.add(
							new Error(messageSource.getMessage("error.null-country-name", null, Locale.getDefault())));
				} else {
					if (countryDTO.getName().equals("0")) {
						startupDTO.setCountry(null);
					} else {
						countryDTO = countryService.getCountryDTOByName(countryDTO);
						if (countryDTO == null) {
							errors.add(new Error(
									messageSource.getMessage("error.null-country", null, Locale.getDefault())));
						} else {
							startupDTO.setCountry(countryDTO);
						}
					}
				}
			}
		}

		Map<StartupDTO, List<Error>> result = new HashMap<StartupDTO, List<Error>>();
		result.put(startupDTO, errors);
		return result;
	}

	@Override
	public Map<StartupDTO, List<Error>> updateStartup(StartupDTO startupDTO) {
		Map<StartupDTO, List<Error>> result = new HashMap<StartupDTO, List<Error>>();
		List<Error> errors = new LinkedList<Error>();
		if (startupDTO != null) {
			String name = startupDTO.getName();
			String email = startupDTO.getEmail();
			String phone = startupDTO.getPhone();
			String website = startupDTO.getWebsite();
			String vat = startupDTO.getVat();
			Startup startup = startupRepository.findOne(startupDTO.getId());
			if (startup != null) {
				startup.setName(name);
				startup.setEmail(email);
				startup.setPhone(phone);
				startup.setWebsite(website);
				startup.setVat(vat);
				startup.setCountry(countryService.convertCountryDTOToCountry(startupDTO.getCountry()));
				Startup saved = startupRepository.save(startup);
				StartupDTO startupDTOSaved = convertStartupToStartupDTO(saved);
				result.put(startupDTOSaved, errors);
			} else {
				errors.add(new Error(messageSource.getMessage("error.update-startup", null, Locale.getDefault())));
				result.put(null, errors);
			}
		} else {
			errors.add(new Error(messageSource.getMessage("error.null-startup", null, Locale.getDefault())));
			result.put(null, errors);
		}
		return result;
	}

	@Override
	public Map<UserDTO, List<Error>> addUser(User user, Startup startup) {
		List<Error> errors = new LinkedList<Error>();
		UserDTO userDTO = null;
		if (user == null) {
			errors.add(new Error(messageSource.getMessage("error.creating-user", null, Locale.getDefault())));
		} else {
			if (startup == null) {
				errors.add(new Error(messageSource.getMessage("error.no-such-startup", null, Locale.getDefault())));
			} else {
				Membership membership = membershipService.createMembership(user, startup);
				if (membership == null) {
					errors.add(new Error(
							messageSource.getMessage("error.creating-membership", null, Locale.getDefault())));
				} else {
					List<Membership> memberships = new LinkedList<Membership>();
					if (user.getMemberships() != null) {
						memberships = user.getMemberships();
					}
					memberships.add(membership);
					user.setMemberships(memberships);
					userDTO = userService.convertUserToUserDTO(user);
					if (userDTO != null) {
						userDTO.setMembership(membershipService.convertMembershipToMembershipDTO(membership));
					}
				}
			}
		}
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(userDTO, errors);
		return result;
	}
	
	public Map<UserDTO, List<Error>> addUserToStartup(UserDTO userDTO, StartupDTO startupDTO){
		List<Error> errors = new LinkedList<Error>();
		if (userDTO == null) {
			errors.add(new Error(messageSource.getMessage("error.creating-user", null, Locale.getDefault())));
		} else {
			if (startupDTO == null) {
				errors.add(new Error(messageSource.getMessage("error.no-such-startup", null, Locale.getDefault())));
			} else {
				User user = userService.convertUserDTOToUser(userDTO);
				Startup startup = convertStartupDTOToStartup(startupDTO);
				Membership membership = membershipService.createMembership(user, startup);
				if (membership == null) {
					errors.add(new Error(
							messageSource.getMessage("error.creating-membership", null, Locale.getDefault())));
				} else {
					List<Membership> memberships = new LinkedList<Membership>();
					if (user.getMemberships() != null) {
						memberships = user.getMemberships();
					}
					memberships.add(membership);
					user.setMemberships(memberships);
					userDTO = userService.convertUserToUserDTO(user);
					if (userDTO != null) {
						userDTO.setMembership(membershipService.convertMembershipToMembershipDTO(membership));
					}
				}
			}
		}
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(userDTO, errors);
		return result;
	}

	@Override
	public List<Startup> getAllStartups() {
		return (List<Startup>) startupRepository.findAll();
	}

	@Override
	public Integer getAllStartupCount() {
		return (int) startupRepository.count();
	}

	@Override
	public List<Startup> getPaginationStartups(Integer limit, Integer offset) {
		return (List<Startup>) startupRepository.findPaginationStartups(limit, offset);
	}

	@Override
	public void deleteStartup(Integer id) {
		startupRepository.delete(id);
	}

	@Override
	public Startup findByName(String name) {
		return startupRepository.findByName(name);
	}

	@Override
	public List<StartupDTO> convert(List<Startup> startups) {
		if (startups == null) {
			return null;
		}
		List<StartupDTO> dtos = new ArrayList<StartupDTO>();
		for (Startup startup : startups) {
			StartupDTO dto = convertStartupToStartupDTO(startup);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public Startup findById(Integer defaults) {
		return startupRepository.findOne(defaults);
	}
	
	@Override
	public boolean startupNameExists(String name) {
		if(startupRepository.findByName(name) == null){
			return false;
		}else{
			return true;
		}
	}
	
	@Override
	public boolean isCurrentStartupName(String name, StartupDTO currentStartup){
		if(currentStartup.getName().equals(name)){
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void addExistingActive(StartupDTO startupDTO, UserDTO currentUserDTO, UserDTO existingUserDTO,
			List<Error> errors, String email) {
		if(startupDTO.getUsers().contains(existingUserDTO)){
			errors.add(new Error(messageSource.getMessage("error.user-already-added", new Object[0],
					Locale.getDefault())));
		} else {
			Map<UserDTO, List<Error>> result = addUserToStartup(existingUserDTO, startupDTO);
			if (result.containsKey(null)) {
				errors = result.get(null);
			} else {
				// get added user
				UserDTO userDTO = (UserDTO) result.keySet().toArray()[0];
				// get admin's name
				String adminName = currentUserDTO.getName();
				// send notification
				boolean mailResult = mailService.sendNotificationEmailForStartup(MailService.FROM,
						email, MailService.NOTIFICATION_SUBJECT, adminName, startupDTO.getName());
				// mail was sent successfully
				if (mailResult) {
					currentUserDTO.getStartup().getUsers().add(userDTO);
				} else {
					membershipService.deleteMembership(userDTO.getMembership());
					errors.add(new Error(messageSource.getMessage("error.mail-error", new Object[0], Locale.getDefault())));
				}
			}
		}
	}

	@Override
	public void addExistingInactive(StartupDTO startupDTO, UserDTO currentUserDTO, UserDTO existingUserDTO,
			List<Error> errors, String email) {
		if(startupDTO.getUsers().contains(existingUserDTO)){
			errors.add(new Error(messageSource.getMessage("error.user-already-added", new Object[0],
					Locale.getDefault())));
		} else {
			//add to the startup and send activation link
			Map<UserDTO, List<Error>> result = addUserToStartup(existingUserDTO, startupDTO);
			if (result.containsKey(null)) {
				errors = result.get(null);
			} else {
				//send new activation email
				existingUserDTO = (UserDTO) result.keySet().toArray()[0];
				existingUserDTO.setActive(true);
				String token = userService.generateActivationToken(existingUserDTO);
				boolean mailResult = mailService.sendInvitationLinkForStartup(MailService.FROM, email,
						MailService.STARTUP_INVITATION_SUBJECT, token, startupDTO.getName());
				if (mailResult) {
					currentUserDTO.getStartup().getUsers().add(existingUserDTO);
				} else {
					membershipService.deleteMembership(existingUserDTO.getMembership());
					userService.deleteUser(existingUserDTO);
					errors.add(new Error(
							messageSource.getMessage("error.mail-error", new Object[0], Locale.getDefault())));
				}
			}
		}
	}

	@Override
	public boolean addNonExistigUser(StartupDTO startupDTO, UserDTO currentUserDTO, List<Error> errors, String email) {
		UserDTO userDTO = userService.createDummyDTO(email);
		userDTO.setStartup(startupDTO);
		//save user to db
		Map<UserDTO, List<Error>> result = addUserToStartup(userDTO, startupDTO);
		if (result.containsKey(null)) {
			errors = result.get(null);
			return false;
		} else {
			userDTO = (UserDTO) result.keySet().toArray()[0];
			String token = userService.generateActivationToken(userDTO);
			boolean mailResult = mailService.sendInvitationLinkForStartup(MailService.FROM, email,
					MailService.STARTUP_INVITATION_SUBJECT, token, startupDTO.getName());
			if (mailResult) {
				currentUserDTO.getStartup().getUsers().add(userDTO);
				return true;
			} else {
				membershipService.deleteMembership(userDTO.getMembership());
				userService.deleteUser(userDTO);
				errors.add(new Error(
						messageSource.getMessage("error.mail-error", new Object[0], Locale.getDefault())));
				return false;
			}
		}
	}
}
