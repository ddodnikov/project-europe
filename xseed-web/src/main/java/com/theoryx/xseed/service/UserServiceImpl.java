package com.theoryx.xseed.service;

import java.util.ArrayList;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.theoryx.xseed.dto.MembershipDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.repository.UserRepository;
import com.theoryx.xseed.utils.ValidationUtils;

@Service
public class UserServiceImpl implements UserService {

	private static final String KEY = "Th30RyXx533D"; // THEORYXXSEED
	private static final String PROTECTION = "Pr0J3cT3uR0p3"; // PROJECTEUROPE
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private StartupService startupService;
	@Autowired
	private MembershipService membershipService;

	private static final String EMPTY_NAME = "";
	private static final String DUMMY_PASSWORD = "DummyPassword12Th30RyX";
	private static final String DUMMY_PASS_CONVERT = "DummyPass";

	@Override
	public User createUser(UserDTO user) {
		if (user != null) {
			User userModel = convertUserDTOToUser(user);
			if (userModel != null) {
				userModel.setIsActive(user.isActive());
				userModel.setRole(user.getRole());
			}
			userModel = userRepository.save(userModel);
			if (userModel != null) {
				return userModel;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public UserDTO createUserDTO(UserDTO user) {
		User u = createUser(user);
		if (u != null) {
			user.setId(u.getId());
		}
		return user;
	}

	@Override
	public List<Error> validateRegisterInput(UserDTO user) {
		if (user != null) {
			String name = "";
			if (user.getName() != null) {
				name = user.getName().trim();
			}
			String email = "";
			if (user.getEmail() != null) {
				email = user.getEmail().trim();
			}
			String password = "";
			if (user.getPassword() != null) {
				password = user.getPassword().trim();
			}
			String confirmationPassword = "";
			if (user.getConfirmationPassword() != null) {
				confirmationPassword = user.getConfirmationPassword().trim();
			}
			String startupName = "";
			if (user.getStartup() != null && user.getStartup().getName() != null) {
				startupName = user.getStartup().getName().trim();
			}

			List<Error> errors = new LinkedList<Error>();
			if (!ValidationUtils.validateUserEmail(email)) {
				errors.add(new Error(messageSource.getMessage("error.invalid-email", null, Locale.getDefault())));
			}

			if (ifExist(email)) {
				errors.add(new Error(messageSource.getMessage("error.user-exist", null, Locale.getDefault())));
			}

			if (!ValidationUtils.isValidUserName(name)) {
				errors.add(new Error(messageSource.getMessage("error.invalid-username", null, Locale.getDefault())));
			}
			if (!ValidationUtils.isValidUserPassword(password, confirmationPassword)) {
				errors.add(new Error(messageSource.getMessage("error.invalid-password", null, Locale.getDefault())));
			}
			if (!ValidationUtils.isValidStartupName(startupName)) {
				errors.add(
						new Error(messageSource.getMessage("error.invalid-startup-name", null, Locale.getDefault())));
			}
			if (startupService.startupNameExists(startupName)) {
				errors.add(new Error(messageSource.getMessage("error.exists-startup-name", null, Locale.getDefault())));
			}
			return errors;
		} else {
			return null;
		}
	}

	@Override
	public UserDTO updateUserInfo(UserDTO editedUser, UserDTO currentUser, User user) {
		String name = "";
		String password = "";
		if (editedUser != null) {
			if (editedUser.getName() != null) {
				name = editedUser.getName().trim();
			}
			if (editedUser.getPassword() != null) {
				password = editedUser.getPassword().trim();
			}
		}
		if (!name.isEmpty()) {
			user.setName(name);
			currentUser.setName(name);
		}
		if (!password.isEmpty()) {
			user.setHashedPassword(new BCryptPasswordEncoder().encode(password));
		}

		user = userRepository.save(user);

		if (user != null) {
			return currentUser;
		} else {
			return null;
		}
	}

	public List<Error> validateNewUserProfile(UserDTO user) {
		List<Error> errors = new ArrayList<Error>();
		if (user != null) {
			String name = "";
			if (user.getName() != null) {
				name = user.getName().trim();
			}
			String password = "";
			if (user.getPassword() != null) {
				password = user.getPassword().trim();
			}
			String confirmationPassword = "";
			if (user.getConfirmationPassword() != null) {
				confirmationPassword = user.getConfirmationPassword().trim();
			}
			if (name.isEmpty() || password.isEmpty() || confirmationPassword.isEmpty()) {
				errors.add(new Error(messageSource.getMessage("error.no-input", null, Locale.getDefault())));
				return errors;
			} else {
				if (!ValidationUtils.isValidUserName(name)) {
					errors.add(
							new Error(messageSource.getMessage("error.invalid-username", null, Locale.getDefault())));
				}

				if (!ValidationUtils.isValidUserPassword(password, confirmationPassword)) {
					errors.add(
							new Error(messageSource.getMessage("error.invalid-password", null, Locale.getDefault())));
				}
			}
		} else {
			errors.add(new Error(messageSource.getMessage("error.no-input", null, Locale.getDefault())));
		}
		return errors;
	}

	@Override
	public List<Error> validateEditProfileInput(UserDTO user) {
		List<Error> errors = new ArrayList<Error>();
		if (user != null) {
			String name = "";
			if (user.getName() != null) {
				name = user.getName().trim();
			}
			String password = "";
			if (user.getPassword() != null) {
				password = user.getPassword().trim();
			}
			String confirmationPassword = "";
			if (user.getConfirmationPassword() != null) {
				confirmationPassword = user.getConfirmationPassword().trim();
			}
			if (name.isEmpty() && password.isEmpty() && confirmationPassword.isEmpty()) {
				errors.add(new Error(messageSource.getMessage("error.no-input", null, Locale.getDefault())));
				return errors;
			}
			if (name.isEmpty() || !ValidationUtils.isValidUserName(name)) {
				errors.add(new Error(messageSource.getMessage("error.invalid-username", null, Locale.getDefault())));
			}
			if ((!password.isEmpty() || !confirmationPassword.isEmpty())
					&& !ValidationUtils.isValidUserPassword(password, confirmationPassword)) {
				errors.add(new Error(messageSource.getMessage("error.invalid-password", null, Locale.getDefault())));
			}
		} else {
			errors.add(new Error(messageSource.getMessage("error.no-input", null, Locale.getDefault())));
		}
		return errors;
	}

	@Override
	public User convertUserDTOToUser(UserDTO userDto) {
		if (userDto != null) {
			Integer id = null;
			if (userDto.getId() != null) {
				id = userDto.getId();
			}
			String name = null;
			if (userDto.getName() != null) {
				name = userDto.getName();
			}
			String email = null;
			if (userDto.getEmail() != null) {
				email = userDto.getEmail();
			}
			String password = null;
			String hashedPassword = null;
			if (userDto.getPassword() != null) {
				password = userDto.getPassword();
				hashedPassword = new BCryptPasswordEncoder().encode(password);
			}
			User user = new User(name, email, hashedPassword);
			user.setId(id);
			return user;
		} else {
			return null;
		}
	}

	@Override
	public UserDTO getUserDTOfromUser(User user) {
		if (user != null) {
			String name = user.getName();
			String email = user.getEmail();
			UserRole role = user.getRole();
			Integer id = user.getId();

			UserDTO userDto = new UserDTO();
			userDto.setId(id);
			userDto.setRole(role);
			userDto.setEmail(email);
			userDto.setName(name);
			userDto.setPassword(DUMMY_PASS_CONVERT);
			userDto.setConfirmationPassword(DUMMY_PASS_CONVERT);

			return userDto;
		} else {
			return null;
		}
	}

	@Override
	public UserDTO convertUserToUserDTO(User user) {
		if (user == null) {
			return null;
		}
		UserDTO userDto = getUserDTOfromUser(user);
		Startup startup = null;
		List<Membership> memberships = user.getMemberships();
		if (memberships != null && !memberships.isEmpty()) {
			for (Membership membership : memberships) {
				if (membership.isDefault()) {
					startup = membership.getStartup();
					MembershipDTO dto = membershipService.convertMembershipToMembershipDTO(membership);
					userDto.setMembership(dto);
				}
			}
			if (startup == null) {
				Membership mem = user.getMemberships().get(0);
				startup = mem.getStartup();
				MembershipDTO dto = membershipService.convertMembershipToMembershipDTO(mem);
				userDto.setMembership(dto);
			}
		}
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startup);
		userDto.setStartup(currentStartup);
		return userDto;
	}

	@Override
	public User getByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public Map<UserDTO, List<Error>> getUserById(Integer id) {
		User userModel = userRepository.findOne(id);
		List<Error> errors = new LinkedList<Error>();
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		if (userModel != null) {
			UserDTO user = new UserDTO();
			user.setId(userModel.getId());
			user.setEmail(userModel.getEmail());
			user.setName(userModel.getName());
			user.setPassword(userModel.getHashedPassword());
			user.setConfirmationPassword(userModel.getHashedPassword());
			user.setRole(user.getRole());
			result.put(user, errors);
		} else {
			errors.add(new Error(messageSource.getMessage("error.no-such-user", null, Locale.getDefault())));
			result.put(null, errors);
		}
		return result;
	}

	@Override
	public Map<UserDTO, List<Error>> getUserByEmail(String email) {
		List<Error> errors = new LinkedList<Error>();
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		if (ValidationUtils.validateUserEmail(email)) {
			User user = userRepository.findByEmail(email);
			if (user != null) {
				UserDTO userDTO = new UserDTO();
				userDTO.setId(user.getId());
				userDTO.setEmail(user.getEmail());
				userDTO.setName(user.getName());
				userDTO.setPassword(user.getHashedPassword());
				userDTO.setConfirmationPassword(user.getHashedPassword());
				userDTO.setRole(user.getRole());
				userDTO.setActive(user.getIsActive());
				// TODO get specific startup and membership
				if (user.getRole() != null && user.getRole().equals(UserRole.USER)) {
					if (user.getStartups() != null && !user.getStartups().isEmpty()) {
						userDTO.setStartup(startupService.convertStartupToStartupDTO(user.getStartups().get(0)));
					}

					if (user.getMemberships() != null && !user.getMemberships().isEmpty()) {
						userDTO.setMembership(
								membershipService.convertMembershipToMembershipDTO(user.getMemberships().get(0)));
					}
				} else {

				}

				result.put(userDTO, errors);
			} else {
				errors.add(new Error(messageSource.getMessage("error.no-such-user", null, Locale.getDefault())));
				result.put(null, errors);
			}
		} else {
			errors.add(new Error(messageSource.getMessage("error.invalid-email", null, Locale.getDefault())));
			result.put(null, errors);
		}
		return result;
	}

	@Override
	public UserDTO getCurrentUserDTO() {
		User currentUser = getCurrentUser();
		UserDTO currentUserDto = convertUserToUserDTO(currentUser);
		if (currentUserDto != null) {
			return currentUserDto;
		} else {
			return null;
		}
	}

	@Override
	public User getCurrentUser() {
		if (SecurityContextHolder.getContext().getAuthentication().getPrincipal().getClass().getSimpleName()
				.equals("String")) {
			User user = userRepository
					.findByEmail((String) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
			return user;
		}
		org.springframework.security.core.userdetails.User springUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		String email = springUser.getUsername();
		User user = userRepository.findByEmail(email);
		return user;
	}

	// used in createNewPassword and updateNewUserInfo methods in UserController
	@Override
	public boolean updateUser(UserDTO userDTO) {
		if (userDTO != null) {
			String password = userDTO.getPassword();
			String confirmationPassword = userDTO.getConfirmationPassword();
			if (ValidationUtils.isValidUserPassword(password, confirmationPassword)) {
				String hashedPassword = new BCryptPasswordEncoder().encode(password);
				userDTO.setPassword(hashedPassword);
				userDTO.setConfirmationPassword(hashedPassword);
				User user = userRepository.findByEmail(userDTO.getEmail());
				user.setName(userDTO.getName());
				user.setHashedPassword(userDTO.getPassword());
				user.setIsActive(userDTO.isActive());
				userRepository.save(user);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public String generatePasswordToken(UserDTO user) {
		if (user != null) {
			String email = user.getEmail();
			String name = user.getName();
			LocalDate d = LocalDate.now();
			String token = email + KEY + name + KEY + d.getDayOfMonth() + "-" + d.getMonthValue() + "-"
					+ d.getDayOfYear();
			String hash = DigestUtils.md5Hex(token);
			return hash;
		} else {
			return null;
		}
	}

	public String generateActivationToken(UserDTO user) {
		if (user != null) {
			String email = user.getEmail();
			LocalDate d = LocalDate.now();
			String token = email + KEY + d.getDayOfMonth() + "-" + d.getMonthValue() + "-" + d.getDayOfYear() + KEY;
			String hash = DigestUtils.md5Hex(token);
			return hash;
		} else {
			return null;
		}
	}

	@Override
	public boolean ifExist(String email) {
		User user = userRepository.findByEmail(email);
		if (user == null) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public List<User> getAllUsersInDB() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public List<User> getAllUsers() {
		return (List<User>) userRepository.findByRole(UserRole.USER);
	}

	@Override
	public List<User> getPaginationUsers(Integer limit, Integer offset) {
		return (List<User>) userRepository.findPaginationUsers(limit, offset);
	}

	@Override
	public boolean deleteUser(UserDTO user) {
		if (user != null && user.getId() != null) {
			userRepository.delete(user.getId());
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void deleteUserByEmail(String email) {
		userRepository.delete(userRepository.findByEmail(email).getId());
	}

	@Override
	public void deleteUserById(Integer id) {
		userRepository.delete(id);
	}

	@Override
	public List<User> getAllAdmins() {
		List<User> admins = userRepository.findByRole(UserRole.ADMIN);
		return admins;
	}

	@Override
	public void authenticateUserAndInitializeSessionByUsername(String username, UserDetailsService userDetailsManager) {
		try {
			// System.out.println("Start authentication");
			UserDetails user = userDetailsManager.loadUserByUsername(username);
			UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, user.getPassword(),
					user.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(auth);
			// System.out.println("End authentication");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public Integer getAllUsersCount() {
		return (int) userRepository.count();
	}

	@Override
	public Integer getUsersCount() {
		return userRepository.findUsersCount();
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	public UserDTO createDummyDTO(String email) {
		UserDTO userDTO = new UserDTO(EMPTY_NAME, email, DUMMY_PASSWORD, DUMMY_PASSWORD, false, UserRole.USER);
		userDTO = createUserDTO(userDTO);
		return userDTO;
	}

	@Override
	public String generateProtectionToken(String email) {
		String toEncode = PROTECTION + "-" + email + "-" + PROTECTION;
		String encoded = DigestUtils.md5Hex(toEncode);
		return encoded;
	}

	@Override
	public UserDTO convertUserToLightUserDTO(User user) {
		if (user != null) {
			UserDTO userDTO = new UserDTO();
			if (user.getId() == null || user.getEmail() == null || user.getName() == null) {
				return null;
			} else {
				userDTO.setId(user.getId());
				userDTO.setEmail(user.getEmail());
				userDTO.setName(user.getName());
				userDTO.setPassword(DUMMY_PASS_CONVERT);
				userDTO.setConfirmationPassword(DUMMY_PASS_CONVERT);
				return userDTO;
			}
		} else {
			return null;
		}
	}
}
