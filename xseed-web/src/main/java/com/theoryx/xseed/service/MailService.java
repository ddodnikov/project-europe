package com.theoryx.xseed.service;

import javax.mail.MessagingException;

public interface MailService {

	static final String FROM = "xseed.theoryx@gmail.com";
	static final String STARTUP_INVITATION_SUBJECT = "Startup Invitation";
	static final String CHANGE_PASSWORD_SUBJECT = "Change Password";
	static final String NOTIFICATION_SUBJECT = "Notification";
	static final String ADMIN_INVITATION_SUBJECT = "Admin Invitation";
	static enum EmailType {
		ADMIN, STARTUP, NOTIFICATION, PASSWORD
	};
	
	
	/**
	 * Sends an email
	 * 
	 * @param to
	 * @param subject
	 * @param content
	 * @exception MessagingException
	 * @return nothing
	 */
	void sendEmail(String from, String to, String subject, String content) throws MessagingException;

	/**
	 * Creates an email for password activation and calls sendEmail method
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param token
	 * @return boolean. if the email was sent successfully - true, otherwise - false
	 */
	boolean sendChangePasswordLink(String from, String to, String subject, String token);

	/**
	 * Creates an email for startup invitation and calls sendEmail method
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param token
	 * @param startup
	 * @return boolean. if the email was sent successfully - true, otherwise - false
	 */
	boolean sendInvitationLinkForStartup(String from, String to, String subject, String token, String startup);

	/**
	 * Creates an email for activating admin profile and calls sendEmail method
	 * 
	 * @param from
	 * @param to
	 * @param subject
	 * @param token
	 * @return boolean. if the email was sent successfully - true, otherwise - false
	 */
	boolean sendAdminCreationEmail(String from, String to, String subject, String token);

	/**
	 * Creates an email notifying that the user is added to a startup
	 * @param from
	 * @param to
	 * @param subject
	 * @param adminName
	 * @param startupName
	 * @return boolean. if the email was sent successfully - true, otherwise - false
	 * */
	boolean sendNotificationEmailForStartup(String from, String to, String subject, String adminName, String startupName);

}
