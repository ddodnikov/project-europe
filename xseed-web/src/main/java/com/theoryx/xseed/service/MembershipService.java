package com.theoryx.xseed.service;

import com.theoryx.xseed.dto.MembershipDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;

public interface MembershipService {

	/**
	 * Creates a membership
	 * 
	 * @param user
	 * @param startup
	 * @return Membership model
	 */
	Membership createMembership(User user, Startup startup);

	/**
	 * Saves membership
	 * 
	 * @param membership
	 * @return Membership. The returned membership contains the generated id.
	 */
	Membership saveMembership(Membership membership);

	/**
	 * Delete membership
	 * 
	 * @param membership
	 * @return nothing
	 */
	void deleteMembership(Membership membership);

	/**
	 * Delete membership by MembershipDTO
	 * 
	 * @param membership
	 * @return nothing
	 */
	void deleteMembership(MembershipDTO membership);

	/**
	 * Converts Membership model to MembershipDTO
	 * 
	 * @param membership
	 * @return MembershipDTO
	 */
	MembershipDTO convertMembershipToMembershipDTO(Membership membership);
	
	/**
	 * Finds a membership by user and startup
	 * @param user
	 * @param startup
	 * 
	 * */
	Membership getMembershipByUserAndStartup(User user, Startup startup);

	Membership getMembershipByUserIdAndStartupId(Integer userId, Integer startupId);

	//boolean removeMembership(User user, Startup currentStartup);
	
	MembershipDTO getMembershipByUserAndStartup(UserDTO userDTO, StartupDTO startupDTO);

	boolean checkNoMembership(UserDTO existingUserDTO);
	
}
