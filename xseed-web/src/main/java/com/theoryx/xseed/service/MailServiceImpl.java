package com.theoryx.xseed.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

	private static final String HOST_CODE = "@@host";
	private static final String PORT_CODE = "@@port";
	private static final String APP_CODE = "/@@app";
	// localhost
	// private static final String HOST = "localhost";
	// inner dev
	// private static final String HOST = "10.0.11.162";
	// outer dev
	private static final String HOST = "78.128.33.7";
	private static final String PORT = "8080";
	private static final String APP = "/xseed-web";
	private static final String EMAIL = "@@email";
	private static final String TOKEN = "@@token";
	private static final String ADMIN_NAME = "@@StartupAdmin";
	private static final String STARTUP_NAME = "@@startup";

	@Override
	public void sendEmail(String from, String to, String subject, String content) throws MessagingException {
		String email_password = System.getenv("XSEED_EMAIL_PASS");
		Properties props = new Properties();
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", "smtp.gmail.com");
		// props.put("mail.smtp.host", "74.125.206.109");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		Session ses = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, email_password);
			}
		});

		try {
			MimeMessage message = new MimeMessage(ses);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(content, "text/html; charset=utf-8");
			Transport.send(message);
		} catch (MessagingException e) {
			throw e;
		}
	}

	@Override
	public boolean sendChangePasswordLink(String from, String to, String subject, String token) {
		try {
			String content = generateContent(EmailType.PASSWORD)
					.replaceAll(TOKEN, token)
					.replaceAll(EMAIL, to)
					.replaceAll(HOST_CODE, HOST)
					.replaceAll(PORT_CODE, PORT)
					.replaceAll(APP_CODE, APP);
			sendEmail(from, to, subject, content);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean sendInvitationLinkForStartup(String from, String to, String subject, String token, String startup) {
		String content = generateContent(EmailType.STARTUP)
					.replaceAll(TOKEN, token)
					.replaceAll(EMAIL, to)
					.replaceAll(HOST_CODE, HOST)
					.replaceAll(PORT_CODE, PORT)
					.replaceAll(APP_CODE, APP)
					.replaceAll(STARTUP_NAME, startup);
		try {
			sendEmail(from, to, subject, content);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean sendAdminCreationEmail(String from, String to, String subject, String token) {
		if (token == null) {
			return false;
		}
		String content = generateContent(EmailType.ADMIN)
						.replaceAll(TOKEN, token)
						.replaceAll(EMAIL, to)
						.replaceAll(HOST_CODE, HOST)
						.replaceAll(PORT_CODE, PORT)
						.replaceAll(APP_CODE, APP);
		try {
			sendEmail(from, to, subject, content);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean sendNotificationEmailForStartup(String from, String to, String subject, String adminName,
			String startupName) {
		String content = generateContent(EmailType.NOTIFICATION)
						.replaceAll(ADMIN_NAME, adminName)
						.replaceAll(STARTUP_NAME, startupName);
		try {
			sendEmail(from, to, subject, content);
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
/*
	private String generateContentWithToken(EmailType type, String email, String token) {
		String content = generateContent(type);
		content = content.replaceAll(TOKEN, token).replaceAll(EMAIL, email).replaceAll(HOST_CODE, HOST)
				.replaceAll(PORT_CODE, PORT).replaceAll(APP_CODE, APP);
		return content;
	}

*/	
	private String generateContent(EmailType type) {
		String email_template = "";
		switch (type) {
		case ADMIN:
			email_template = "messages/admin_token.txt";
			break;
		case STARTUP:
			email_template = "messages/invitation.txt";
			break;
		case PASSWORD:
			email_template = "messages/forgottenpassword.txt";
			break;
		case NOTIFICATION:
			email_template = "messages/notification.txt";
			break;
		default:
			email_template = "";
		}

		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(getClass().getClassLoader().getResourceAsStream(email_template), "UTF8"))) {
			String line = "";
			while ((line = br.readLine()) != null) {
				if (line.equals("")) {
					sb.append("<br></br>");
				} else {
					sb.append(line);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String content = sb.toString();

		return content;
	}
}
