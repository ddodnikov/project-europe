package com.theoryx.xseed.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theoryx.xseed.dto.MembershipDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.repository.MembershipRepository;

@Service
public class MembershipServiceImpl implements MembershipService {

	@Autowired
	private MembershipRepository membershipRepository;

	@Override
	public Membership createMembership(User user, Startup startup) {
		Membership membership = new Membership(user, startup, UserRole.USER);
		membership = membershipRepository.save(membership);
		if (membership != null) {
			return membership;
		} else {
			return null;
		}
	}

	@Override
	public Membership saveMembership(Membership membership) {
		Membership mem = membershipRepository.save(membership);
		if (mem != null) {
			return mem;
		} else {
			return null;
		}
	}

	@Override
	public void deleteMembership(Membership membership) {
		membershipRepository.delete(membership);
	}

	public MembershipDTO convertMembershipToMembershipDTO(Membership membership) {
		MembershipDTO membershipDTO = new MembershipDTO();
		if (membership != null) {
			membershipDTO.setId(membership.getId());
			membershipDTO.setRole(membership.getRole());
		}
		return membershipDTO;
	}

	@Override
	public void deleteMembership(MembershipDTO membershipDTO) {
		if (membershipDTO != null) {
			//Membership membership = membershipRepository.findOne(membershipDTO.getId());
			//System.out.println("membership1: " + membership.getId());
			//deleteMembership(membership);
			//membership = membershipRepository.findOne(membershipDTO.getId());
			//System.out.println(membership);
			//System.out.println("m23");
			membershipRepository.delete(membershipDTO.getId());
		}
	}

	@Override
	public Membership getMembershipByUserAndStartup(User user, Startup startup) {
		if (user != null && startup != null) {
			return getMembershipByUserIdAndStartupId(user.getId(), startup.getId());
		} else {
			return null;
		}
	}

	@Override
	public Membership getMembershipByUserIdAndStartupId(Integer userId, Integer startupId) {
		if (userId != null && startupId != null) {
			return membershipRepository.findByUserIdAndStartupId(userId, startupId);
		} else {
			return null;
		}
	}

	public MembershipDTO getMembershipByUserAndStartup(UserDTO userDTO, StartupDTO startupDTO) {
		if (userDTO != null && startupDTO != null) {
			Membership membership = getMembershipByUserIdAndStartupId(userDTO.getId(), startupDTO.getId());
			if (membership != null) {
				MembershipDTO membershipDTO = convertMembershipToMembershipDTO(membership);
				return membershipDTO;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public boolean checkNoMembership(UserDTO existingUserDTO) {
		if(existingUserDTO != null && existingUserDTO.getId() != null) {
			List<Membership> memberships = membershipRepository.findByUserId(existingUserDTO.getId());
			if(memberships.size()==0){
				return true;
			} else {
				return false;
			}
		} else{
			return false;
		}
	}
}
