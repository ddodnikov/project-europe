package com.theoryx.xseed.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.theoryx.xseed.annotation.Link;
import com.theoryx.xseed.dto.CountryDTO;
import com.theoryx.xseed.dto.MembershipDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.service.CountryService;
import com.theoryx.xseed.service.MembershipService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserService;
import com.theoryx.xseed.utils.ValidationUtils;

@Controller
public class StartupController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private StartupService startupService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private UserService userService;
	@Autowired
	private MembershipService membershipService;

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * This method shows the page to edit the current startup
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param model
	 *            Model
	 * @return "editstartup" String
	 */
	@Link(label = "Edit startup", url = "/editstartup", parent = "Startups")
	@RequestMapping(value = "/editstartup", method = RequestMethod.GET)
	public String getEditStartupPage(Model model, HttpServletRequest request) {
		UserDTO currentUser = (UserDTO) request.getSession().getAttribute("currentUser");
		String role = currentUser.getMembership().getRole().toString();
		if (role == "ADMIN") {
			HttpSession session = request.getSession();
			StartupDTO startupDTO = (StartupDTO) session.getAttribute("currentStartup");
			CountryDTO country = startupDTO.getCountry();
			if (country == null) {
				country = new CountryDTO();
				startupDTO.setCountry(country);
			}
			model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
			model.addAttribute("startupdto", startupDTO);
			List<CountryDTO> countries = countryService.getAllContryDTOs();
			model.addAttribute("countries", countries);
			return "/editstartup";
		} else {
			return "redirect:/home";
		}
	}

	/**
	 * This method is used when a request to edit the current startup is sent
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param model
	 *            Model
	 * @param startupDTO
	 *            StartupDTO
	 * @return "editstartup" String
	 */
	@Link(label = "Edit startup", url = "/editstartup", parent = "Startups")
	@RequestMapping(value = "editstartup", method = RequestMethod.POST)
	public String editStartup(Model model, HttpServletRequest request,
			@ModelAttribute("StartupDTO") StartupDTO startupDTO) {
		List<Error> errors = null;
		// normalization
		Map<StartupDTO, List<Error>> result = startupService.normalizeStartup(startupDTO);
		if (result.containsKey(null)) {
			errors = result.get(null);
			model.addAttribute("errors", errors);
		} else {
			// no errors after normalization
			// validation
			StartupDTO currentStartup = ((StartupDTO) request.getSession().getAttribute("currentStartup"));
			boolean isCurrentStartupName = startupService.isCurrentStartupName(startupDTO.getName(), currentStartup);
			errors = startupService.validateStartupInfo(startupDTO);
			if(!isCurrentStartupName && startupService.startupNameExists(startupDTO.getName())){
				errors.add(new Error(messageSource.getMessage("error.exists-startup-name", null, Locale.getDefault())));
			}
			if (errors.isEmpty()) {
				StartupDTO oldStartupDTO = (StartupDTO) request.getSession().getAttribute("currentStartup");
				startupDTO.setId(oldStartupDTO.getId());
				Map<StartupDTO, List<Error>> updateResult = startupService.updateStartup(startupDTO);
				if (updateResult.containsKey(null)) {
					model.addAttribute("errors", updateResult.get(null));
				} else {
					startupDTO = (StartupDTO) updateResult.keySet().toArray()[0];
					request.getSession().setAttribute("currentStartup", startupDTO);
				}
			} else {
				model.addAttribute("errors", errors);
			}
		}

		model.addAttribute("startupdto", startupDTO);
		List<CountryDTO> countries = countryService.getAllContryDTOs();
		model.addAttribute("countries", countries);
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		return "/editstartup";
	}

	/**
	 * This method shows the page to add a user to the current startup
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param model
	 *            Model
	 * @return "adduser" String
	 */
	@Link(label = "New user", url = "/add-user-to-startup", parent = "Startups")
	@RequestMapping(value = "add-user-to-startup", method = RequestMethod.GET)
	public String addUserToStartupGet(HttpServletRequest request, Model model) {
		UserDTO currentUser = (UserDTO) request.getSession().getAttribute("currentUser");
		String role = currentUser.getMembership().getRole().toString();
		if (role == "ADMIN") {
			model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
			return "/adduser";
		} else {
			return "redirect:/home";
		}
	}

	/**
	 * This method shows all the users in the current startup
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param model
	 *            Model
	 * @return "startupusers" String
	 */
	@Link(label = "Users", url = "/showusers", parent = "Startups")
	@RequestMapping(value = "showusers", method = RequestMethod.GET)
	public String showStartupUsersGet(HttpServletRequest request, Model model) {
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		// get current user
		UserDTO currentUserDto = (UserDTO) request.getSession().getAttribute("currentUser");
		// check if the currentUser is the admin of the startup
		if (currentUserDto.getMembership().getRole().toString().equals("ADMIN")) {
			return "/startupusers";
		} else {
			return "redirect:/home";
		}
	}

	/**
	 * This method is used when a request to add a user to the current startup
	 * is sent
	 * 
	 * @param session
	 *            HttpSession
	 * @param model
	 *            Model
	 * @param email
	 *            String
	 * @return "adduser" String
	 */
	@Link(label = "New user", url = "/add-user-to-startup", parent = "Startups")
	@RequestMapping(value = "add-user-to-startup", method = RequestMethod.POST)
	public String addUserToStartup(HttpServletRequest request, HttpSession session, Model model,
			@RequestParam("email") String email) {
		List<Error> errors = new LinkedList<Error>();
		StartupDTO startupDTO = (StartupDTO) session.getAttribute("currentStartup");
		model.addAttribute("startupdto", startupDTO);
		UserDTO currentUserDto = (UserDTO) request.getSession().getAttribute("currentUser");
		// check if currentUser is the admin of the startup
		if (currentUserDto.getMembership().getRole().equals(UserRole.ADMIN)) {
			if (ValidationUtils.validateUserEmail(email)) {
				email = email.trim();
				if (userService.ifExist(email)) {
					Map<UserDTO, List<Error>> userResult = userService.getUserByEmail(email);
					if(userResult.containsKey(null)){
						errors = userResult.get(null);
					} else{
						UserDTO existingUserDTO = (UserDTO)userResult.keySet().toArray()[0];
						if(existingUserDTO.isActive()){
							startupService.addExistingActive(startupDTO, currentUserDto, existingUserDTO, errors, email);
						} else {
							startupService.addExistingInactive(startupDTO, currentUserDto, existingUserDTO, errors, email);	
						}
					}
					if(errors.size()==0){
						session.setAttribute("currentStartup", currentUserDto.getStartup());
						model.addAttribute("success", messageSource.getMessage("success.email", new Object[0], Locale.getDefault()));
					}
				} else {
					boolean addingResult = startupService.addNonExistigUser(startupDTO, currentUserDto, errors, email);
					if(addingResult){
						session.setAttribute("currentStartup", currentUserDto.getStartup());
						model.addAttribute("success",
								messageSource.getMessage("success.email", new Object[0], Locale.getDefault()));
					}
				}
			} else {
				errors.add(
						new Error(messageSource.getMessage("error.invalid-email", new Object[0], Locale.getDefault())));
			}
		} else {
			return "redirect:/403";
		}
		model.addAttribute("errors", errors);
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		return "/adduser";
	}
	
	@Link(label = "Remove user", url = "/remove-user-from-startup", parent = "Startups")
	@RequestMapping(value = "remove-user-from-startup", method = RequestMethod.GET)
	public String removeUserFromStartup(HttpServletRequest request, Model model) {
		// show users
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		// get current user
		UserDTO currentUserDto = (UserDTO) request.getSession().getAttribute("currentUser");
		// check if the currentUser is the admin of the startup
		if (currentUserDto.getMembership().getRole().toString().equals("ADMIN")) {
			return "/removeusers";
		} else {
			return "redirect:/home";
		}
	}

	@Link(label = "Remove user", url = "/remove-user-from-startup", parent = "Startups")
	@RequestMapping(value = "remove-user-from-startup", method = RequestMethod.POST)
	public String removeUserFromStartupPost(@RequestParam(value="removed_emails", required=false) List<String> emails, HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		UserDTO currentUserDto = (UserDTO) request.getSession().getAttribute("currentUser");
		if(emails == null){
			model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
			return "/removeusers";
		} else {
			if (currentUserDto.getMembership().getRole().equals(UserRole.ADMIN)) {
				StartupDTO startupDTO = (StartupDTO) session.getAttribute("currentStartup");
				List<Error> errors = new LinkedList<Error>();
				for(String email: emails){
					//check if email is valid
					if(ValidationUtils.validateUserEmail(email)){
						//get user by email
							User user = userService.getByEmail(email);
							UserDTO existingUserDTO = userService.convertUserToLightUserDTO(user);
							//check if currentStartup contains the user.
							if(startupDTO.getUsers().contains(existingUserDTO)){
								//get membership
								MembershipDTO membershipDTO = membershipService.getMembershipByUserAndStartup(existingUserDTO, startupDTO);
								//check if role is user							
								if(membershipDTO!=null && membershipDTO.getRole().equals(UserRole.USER)){								
									membershipService.deleteMembership(membershipDTO);
									//membership 0 => inactive
									if(membershipService.checkNoMembership(existingUserDTO)){
										existingUserDTO.setActive(false);
										userService.updateUser(existingUserDTO);
									}
									//update users in startupDTO.
									startupDTO.getUsers().remove(existingUserDTO);
								} else {
									//return error message invalid user.
									String errorText = email + messageSource.getMessage("error.remove-invalid-email", new Object[0], Locale.getDefault());
									errors.add(new Error(errorText));
								}
							} else {
								//return error message invalid user.
								String errorText = email + messageSource.getMessage("error.remove-invalid-email", new Object[0], Locale.getDefault());
								errors.add(new Error(errorText));
							}
					} else{
						//return error message invalid user.
						String errorText = email + messageSource.getMessage("error.remove-invalid-email", new Object[0], Locale.getDefault());
						errors.add(new Error(errorText));
					}
				}		
				currentUserDto.setStartup(startupDTO);
				session.setAttribute("currentStartup", startupDTO);
				//add data in model
				model.addAttribute("errors", errors);
				model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
				return "/removeusers";
			} else {
				return "redirect:/403";
			}
		}
	}

	@Link(label = "Startups", parent = "Home", url = "/startups")
	@RequestMapping(value = "/startups", method = RequestMethod.GET)
	public String showUsersInStartup(Model model, HttpServletRequest request, HttpServletResponse response) {
		String email = ((UserDTO) request.getSession().getAttribute("currentUser")).getEmail();
		User currentUser = (User) userService.getByEmail(email);
		
		List<Startup> startups = currentUser.getStartups();
		List<StartupDTO> userStartups = startupService.convert(startups);
		Integer defaultStartup = userStartups.get(0).getId();
		for (Membership mem : currentUser.getMemberships()) {
			if (mem.isDefault()) {
				defaultStartup = mem.getStartup().getId();
			}
		}

		model.addAttribute("defaultStartup", defaultStartup);
		model.addAttribute("userStartups", userStartups);
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		return "/startups";
	}

	@RequestMapping(value = "/setstartups", method = RequestMethod.POST)
	public String setDafaultAndCurrentStartup(Model model, HttpServletRequest request, HttpServletResponse response,
			RedirectAttributes redir) {
		String current = (String) request.getParameter("current");
		String defaults = (String) request.getParameter("default");
		boolean cur = false;
		boolean def = false;
		StartupDTO currentStartup = (StartupDTO) request.getSession().getAttribute("currentStartup");
		UserDTO currentUserDto = (UserDTO) request.getSession().getAttribute("currentUser");
		if (!currentStartup.getId().equals(Integer.valueOf(current))) {
			Startup newCurrentStartup = startupService.findById(Integer.valueOf(current));
			StartupDTO dto = startupService.convertStartupToStartupDTO(newCurrentStartup);
			request.getSession().setAttribute("currentStartup", dto);
			Membership mem = membershipService.getMembershipByUserIdAndStartupId(currentUserDto.getId(), newCurrentStartup.getId());
			if(mem.getRole().toString().equals("ADMIN")) {
				currentUserDto.getMembership().setRole(UserRole.ADMIN);
			} else {
				currentUserDto.getMembership().setRole(UserRole.USER);
			}
			request.getSession().setAttribute("currentUser", currentUserDto);
			cur = true;
		}
		User currentUser = (User) userService.getByEmail(currentUserDto.getEmail());
		List<Membership> mems = currentUser.getMemberships();
		for (Membership membership : mems) {
			if (membership.isDefault()) {
				if (!membership.getStartup().getId().equals(Integer.valueOf(defaults))) {
					membership.setDefault(false);
					membershipService.saveMembership(membership);
					def = true;
				}
			} else {
				if (membership.getStartup().getId().equals(Integer.valueOf(defaults))) {
					membership.setDefault(true);
					membershipService.saveMembership(membership);
					def = true;
				}
			}
		}
		redir.addFlashAttribute("message", startupsChangeMessage(def, cur));
		model.addAttribute("defaultStartup", defaults);
		return "redirect:/startups";
	}

	private JSONObject startupsChangeMessage(boolean defaults, boolean currents) {
		JSONObject object = new JSONObject();
		String message = null;
		if (defaults) {
			message = messageSource.getMessage("startups.change-default", new Object[0], Locale.getDefault());
		}
		if (currents) {
			message = messageSource.getMessage("startups.change-current", new Object[0], Locale.getDefault());
		}
		if (defaults && currents) {
			message = messageSource.getMessage("startups.change-both", new Object[0], Locale.getDefault());
		}
		if (!defaults && !currents) {
			message = messageSource.getMessage("startups.change-none", new Object[0], Locale.getDefault());
		}
		object.put("message", message);
		return object;
	}
}
