package com.theoryx.xseed.controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.theoryx.xseed.annotation.Link;
import com.theoryx.xseed.dto.QuestionDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.service.MailService;
import com.theoryx.xseed.service.QuestionService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserService;
import com.theoryx.xseed.utils.PaginationUtils;
import com.theoryx.xseed.utils.TokenUtils;
import com.theoryx.xseed.utils.ValidationUtils;

@Controller
public class AdminController {

	private static final String EMPTY_NAME = "";
	private static final String DUMMY_PASSWORD = "DummyPassword123Th30RyX";

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private StartupService startupService;
	@Autowired
	private UserService userService;
	@Autowired
	private MailService mailService;
	@Autowired
	private QuestionService questionService;

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * This method shows the administrators' login page
	 * 
	 * @return "/admin" String
	 */
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String getAdminLoginPage() {
		return "/admin";
	}

	/**
	 * This method shows a page with all the administrators
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param model
	 *            Model
	 * @return "admins" String
	 */
	@Link(label = "Administrators", parent = "Home", url = "/admins")
	@RequestMapping(value = "/admins", method = RequestMethod.GET)
	public String showAdmins(Model model, HttpServletRequest request) {
		List<User> admins = userService.getAllAdmins();
		if (admins == null) {
			admins = new LinkedList<User>();
		}
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		model.addAttribute("admins", admins);
		return "/admins";
	}

	/**
	 * This method shows a page to add administrators
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param model
	 *            Model
	 * @return "addadmin" String
	 */
	@Link(label = "Add administrator", parent = "Administrators", url = "/addadmin")
	@RequestMapping(value = "/addadmin", method = RequestMethod.GET)
	public String getAddingAddminsPage(Model model, HttpServletRequest request) {
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		return "/addadmin";
	}

	/**
	 * This method is used when a request to add admin is sent
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @param email
	 *            String
	 * @param model
	 *            Model
	 * @return "addadmin" String
	 */
	@Link(label = "Add administrator", parent = "Administrators", url = "/addadmin")
	@RequestMapping(value = "/addadmin", method = RequestMethod.POST)
	public String addAddmin(HttpServletRequest request, Model model, @RequestParam("email") String email) {
		List<Error> errors = new LinkedList<Error>();
		if (ValidationUtils.validateUserEmail(email)) {
			email = email.trim();
			if (userService.ifExist(email)) {
				errors.add(
						new Error(messageSource.getMessage("error.user-exists", new Object[0], Locale.getDefault())));
			} else {
				UserDTO userDTO = new UserDTO(EMPTY_NAME, email, DUMMY_PASSWORD, DUMMY_PASSWORD, false, UserRole.ADMIN);
				User user = userService.createUser(userDTO);
				userDTO = userService.convertUserToUserDTO(user);

				String token = userService.generateActivationToken(userDTO);
				boolean mailResult = mailService.sendAdminCreationEmail(MailService.FROM, email, MailService.ADMIN_INVITATION_SUBJECT, token);
				if (mailResult) {
					model.addAttribute("success",
							messageSource.getMessage("success.email", new Object[0], Locale.getDefault()));
				} else {
					errors.add(new Error(
							messageSource.getMessage("error.mail-error", new Object[0], Locale.getDefault())));
				}
			}
		} else {
			errors.add(new Error(messageSource.getMessage("error.invalid-email", new Object[0], Locale.getDefault())));
		}
		model.addAttribute("errors", errors);
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		return "/addadmin";
	}

	/**
	 * This method is used when an added administrator clicks on the activation
	 * link
	 * 
	 * @param token
	 *            String
	 * @param email
	 *            String
	 * @param model
	 *            Model
	 * @return "addadmin" String
	 */
	@RequestMapping(value = "/createadmin", method = RequestMethod.GET)
	public String createAdmin(@RequestParam("token") String token, @RequestParam("email") String email, Model model,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		
		if (request.getSession().getAttribute("currentUser") != null) {
			UserController.Logout(request, response);
			TokenUtils.setNewToken(model, request);
		}
		
		Map<UserDTO, List<Error>> result = userService.getUserByEmail(email);
		UserDTO userDTO = null;
		List<Error> errors = null;
		if (result != null) {
			for (Entry<UserDTO, List<Error>> entry : result.entrySet()) {
				userDTO = entry.getKey();
				errors = entry.getValue();
			}
		}
		if (userDTO == null) {
			model.addAttribute("errors", errors);
		} else {
			if (userDTO.isActive()) {
				errors.add(new Error(messageSource.getMessage("error.account-already-activated", new Object[0],
						Locale.getDefault())));
				model.addAttribute("errors", errors);
				return "/login";
			} else {
				String generatedToken = userService.generateActivationToken(userDTO);
				if (token.equals(generatedToken)) {
					model.addAttribute("email", email);
				} else {
					String newToken = userService.generateActivationToken(userDTO);
					mailService.sendAdminCreationEmail(MailService.FROM, email, MailService.ADMIN_INVITATION_SUBJECT, newToken);
					errors.add(new Error(
							messageSource.getMessage("error.new-activation-link", new Object[0], Locale.getDefault())));
					model.addAttribute("errors", errors);
					return "/login";
				}
			}
		}
		model.addAttribute("userdto", userDTO);
		return "/createadmin";
	}

	@Link(label = "Algo questions", parent = "Home", url = "/algoquestions")
	@RequestMapping(value = "/algoquestions", method = RequestMethod.GET)
	public String getAlgoQuestions(HttpServletRequest request, Model model) {
		List<Question> algoQuestions = questionService.findAll();
		List<QuestionDTO> algoQuestionDtos = questionService.convert(algoQuestions);
		model.addAttribute("algoQuestions", algoQuestionDtos);
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));
		return "/algoquestions";
	}

	@RequestMapping(value = "/algoquestions", method = RequestMethod.POST)
	public String saveAlgoQuestions(HttpServletRequest request, Model model) {
		List<Question> algoQuestions = questionService.findAll();
		for (Question question : algoQuestions) {
			String value = request.getParameter(question.getId().toString());
			if (value != null && !question.isAlgo()) {
				question.setAlgo(true);
				questionService.save(question);
			}
			if (value == null && question.isAlgo()) {
				question.setAlgo(false);
				questionService.save(question);
			}
		}
		return "redirect:/algoquestions";
	}
	
	/**
	 * This method returns the show all startups page
	 * 
	 * @param model
	 * @param request
	 */
	@Link(label = "Startups", parent = "Home", url = "/showallstartups?page=1&show=15")
	@RequestMapping(value = "/showallstartups", method = RequestMethod.GET)
	public String getAllStartups(Model model, HttpServletRequest request, @RequestParam("page") Integer page,
			@RequestParam("show") String limit) {
		List<Startup> startups = null;
		Integer allStartupsCount = startupService.getAllStartupCount();
		Integer startupsPerPage = 0;
		if (limit.equals("All")) {
			startups = startupService.getAllStartups();
		} else {
			startupsPerPage = Integer.parseInt(limit);
			if (startupsPerPage >= allStartupsCount) {
				startups = startupService.getAllStartups();
			} else {
				Integer paginationSize = PaginationUtils.getPaginationSize(allStartupsCount, startupsPerPage);
				Integer maxPage = PaginationUtils.getMaxPage(allStartupsCount, startupsPerPage);
				if (page <= 0) {
					return "redirect:/showallstartups?page=1&show=" + startupsPerPage;
				}
				if (page > maxPage) {
					return "redirect:/showallstartups?page=" + maxPage + "&show=" + startupsPerPage;
				}
				startups = startupService.getPaginationStartups(startupsPerPage, (page - 1) * startupsPerPage);
				Map<Integer, Boolean> pages = PaginationUtils.getPages(paginationSize, maxPage, page);
				Map<Integer, Boolean> nextPrevPages = PaginationUtils.getPrevNextPages(maxPage, page);
				model.addAttribute("pages", pages);
				model.addAttribute("nextPrevPages", nextPrevPages);
			}
		}
		List<StartupDTO> startupDtos = new ArrayList<StartupDTO>();
		for (Startup startup : startups) {
			startupDtos.add(startupService.convertStartupToStartupDTO(startup));
		}
		model.addAttribute("startupsPerPage", startupsPerPage);
		model.addAttribute("startupsCount", allStartupsCount);
		model.addAttribute("startups", startupDtos);
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));

		return "/showallstartups";
	}
	
	/**
	 * This method returns the show all users page
	 * 
	 * @param model
	 * @param request
	 */
	@Link(label = "Users", parent = "Home", url = "/showallusers?page=1&show=15")
	@RequestMapping(value = "/showallusers", method = RequestMethod.GET)
	public String getAllUsers(Model model, HttpServletRequest request, @RequestParam("page") Integer page,
			@RequestParam("show") String limit) {
		List<User> users = null;
		Integer allUsersCount = userService.getUsersCount();
		Integer usersPerPage = 0;
		if (limit.equals("All")) {
			users = userService.getAllUsers();
		} else {
			usersPerPage = Integer.parseInt(limit);
			if (usersPerPage >= allUsersCount) {
				users = userService.getAllUsers();
			} else {
				Integer paginationSize = PaginationUtils.getPaginationSize(allUsersCount, usersPerPage);
				Integer maxPage = PaginationUtils.getMaxPage(allUsersCount, usersPerPage);
				if (page <= 0) {
					return "redirect:/showallusers?page=1&show=" + usersPerPage;
				}
				if (page > maxPage) {
					return "redirect:/showallusers?page=" + maxPage + "&show=" + usersPerPage;
				}
				users = userService.getPaginationUsers(usersPerPage, (page - 1) * usersPerPage);
				Map<Integer, Boolean> pages = PaginationUtils.getPages(paginationSize, maxPage, page);
				Map<Integer, Boolean> nextPrevPages = PaginationUtils.getPrevNextPages(maxPage, page);
				model.addAttribute("pages", pages);
				model.addAttribute("nextPrevPages", nextPrevPages);
			}
		}
		List<UserDTO> userDtos = new ArrayList<UserDTO>();
		for (User user : users) {
			userDtos.add(userService.convertUserToUserDTO(user));
		}
		model.addAttribute("usersPerPage", usersPerPage);
		model.addAttribute("usersCount", allUsersCount);
		model.addAttribute("users", userDtos);
		model.addAttribute("breadcrumbs", request.getAttribute("breadCrumbs"));

		return "/showallusers";
	}


}
