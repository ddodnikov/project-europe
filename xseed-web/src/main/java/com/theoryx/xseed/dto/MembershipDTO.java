package com.theoryx.xseed.dto;

import com.theoryx.xseed.model.UserRole;

public class MembershipDTO {
	
	private Integer id;
	private UserRole role;
	
	public MembershipDTO(){
	}
	
	public MembershipDTO(Integer id){
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}
}
