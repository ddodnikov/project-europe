package com.theoryx.xseed.dto;

public class UrlLabelMapping {

	private String url;
	private String label;
	private boolean isLink;

	public UrlLabelMapping(String url, String label, boolean isLink) {
		this.url = url;
		this.label = label;
		this.isLink = isLink;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean getIsLink() {
		return isLink;
	}

	public void setIsLink(boolean link) {
		this.isLink = link;
	}
}
