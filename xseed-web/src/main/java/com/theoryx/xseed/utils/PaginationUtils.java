package com.theoryx.xseed.utils;

import java.util.LinkedHashMap;
import java.util.Map;

public class PaginationUtils {

	public static Map<Integer, Boolean> getPages(Integer paginationSize, Integer maxPage, Integer currentPage) {
		Map<Integer, Boolean> pages = new LinkedHashMap<Integer, Boolean>();
		Integer middlePage = paginationSize / 2 + 1;
		Integer beforeMiddlePage = paginationSize / 2;

		for (int pageIndex = 1; pageIndex <= paginationSize; pageIndex++) {
			Integer pageNumber = 0;
			Boolean active = false;

			if (currentPage <= middlePage) {
				pageNumber = middlePage - beforeMiddlePage + pageIndex - 1;
			} else {
				if (maxPage > paginationSize && currentPage <= maxPage - beforeMiddlePage) {
					pageNumber = currentPage - beforeMiddlePage + pageIndex - 1;
				} else {
					pageNumber = maxPage + 1 - middlePage - beforeMiddlePage + pageIndex - 1;
				}
			}

			if (pageNumber == currentPage) {
				active = true;
			}

			pages.put(pageNumber, active);
		}

		return pages;
	}

	public static Map<Integer, Boolean> getPrevNextPages(Integer maxPage, Integer currentPage) {
		Map<Integer, Boolean> pages = new LinkedHashMap<Integer, Boolean>();

		Integer prevPage = 0;
		Boolean prevDisabled = true;
		if (maxPage > 1 && currentPage > 1) {
			prevDisabled = false;
			prevPage = currentPage - 1;
		}
		pages.put(prevPage, prevDisabled);

		Integer nextPage = 0;
		Boolean nextDisabled = true;
		if (maxPage > 1 && currentPage < maxPage) {
			nextDisabled = false;
			nextPage = currentPage + 1;
		}
		if(currentPage == maxPage){
			nextPage = maxPage;
		}
		pages.put(nextPage, nextDisabled);

		return pages;
	}

	public static Integer getPaginationSize(Integer allCount, Integer perPage) {
		Integer paginationSize = 0;
		if (allCount <= 2 * perPage) {
			paginationSize = 2;
		} else {
			if (allCount <= 5 * perPage) {
				paginationSize = 3;
			} else {
				if (allCount <= 10 * perPage) {
					paginationSize = 5;
				} else {
					if (allCount <= 15 * perPage) {
						paginationSize = 7;
					} else {
						paginationSize = 9;
					}
				}
			}
		}
		return paginationSize;
	}

	public static Integer getMaxPage(Integer allCount, Integer perPage) {
		Integer maxPage = (allCount % perPage == 0) ? (allCount / perPage) : (allCount / perPage + 1);
		return maxPage;
	}

}
