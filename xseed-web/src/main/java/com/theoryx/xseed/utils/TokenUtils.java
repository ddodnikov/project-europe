package com.theoryx.xseed.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.ui.Model;

public class TokenUtils {
	
	public static void setNewToken(Model model, HttpServletRequest request){
		org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository tokenRepository = new org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository();
		org.springframework.security.web.csrf.DefaultCsrfToken defaultCSRFToken = (DefaultCsrfToken) tokenRepository.generateToken(request);
		request.setAttribute(defaultCSRFToken.getParameterName(), defaultCSRFToken);
		HttpSession session = request.getSession(true);
		session.setAttribute("org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN", defaultCSRFToken);
	
	}

}
