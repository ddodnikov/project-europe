package com.theoryx.xseed.unit.service;

import static org.mockito.Matchers.any;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.testng.Assert;

import com.theoryx.xseed.dto.CountryDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Country;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.repository.UserRepository;
import com.theoryx.xseed.service.MembershipService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@Mock
	private UserRepository userRepository;
	@Mock
	private MessageSource messageSource;
	@Mock
	private StartupService startupService;
	@Mock
	private MembershipService membershipService;
	@InjectMocks
	private UserServiceImpl userService;
	
	private User user;
	private UserDTO userDTO;
	private StartupDTO startupDTO;
	
	@Before
	public void setUp(){
		//User
		user = new User();
		user.setEmail("test1@abv.bg");
		user.setId(1);
		user.setIsActive(true);
		user.setRole(UserRole.USER);
		user.setName("Test1");
		user.setHashedPassword("123456");
		
		//StartupDTO
		startupDTO = new StartupDTO();		
		startupDTO.setId(1);
		startupDTO.setName("StartupTest1");
		startupDTO.setEmail("test1startup@abv.bg");
		startupDTO.setPhone("0896480123");
		startupDTO.setVat("0000");
		startupDTO.setWebsite("www.xseed-java.com");
		CountryDTO country = new CountryDTO();
		country.setId(1);
		country.setName("Bulgaria");
		startupDTO.setCountry(country);
		
		//UserDTO
		userDTO = new UserDTO();
		userDTO.setId(1);
		userDTO.setName("Test1");
		userDTO.setEmail("test1@abv.bg");
		userDTO.setPassword("123456");
		userDTO.setConfirmationPassword("123456");
		userDTO.setActive(true);
		userDTO.setRole(UserRole.USER);
		userDTO.setStartup(startupDTO);
	}
	
	@Test
	public void testIfExistWithExistingEmail() {
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		Mockito.when(userRepository.findByEmail("tesssst1@abv.bg")).thenReturn(null);
		Mockito.when(userRepository.findByEmail("")).thenReturn(null);
		Mockito.when(userRepository.findByEmail(null)).thenReturn(null);
		
		// existing email
		Assert.assertTrue(userService.ifExist("test1@abv.bg"));
	}
	
	@Test
	public void testIfExistWithNonExistingEmail(){
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		Mockito.when(userRepository.findByEmail("tesssst1@abv.bg")).thenReturn(null);
		Mockito.when(userRepository.findByEmail("")).thenReturn(null);
		Mockito.when(userRepository.findByEmail(null)).thenReturn(null);
		
		// non-existing email
		Assert.assertFalse(userService.ifExist("tesssst1@abv.bg"));
	}
	
	@Test
	public void testIfExistWithEmptyEmail(){
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		Mockito.when(userRepository.findByEmail("tesssst1@abv.bg")).thenReturn(null);
		Mockito.when(userRepository.findByEmail("")).thenReturn(null);
		Mockito.when(userRepository.findByEmail(null)).thenReturn(null);
		
		//empty email
		Assert.assertFalse(userService.ifExist(""));
	}
	
	@Test
	public void testIfExistWithNullEmail(){
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		Mockito.when(userRepository.findByEmail("tesssst1@abv.bg")).thenReturn(null);
		Mockito.when(userRepository.findByEmail("")).thenReturn(null);
		Mockito.when(userRepository.findByEmail(null)).thenReturn(null);
		
		// null email
		Assert.assertFalse(userService.ifExist(null));
	}
	
	private UserDTO createValidUserDTO(){
		// valid user
		UserDTO userdto = new UserDTO();
		userdto.setName("userTest1");
		userdto.setEmail("userTest@abv.bg");
		userdto.setPassword("123456");
		userdto.setConfirmationPassword("123456");
		userdto.setActive(true);
		userdto.setRole(UserRole.USER);
		StartupDTO startup = new StartupDTO();
		startup.setName("UserTest1Startup");
		userdto.setStartup(startup);
		
		return userdto;
	}
	
	@Test
	public void testValidateRegisterInputNullUser() {
		Assert.assertNull(userService.validateRegisterInput(null));
	}
	
	@Test
	public void testValidateRegisterInputInvalidUserNameLengthGT50() {
		// valid user
		UserDTO userdto = createValidUserDTO();
		
		//large name size >50
		userdto.setName("123456789012345678901234567890123456789012345678901");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidEmptyUserName(){
		// valid user
		UserDTO userdto = createValidUserDTO();

		// empty name
		userdto.setName("");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidNullUserName(){
		// valid user
		UserDTO userdto = createValidUserDTO();


		// null name
		userdto.setName(null);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidUserNameLengthLT3(){
		// valid user
		UserDTO userdto = createValidUserDTO();

		//small name size <3
		userdto.setName("Te");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputInvalidEmptyPassword(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//empty password
		userdto.setPassword("");
		userdto.setConfirmationPassword("");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidNullPassword(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//null password
		userdto.setPassword(null);
		userdto.setConfirmationPassword(null);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidDifferentPassword(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//different passwords
		userdto.setPassword("123456");
		userdto.setConfirmationPassword("");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
		userdto.setPassword("");
		userdto.setConfirmationPassword("123456");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
		userdto.setPassword("123456");
		userdto.setConfirmationPassword(null);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
		userdto.setPassword(null);
		userdto.setConfirmationPassword("123456");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}

	@Test
	public void testValidateRegisterInputWithInvalidPasswordLengthLT4(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//small password size
		userdto.setPassword("123");
		userdto.setConfirmationPassword(null);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
		userdto.setPassword(null);
		userdto.setConfirmationPassword("123");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
		userdto.setPassword("123");
		userdto.setConfirmationPassword("123");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidPasswordLengthGT20(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//large password size
		userdto.setPassword("123456789101112131415");
		userdto.setConfirmationPassword(null);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
		userdto.setPassword(null);
		userdto.setConfirmationPassword("123456789101112131415");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
		userdto.setPassword("123456789101112131415");
		userdto.setConfirmationPassword("123456789101112131415");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidNullEmail(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//null email
		userdto.setEmail(null);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-email", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
				
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidEmptyEmail(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//empty email
		userdto.setEmail("");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-email", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidEmailSymbols(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//invalid email-invalid symbols
		userdto.setEmail("abc!$ABC.@abca%bcabc.abcc");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-email", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}

	@Test
	public void testValidateRegisterInputWithInvalidEmailSymbolsInTLD(){
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//invalid email-invalid symbols in TLD(Top-Level Domain)
		userdto.setEmail("abcABC.@abcabcabc.a$!%");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-email", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidEmailLengthInTLD() {
		//valid user
		UserDTO userdto = createValidUserDTO();
		
		//invalid email-invalid length of TLD(Top-Level Domain)
		userdto.setEmail("abcABC.@abcabcabc.abvgdccc");
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-email", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithExistingEmail() {
		Mockito.when(messageSource.getMessage("error.user-exist", null, Locale.getDefault())).thenReturn("exists");
		UserDTO userdto = createValidUserDTO();
		Mockito.when(userRepository.findByEmail(userdto.getEmail())).thenReturn(new User());
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(userService.validateRegisterInput(userdto).get(0).getMessage(), "exists");
	}
	
	@Test
	public void testValidateRegisterInputWithExistingStartupName() {
		Mockito.when(messageSource.getMessage("error.exists-startup-name", null, Locale.getDefault())).thenReturn("exists");
		UserDTO userdto = createValidUserDTO();
		Mockito.when(startupService.startupNameExists(userdto.getStartup().getName())).thenReturn(true);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(userService.validateRegisterInput(userdto).get(0).getMessage(), "exists");
	}
	
	private StartupDTO createValidStartupDTO(){
		StartupDTO startup = new StartupDTO();
		startup.setName("UserTest1Startup");
		return startup;
	}
	
	
	@Test
	public void testValidateRegisterInputWithInvalidEmptyStartupName(){
		// valid user
		UserDTO userdto = createValidUserDTO();
		
		//null startup
		userdto.setStartup(null);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-startup-name", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidNullStartup(){
		// valid user
		UserDTO userdto = createValidUserDTO();
		
		//valid startup
		StartupDTO startup = createValidStartupDTO();
		
		//empty startup name
		startup.setName("");
		userdto.setStartup(startup);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-startup-name", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidStartupNameLengthLT3(){
		// valid user
		UserDTO userdto = createValidUserDTO();
		
		//valid startup
		StartupDTO startup = createValidStartupDTO();
		
		//small startup name
		startup.setName("ab");
		userdto.setStartup(startup);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-startup-name", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
		
	}
	
	@Test
	public void testValidateRegisterInputWithInvalidStartupNameLengthGT50(){
		// valid user
		UserDTO userdto = createValidUserDTO();
		
		//valid startup
		StartupDTO startup = createValidStartupDTO();
				
		//large startup name
		startup.setName("123456789012345678901234567890123456789012345678901");
		userdto.setStartup(startup);
		Assert.assertEquals(1, userService.validateRegisterInput(userdto).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-startup-name", null, Locale.getDefault()),
				userService.validateRegisterInput(userdto).get(0).getMessage());
	}

	@Test
	public void testValidateRegisterInputValidUser() {
		UserDTO userdto = createValidUserDTO();
		Assert.assertTrue(userService.validateRegisterInput(userdto).isEmpty());
	}
	
	@Test
	public void testCreateNullUser() {
		Mockito.when(userRepository.save(any(User.class))).thenReturn(null);
		Assert.assertNull(userService.createUser(null));
	}
	
	@Test
	public void testCreateNullUser2() {
		Mockito.when(userRepository.save(any(User.class))).thenReturn(null);
		Assert.assertNull(userService.createUser(new UserDTO()));
	}
	
	@Test
	public void testCreateValidUser(){
		// valid user
		Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
		User savedUser = userService.createUser(userDTO);
		Assert.assertNotNull(savedUser);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void testCreateSameUser(){
		
		Mockito.when(userRepository.save(any(User.class)))
				.thenReturn(user)
				.thenThrow(Exception.class);
		userService.createUser(userDTO);
		userService.createUser(userDTO);
	}
	
	@Test
	public void testDeleteValidUser(){
		// valid user
		Mockito.doNothing().when(userRepository).delete(user);
		Assert.assertTrue(userService.deleteUser(userDTO));
	}
	
	@Test
	public void testDeleteInvalidUser(){
		Mockito.doNothing().when(userRepository).delete(any(User.class));
		//null id
		userDTO.setId(null);
		Assert.assertFalse(userService.deleteUser(userDTO));
		//null email
		userDTO.setEmail(null);
		Assert.assertFalse(userService.deleteUser(userDTO));
	}
	
	@Test
	public void testDeleteNullUser(){
		Mockito.doNothing().when(userRepository).delete(any(User.class));
		//null user
		Assert.assertFalse(userService.deleteUser(null));
	}
	
	@Test
	public void testUpdateValidUser(){
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
		Assert.assertTrue(userService.updateUser(userDTO));
	}
	
	@Test
	public void testUpdateInvalidUser(){
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		//invalid password
		//null password
		userDTO.setPassword(null);
		userDTO.setConfirmationPassword(null);
		Assert.assertFalse(userService.updateUser(userDTO));
		
		//empty password
		userDTO.setPassword("");
		userDTO.setConfirmationPassword("");
		Assert.assertFalse(userService.updateUser(userDTO));
		
		//different passwords
		userDTO.setPassword("123456");
		userDTO.setConfirmationPassword("1234561");
		Assert.assertFalse(userService.updateUser(userDTO));
	}
	
	@Test
	public void testUpdateNullUser(){
		Assert.assertFalse(userService.updateUser(null));
	}
	
	@Test
	public void testValidateNewUserProfileWithValidData(){
		Assert.assertTrue(userService.validateNewUserProfile(userDTO).isEmpty());
	}
	
	@Test
	public void testValidateNewUserProfileWithInvalidData(){		
		//empty name/password/confirmation password
		userDTO.setName("");
		userDTO.setPassword("");
		Assert.assertEquals(1, userService.validateNewUserProfile(userDTO).size());
		Assert.assertEquals(messageSource.getMessage("error.no-input", null, Locale.getDefault()), userService.validateNewUserProfile(userDTO).get(0).getMessage());
		
		//invalid name
		userDTO.setName("Te");
		userDTO.setPassword("123456");
		Assert.assertEquals(1, userService.validateNewUserProfile(userDTO).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()), userService.validateNewUserProfile(userDTO).get(0).getMessage());
		
		userDTO.setName("012345678901234567890123456789012345678901234567890123456789");
		userDTO.setPassword("123456");
		Assert.assertEquals(1, userService.validateNewUserProfile(userDTO).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()), userService.validateNewUserProfile(userDTO).get(0).getMessage());
		
		//invalid password
		userDTO.setName("userTest1");
		userDTO.setPassword("123");
		userDTO.setConfirmationPassword("12");
		Assert.assertEquals(1, userService.validateNewUserProfile(userDTO).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()), userService.validateNewUserProfile(userDTO).get(0).getMessage());
		
		//invalid password and name
		userDTO.setName("Te");
		userDTO.setPassword("123");
		userDTO.setConfirmationPassword("12");
		Assert.assertEquals(2, userService.validateNewUserProfile(userDTO).size());
	}
	
	@Test
	public void testValidateNewUserProfileWithNullData(){
		Assert.assertEquals(messageSource.getMessage("error.no-input", null, Locale.getDefault()), userService.validateNewUserProfile(null).get(0).getMessage());
		//null name&&password
		userDTO.setName(null);
		userDTO.setPassword(null);
		Assert.assertEquals(messageSource.getMessage("error.no-input", null, Locale.getDefault()), userService.validateNewUserProfile(userDTO).get(0).getMessage());
	}
	
	@Test
	public void testValidateEditProfileInputWithValidData(){
		Assert.assertTrue(userService.validateEditProfileInput(userDTO).isEmpty());
	}
	
	@Test
	public void testValidateEditProfileInputWithInvalidData(){
		//empty name&&password&&confirmationPassword
		userDTO.setName("");
		userDTO.setPassword("");
		userDTO.setConfirmationPassword("");
		Assert.assertEquals(1, userService.validateEditProfileInput(userDTO).size());
		Assert.assertEquals(messageSource.getMessage("error.no-input", null, Locale.getDefault()), userService.validateEditProfileInput(userDTO).get(0).getMessage());
		
		//empty name
		userDTO.setName("");
		userDTO.setPassword("123456");
		userDTO.setConfirmationPassword("123456");
		Assert.assertEquals(1, userService.validateEditProfileInput(userDTO).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()), userService.validateEditProfileInput(userDTO).get(0).getMessage());
		
		//empty password
		userDTO.setName("Test123");
		userDTO.setPassword("");
		userDTO.setConfirmationPassword("");
		Assert.assertEquals(0, userService.validateEditProfileInput(userDTO).size());
		
		//empty name and password
		userDTO.setName("");
		userDTO.setPassword("");
		userDTO.setConfirmationPassword("12345");
		Assert.assertEquals(2, userService.validateEditProfileInput(userDTO).size());
		Assert.assertEquals(messageSource.getMessage("error.invalid-username", null, Locale.getDefault()), userService.validateEditProfileInput(userDTO).get(0).getMessage());
		Assert.assertEquals(messageSource.getMessage("error.invalid-password", null, Locale.getDefault()), userService.validateEditProfileInput(userDTO).get(1).getMessage());
	}
	
	@Test
	public void testEditProfileInputWithNullData(){
		Assert.assertEquals(messageSource.getMessage("error.no-input", null, Locale.getDefault()), userService.validateEditProfileInput(null).get(0).getMessage());
	}
	
	@Test
	public void testGetAllUsersCount(){
		Mockito.when(userRepository.count()).thenReturn(new Long(10));
		Assert.assertEquals(userService.getAllUsersCount(), new Integer(10));
	}
	
	@Test
	public void testGetUsersCount(){
		Mockito.when(userRepository.findUsersCount()).thenReturn(new Integer(10));
		Assert.assertEquals(userService.getUsersCount(), new Integer(10));
	}
	
	@Test
	public void testGetAllAdmins(){
		Mockito.when(userRepository.findByRole(UserRole.ADMIN))
				.thenReturn(new LinkedList<User>());
		Assert.assertTrue(userService.getAllAdmins().isEmpty());
	}
	
	@Test
	public void testGetAllUsers(){
		Mockito.when(userRepository.findByRole(UserRole.USER))
			.thenReturn(new LinkedList<User>());
		Assert.assertTrue(userService.getAllUsers().isEmpty());
	}
	
	@Test
	public void testGetAllUsersInDB(){
		Mockito.when(userRepository.findAll()).thenReturn(new LinkedList<User>());
		Assert.assertTrue(userService.getAllUsersInDB().isEmpty());
	}
	
	@Test
	public void testGetByEmail(){
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		Assert.assertEquals(userService.getByEmail("test1@abv.bg"), user);
	}
	
	@Test
	public void testGetUserByIdWithValidData(){
		Mockito.when(userRepository.findOne(1)).thenReturn(user);
		Map<UserDTO, List<Error>> result = userService.getUserById(1);
		UserDTO userDTOResult = (UserDTO)result.keySet().toArray()[0];
		
		Assert.assertFalse(result.keySet().isEmpty());
		Assert.assertNotNull(userDTOResult);
		Assert.assertTrue(result.get(userDTOResult).isEmpty());
	}
	
	@Test
	public void testGetUserByIdWithInvalidData(){
		Mockito.when(userRepository.findOne(-1)).thenReturn(null);
		Map<UserDTO, List<Error>> result = userService.getUserById(1);
		UserDTO userDTOResult = (UserDTO)result.keySet().toArray()[0];
		
		Assert.assertFalse(result.keySet().isEmpty());
		Assert.assertNull(userDTOResult);
		Assert.assertFalse(result.get(userDTOResult).isEmpty());
		Assert.assertEquals(result.get(userDTOResult).get(0).getMessage(), messageSource.getMessage("error.no-such-user", null, Locale.getDefault()));
	}
	
	@Test
	public void testGetUserByEmailWithValidData(){
		Startup startup = new Startup();
		startup.setId(1);
		startup.setName("StartupTest1");
		startup.setEmail("test1startup@abv.bg");
		startup.setPhone("0896480123");
		startup.setVat("0000");
		startup.setWebsite("www.xseed-java.com");
		Country country = new Country();
		country.setId(1);
		country.setName("Bulgaria");
		startup.setCountry(country);
		startup.setUsers(new LinkedList<User>());
		startup.getUsers().add(user);
		user.setStartups(new LinkedList<Startup>());
		user.getStartups().add(startup);
		
		Mockito.when(userRepository.findByEmail("test1@abv.bg")).thenReturn(user);
		Mockito.when(startupService.convertStartupToStartupDTO(startup)).thenReturn(startupDTO);
		
		Map<UserDTO, List<Error>> result = userService.getUserByEmail("test1@abv.bg");
		UserDTO userDTOResult = (UserDTO)result.keySet().toArray()[0];
		Assert.assertFalse(result.keySet().isEmpty());
		Assert.assertNotNull(userDTOResult);
		Assert.assertEquals(userDTOResult.getName(), "Test1");
		Assert.assertNotNull(userDTOResult.getStartup());
		Assert.assertEquals(userDTOResult.getStartup().getName(), "StartupTest1");
		Assert.assertTrue(result.get(userDTOResult).isEmpty());
	}
	
	@Test
	public void testGetUserByEmailWithInvalidData(){
		Startup startup = new Startup();
		startup.setId(1);
		startup.setName("StartupTest1");
		startup.setEmail("test1startup@abv.bg");
		startup.setPhone("0896480123");
		startup.setVat("0000");
		startup.setWebsite("www.xseed-java.com");
		Country country = new Country();
		country.setId(1);
		country.setName("Bulgaria");
		startup.setCountry(country);
		startup.setUsers(new LinkedList<User>());
		startup.getUsers().add(user);
		user.setStartups(new LinkedList<Startup>());
		user.getStartups().add(startup);
		//null email
		Mockito.when(userRepository.findByEmail(null)).thenReturn(null);
		Mockito.when(startupService.convertStartupToStartupDTO(startup)).thenReturn(startupDTO);
		
		Map<UserDTO, List<Error>> result = userService.getUserByEmail(null);
		UserDTO userDTOResult = (UserDTO)result.keySet().toArray()[0];
		Assert.assertFalse(result.keySet().isEmpty());
		Assert.assertNull(userDTOResult);
		Assert.assertFalse(result.get(userDTOResult).isEmpty());
		Assert.assertEquals(result.get(userDTOResult).get(0).getMessage(), messageSource.getMessage("error.invalid-email", null, Locale.getDefault()));
		
		//empty email
		Mockito.when(userRepository.findByEmail("")).thenReturn(null);
		result = userService.getUserByEmail("");
		userDTOResult = (UserDTO)result.keySet().toArray()[0];
		Assert.assertFalse(result.keySet().isEmpty());
		Assert.assertNull(userDTOResult);
		Assert.assertFalse(result.get(userDTOResult).isEmpty());
		Assert.assertEquals(result.get(userDTOResult).get(0).getMessage(), messageSource.getMessage("error.invalid-email", null, Locale.getDefault()));
	}
	
	@Test
	public void testGetUserByEmailWithNullUser(){
		Mockito.when(userRepository.findByEmail("ivane@abv.bg")).thenReturn(null);
		Mockito.when(messageSource.getMessage("error.no-such-user", null, Locale.getDefault())).thenReturn("aaaaaa");
		Map<UserDTO, List<Error>> map = userService.getUserByEmail("ivane@abv.bg");
		Assert.assertNotNull(map);
		Assert.assertTrue(map.entrySet().iterator().next().getValue().get(0).getMessage().equals("aaaaaa"));
	}
	
	@Test
	public void testGetUserByEmailWithMemberships(){
		User user = new User();
		user.setEmail("ivane@abv.bg");
		user.setIsActive(true);
		user.setRole(UserRole.USER);
		user.setMemberships(Arrays.asList(new Membership()));
		
		Mockito.when(userRepository.findByEmail("ivane@abv.bg")).thenReturn(user);

		Map<UserDTO, List<Error>> map = userService.getUserByEmail("ivane@abv.bg");
		Assert.assertNotNull(map);
		Assert.assertTrue(map.entrySet().iterator().next().getKey().getEmail().equals("ivane@abv.bg"));
		Assert.assertTrue(map.entrySet().iterator().next().getValue().isEmpty());
	}
	
	@Test
	public void testUpdateUserInfo_nullUser(){
		Mockito.when(userRepository.save(any(User.class))).thenReturn(null);
		Assert.assertTrue(userService.updateUserInfo(null, new UserDTO(), new User()) == null);
	}
	
	@Test
	public void testUpdateUserInfo_fullUser(){
		UserDTO user = new UserDTO();
		user.setName("Ivan");
		user.setPassword("123456");
		Mockito.when(userRepository.save(any(User.class))).thenReturn(new User());
		
		UserDTO updated = userService.updateUserInfo(user, new UserDTO(), new User());
		Assert.assertTrue(updated != null);
		Assert.assertTrue(updated.getName().equals("Ivan"));
	}
	
	@Test
	public void testUpdateUserInfo_fullUsernull(){
		UserDTO user = new UserDTO();
		Mockito.when(userRepository.save(any(User.class))).thenReturn(new User());
		
		UserDTO updated = userService.updateUserInfo(user, new UserDTO(), new User());
		Assert.assertTrue(updated != null);
		Assert.assertTrue(updated.getName() == null);
	}
	
	@Test
	public void testcreateUserDTO_nullUser(){
		Assert.assertTrue(userService.createUserDTO(null) == null);
	}
	
	@Test
	public void testcreateUserDTO(){
		Mockito.when(userRepository.save(any(User.class))).thenReturn(new User());
		Assert.assertTrue(userService.createUserDTO(new UserDTO()) != null);
	}
	
	@Test
	public void testgeneratePasswordToken_null(){
		Assert.assertTrue(userService.generatePasswordToken(null) == null);
	}
	
	@Test
	public void testgeneratePasswordToken(){
		UserDTO user = new UserDTO();
		user.setEmail("ivane@abv.bg");
		user.setName("Ivan");
		LocalDate d = LocalDate.now();
		String KEY = "Th30RyXx533D";
		String token = "ivane@abv.bg" + KEY + "Ivan" + KEY + d.getDayOfMonth() + "-" + d.getMonthValue() + "-" + d.getDayOfYear();
		String hash = DigestUtils.md5Hex(token);
		String actualHash = userService.generatePasswordToken(user);
		Assert.assertTrue(actualHash.equals(hash));
	}
	
	@Test
	public void testgenerateActivationToken_null(){
		Assert.assertTrue(userService.generateActivationToken(null) == null);
	}
	
	@Test
	public void testgenerateActivationToken(){
		UserDTO user = new UserDTO();
		user.setEmail("ivane@abv.bg");
		LocalDate d = LocalDate.now();
		String KEY = "Th30RyXx533D";
		String token = "ivane@abv.bg" + KEY + d.getDayOfMonth() + "-" + d.getMonthValue() + "-" + d.getDayOfYear() + KEY;
		String hash = DigestUtils.md5Hex(token);
		String actualHash = userService.generateActivationToken(user);
		Assert.assertTrue(actualHash.equals(hash));
	}
	
	@Test
	public void testgenerateProtectionToken(){
		String PROTECTION = "Pr0J3cT3uR0p3";
		String toEncode = PROTECTION + "-" + "ivane@abv.bg" + "-" + PROTECTION;
		String encoded = DigestUtils.md5Hex(toEncode);
		String actualHash = userService.generateProtectionToken("ivane@abv.bg");
		Assert.assertTrue(actualHash.equals(encoded));
	}
	
	@Test
	public void testconvertUserToLightUserDTO_null(){
		Assert.assertTrue(userService.convertUserToLightUserDTO(null) == null);
	}
	
	@Test
	public void testconvertUserToLightUserDTO_nullfields(){
		User user = new User();
		Assert.assertTrue(userService.convertUserToLightUserDTO(user) == null);
	}
	
	@Test
	public void testconvertUserToLightUserDTO(){
		User user = new User();
		user.setId(4);
		user.setEmail("ivane@abv.bg");
		user.setName("Ivan");
		UserDTO newUser = userService.convertUserToLightUserDTO(user);
		Assert.assertTrue(newUser != null);
		Assert.assertTrue(newUser.getEmail().equals("ivane@abv.bg"));
	}
	
	@Test
	public void testcreateDummyDTO(){
		User user = new User();
		user.setEmail("ivane@abv.bg");
		Mockito.when(userRepository.save(any(User.class))).thenReturn(user);
		UserDTO userDTO = userService.createDummyDTO("ivane@abv.bg");
		Assert.assertTrue(userDTO != null);
		Assert.assertTrue(userDTO.getEmail().equals(user.getEmail()));
	}
	
	@Test
	public void testconvertUserDTOToUser_null(){
		Assert.assertNull(userService.convertUserDTOToUser(null));
	}
	
	@Test
	public void testgetUserDTOfromUser_null(){
		Assert.assertNull(userService.getUserDTOfromUser(null));
	}
	
	@Test
	public void testgetUserDTOfromUser(){
		User user = new User();
		user.setEmail("ivane@abv.bg");
		UserDTO dto = userService.getUserDTOfromUser(user);
		Assert.assertNotNull(dto);
		Assert.assertTrue(dto.getEmail().equals(user.getEmail()));
	}
	
	@Test
	public void testconvertUserToUserDTO_null(){
		Assert.assertNull(userService.convertUserToUserDTO(null));
	}
	
	@Test
	public void testconvertUserToUserDTO_nullmemb(){
		User user = new User();
		user.setEmail("ivane@abv.bg");
		UserDTO dto = userService.convertUserToUserDTO(user);
		Assert.assertNotNull(dto);
		Assert.assertTrue(dto.getEmail().equals(user.getEmail()));
	}
	
	@Test
	public void testconvertUserToUserDTO_membdef(){
		User user = new User();
		user.setEmail("ivane@abv.bg");
		Membership mem = new Membership();
		mem.setDefault(true);
		mem.setStartup(new Startup());
		user.setMemberships(Arrays.asList(mem));
		UserDTO dto = userService.convertUserToUserDTO(user);
		Assert.assertNotNull(dto);
		Assert.assertTrue(dto.getEmail().equals(user.getEmail()));
	}
	
	@Test
	public void testconvertUserToUserDTO_membnotdef(){
		User user = new User();
		user.setEmail("ivane@abv.bg");
		Membership mem = new Membership();
		mem.setDefault(false);
		mem.setStartup(new Startup());
		user.setMemberships(Arrays.asList(mem));
		UserDTO dto = userService.convertUserToUserDTO(user);
		Assert.assertNotNull(dto);
		Assert.assertTrue(dto.getEmail().equals(user.getEmail()));
	}

}
