package com.theoryx.xseed.unit.service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;

import static org.mockito.Matchers.any;

import com.theoryx.xseed.model.AnswerGroup;
import com.theoryx.xseed.model.AnswerOption;
import com.theoryx.xseed.model.KPIAdjustment;
import com.theoryx.xseed.model.Snapshot;
import com.theoryx.xseed.model.SnapshotLine;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.StartupCalculation;
import com.theoryx.xseed.repository.StartupCalculationRepository;
import com.theoryx.xseed.service.KPIAdjustmentService;
import com.theoryx.xseed.service.SnapshotlineService;
import com.theoryx.xseed.service.StartupCalculationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class StartupCalculationServiceTest {

	@InjectMocks
	private StartupCalculationServiceImpl startupCalculationService;
	@Mock
	private SnapshotlineService snapshotlineService;
	@Mock
	private KPIAdjustmentService kPIAdjustmentService;
	@Mock
	private StartupCalculationRepository startupCalculationRepository;
	
	@BeforeMethod
	public void initMocks(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void convertStartupCalculationToStartupCalculationDTOTest(){
		StartupCalculation startupCalc = new StartupCalculation();
		startupCalc.setKpi(10);
		startupCalc.setFormula_a(12.5);
		startupCalc.setFormula_b(12.5);
		startupCalc.setFormula_c(12.5);
		startupCalc.setFormula_d(12.5);
		Startup startup = new Startup();
		startup.setName("Startup");
		startupCalc.setStartup(startup);
		
		Assert.assertNull(startupCalculationService.convertStartupCalculationToStartupCalculationDTO(null));
		Assert.assertEquals("Startup", startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc).getStartupName());
		Assert.assertEquals(new Double(12.5), startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc).getFormula_a());
		Assert.assertEquals(new Integer(10), startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc).getKpi());
	}
	
	@Test
	public void convertStartupCalculationToStartupCalculationDTOTestWithEmptyData(){
		StartupCalculation startupCalc = new StartupCalculation();		
		Assert.assertNull(startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc));
	}
	
	@Test
	public void convertStartupCalculationToStartupCalculationDTOWithNullData(){
		StartupCalculation startupCalc = new StartupCalculation();
		startupCalc.setKpi(null);
		startupCalc.setFormula_a(null);
		startupCalc.setFormula_b(null);
		startupCalc.setFormula_c(null);
		startupCalc.setFormula_d(null);
		Startup startup = new Startup();
		startup.setName("Startup");
		startupCalc.setStartup(startup);
		
		Assert.assertNull(startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc));		
		startupCalc.setKpi(10);
		Assert.assertNull(startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc));		
		startupCalc.setFormula_a(12.5);
		Assert.assertNull(startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc));		
		startupCalc.setFormula_b(12.5);
		Assert.assertNull(startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc));		
		startupCalc.setFormula_c(12.5);
		Assert.assertNull(startupCalculationService.convertStartupCalculationToStartupCalculationDTO(startupCalc));		
	}
	
	@Test
	public void convertTestWithNull(){
		Assert.assertNull(startupCalculationService.convert(null));
	}
	
	@Test
	public void convertTestWithEmptyData(){
		List<StartupCalculation> startupCalculations = new LinkedList<StartupCalculation>();
		Assert.assertNull(startupCalculationService.convert(startupCalculations));
		startupCalculations.add(new StartupCalculation());
		Assert.assertNull(startupCalculationService.convert(startupCalculations).get(0));
	}
	
	@Test
	public void convertTestWithValidData(){
		List<StartupCalculation> startupCalculations = new LinkedList<StartupCalculation>();
		StartupCalculation startupCalc = new StartupCalculation();
		startupCalc.setKpi(10);
		startupCalc.setFormula_a(12.5);
		startupCalc.setFormula_b(12.5);
		startupCalc.setFormula_c(12.5);
		startupCalc.setFormula_d(12.5);
		Startup startup = new Startup();
		startup.setName("Startup");
		startupCalc.setStartup(startup);
		startupCalculations.add(startupCalc);
		
		Assert.assertEquals("Startup",startupCalculationService.convert(startupCalculations).get(0).getStartupName());	
	}
	
	@Test
	public void createStartupCalculationLine_null(){
		Assert.assertNull(startupCalculationService.createStartupCalculationLine(null, null, null, null));
	}
	
	@Test
	public void createStartupCalculationLine(){
		SnapshotLine line = new SnapshotLine();
		line.setSelected_answer(new AnswerOption(5, "", "", new AnswerGroup()));
		Mockito.when(snapshotlineService.findByQuestionIdAndSnapshotId(any(Integer.class), any(Integer.class))).thenReturn(line);
		
		KPIAdjustment kpi = new KPIAdjustment();
		kpi.setValue(100000);
		Mockito.when(kPIAdjustmentService.findByAnswerIdAndQuestionId(any(Integer.class), any(Integer.class))).thenReturn(kpi);
		
		Snapshot shot = new Snapshot();
		shot.setId(3);
		shot.setStartup(new Startup());
		
		List<Snapshot> shots = Arrays.asList(new Snapshot(), new Snapshot());
		Mockito.when(kPIAdjustmentService.sumOfAllKpis(shots, 1)).thenReturn(100000);
		StartupCalculation sc = new StartupCalculation();
		Mockito.when(startupCalculationRepository.save(any(StartupCalculation.class))).thenReturn(sc);
		
		StartupCalculation result = startupCalculationService.createStartupCalculationLine(null, shots, shot, new Integer(1));
		Assert.assertNotNull(result);
	}
	
	@Test
	public void createStartupCalculations(){
		SnapshotLine line = new SnapshotLine();
		line.setSelected_answer(new AnswerOption(5, "", "", new AnswerGroup()));
		Mockito.when(snapshotlineService.findByQuestionIdAndSnapshotId(any(Integer.class), any(Integer.class))).thenReturn(line);
		
		KPIAdjustment kpi = new KPIAdjustment();
		kpi.setValue(10000);
		Mockito.when(kPIAdjustmentService.findByAnswerIdAndQuestionId(any(Integer.class), any(Integer.class))).thenReturn(kpi);
		
		Snapshot shot1 = new Snapshot();
		shot1.setId(3);
		shot1.setStartup(new Startup());
		Snapshot shot2 = new Snapshot();
		shot2.setId(34);
		shot2.setStartup(new Startup());
		List<Snapshot> shots = Arrays.asList(shot1, shot2);
		
		List<Snapshot> fshots = Arrays.asList(new Snapshot(), new Snapshot());
		Mockito.when(kPIAdjustmentService.sumOfAllKpis(fshots, 1)).thenReturn(100000);
		StartupCalculation sc = new StartupCalculation();
		
		Double formulaA = (double) kpi.getValue() / (double) 100000;
		Double formulaB = formulaA * (double)fshots.size();
		Double formulaC = Math.log(formulaB);
		Double formulaD = formulaA * formulaC;
		
		sc.setFormula_d(formulaD);
		Mockito.when(startupCalculationRepository.save(any(StartupCalculation.class))).thenReturn(sc);
		
		Double result = startupCalculationService.createStartupCalculations(null, fshots, shots, new Integer(1));
		Assert.assertNotNull(result);
		Assert.assertTrue(result == (formulaD*2));
	}
}

