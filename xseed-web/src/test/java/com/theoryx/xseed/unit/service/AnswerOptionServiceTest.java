package com.theoryx.xseed.unit.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import com.theoryx.xseed.dto.AnswerOptionDTO;
import com.theoryx.xseed.model.AnswerGroup;
import com.theoryx.xseed.model.AnswerOption;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.repository.AnswerOptionRepository;
import com.theoryx.xseed.service.AnswerOptionServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AnswerOptionServiceTest {

	@InjectMocks
	private AnswerOptionServiceImpl answerOptionService;
	@Mock
	private AnswerOptionRepository answerOptionRepository;
	
	@Test
	public void test_getAnswerOptions_null() {
		Assert.isNull(answerOptionService.getAnswerOptions(null));
	}
	
	@Test
	public void test_getAnswerOptions_notnull() {
		Question question = new Question();
		question.setAnswergroup(new AnswerGroup());
		Mockito.when(answerOptionRepository.findByGroupId(null)).thenReturn(new ArrayList<AnswerOption>());
		Assert.notNull(answerOptionService.getAnswerOptions(question));
	}
	
	@Test
	public void test_getAnswerOptionDtos_null() {
		Assert.isNull(answerOptionService.getAnswerOptionDtos(null));
	}
	
	@Test
	public void test_getAnswerOptionDtos_notnull() {
		Question question = new Question();
		question.setAnswergroup(new AnswerGroup());
		List<AnswerOption> list = new ArrayList<AnswerOption>();
		list.add(new AnswerOption());
		Mockito.when(answerOptionRepository.findByGroupId(null)).thenReturn(list);
		Assert.notNull(answerOptionService.getAnswerOptionDtos(question));
	}
	
	@Test
	public void test_findByText() {
		Mockito.when(answerOptionRepository.findByText("text")).thenReturn(new AnswerOption());
		Assert.notNull(answerOptionService.findByText("text"));
		Assert.isTrue(answerOptionService.findByText("text").getClass().getSimpleName().equals("AnswerOption"));
	}
	
	@Test
	public void test_findById() {
		Mockito.when(answerOptionRepository.findOne(null)).thenReturn(new AnswerOption());
		Assert.notNull(answerOptionService.findById(null));
		Assert.isTrue(answerOptionService.findById(null).getClass().getSimpleName().equals("AnswerOption"));
	}
	
	@Test
	public void test_getAnswerOptionOtherId() {
		Mockito.when(answerOptionRepository.findByTextLike("Other")).thenReturn(new HashSet<AnswerOption>());
		Assert.notNull(answerOptionService.getAnswerOptionOtherId());
	}
	
	@Test
	public void test_isOtherChecked_false() {
		Mockito.when(answerOptionRepository.findByTextLike("Other")).thenReturn(new HashSet<AnswerOption>());
		Assert.isTrue(answerOptionService.isOtherChecked("1") == false);
	}
	
	@Test
	public void test_isOtherChecked_true() {
		AnswerOption ao = new AnswerOption();
		ao.setId(1);
		Set<AnswerOption> set = new HashSet<AnswerOption>();
		set.add(ao);
		Mockito.when(answerOptionRepository.findByTextLike("Other")).thenReturn(set);
		Assert.isTrue(answerOptionService.isOtherChecked("1") == true);
	}

	@Test
	public void test_convertAnswerOptionToAnswerOptionDto_ok() {
		AnswerOption answerOption = new AnswerOption(55, "ans55", "ergserf", new AnswerGroup());
		AnswerOptionDTO dto = answerOptionService.convertAnswerOptionToAnswerOptionDto(answerOption);
		Assert.isTrue(dto.getClass().getSimpleName().equals("AnswerOptionDTO"));
		Assert.isTrue(dto.getIdentifier().equals(answerOption.getIdentifier()));
		Assert.isTrue(dto.getId().equals(answerOption.getId()));
		Assert.isTrue(dto.getText().equals(answerOption.getText()));
	}
	
	@Test
	public void test_convert_ok() {
		List<AnswerOption> answerOptions = new ArrayList<AnswerOption>();
		answerOptions.add(new AnswerOption(1, "ans11", "ergserf", new AnswerGroup()));
		answerOptions.add(new AnswerOption(3, "ans33", "ergsergserf", new AnswerGroup()));
		answerOptions.add(new AnswerOption(4, "ans44", "errynyugserf", new AnswerGroup()));
		
		List<AnswerOptionDTO> dtos = answerOptionService.convert(answerOptions);
		Assert.isTrue(dtos.size() == 3);
		for (int i = 0; i < dtos.size(); i++) {
			Assert.isTrue(dtos.get(i).getClass().getSimpleName().equals("AnswerOptionDTO"));
			Assert.isTrue(dtos.get(i).getId().equals(answerOptions.get(i).getId()));
			Assert.isTrue(dtos.get(i).getIdentifier().equals(answerOptions.get(i).getIdentifier()));
			Assert.isTrue(dtos.get(i).getText().equals(answerOptions.get(i).getText()));
		}
	}
	
	@Test
	public void test_convertAnswerOptionToAnswerOptionDto_null_answeroption() {
		AnswerOptionDTO dto = answerOptionService.convertAnswerOptionToAnswerOptionDto(null);
		Assert.isTrue(dto == null);
	}
	
	@Test
	public void test_convert_null_list() {
		List<AnswerOptionDTO> dtos = answerOptionService.convert(null);
		Assert.isTrue(dtos == null);
	}
	
	@Test
	public void test_convert_empty_list() {
		List<AnswerOptionDTO> dtos = answerOptionService.convert(new ArrayList<AnswerOption>());
		Assert.isTrue(dtos.isEmpty());
	}

}
