package com.theoryx.xseed.unit.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.annotations.BeforeMethod;

import com.theoryx.xseed.dto.SurveyDTO;
import com.theoryx.xseed.model.Survey;
import com.theoryx.xseed.repository.SurveyRepository;
import com.theoryx.xseed.service.SurveyQuestionService;
import com.theoryx.xseed.service.SurveyServiceImpl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;

@RunWith(MockitoJUnitRunner.class)
public class SurveyServiceTest {

	@Mock
	private SurveyRepository surveyRepository;
	@Mock
	private SurveyQuestionService surveyQuestionService;
	@InjectMocks
	private SurveyServiceImpl surveyService;

	private final static int SURVEY_ID = 1;
	
	@BeforeMethod
	public void initMocks(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getByIdTest(){
		Survey survey = new Survey();
		survey.setId(SURVEY_ID);
		Mockito.when(surveyRepository.findOne(SURVEY_ID)).thenReturn(survey);
		Assert.assertEquals(survey, surveyService.getById(SURVEY_ID));
	}

	@Test
	public void getByIdTestWithNull(){
		Mockito.when(surveyRepository.findOne(null)).thenReturn(null);
		Assert.assertEquals(null, surveyService.getById(null));
	}
	
	@Test
	public void getByIdTestWithWrongID(){
		Mockito.when(surveyRepository.findOne(-1)).thenReturn(null);
		Assert.assertEquals(null, surveyService.getById(-1));
	}
	
	@Test
	public void getAllSurveysTest(){
		List<Survey> surveys = new LinkedList<Survey>();
		surveys.add(new Survey());
		Mockito.when(surveyRepository.findAll()).thenReturn(surveys);
		Assert.assertEquals(surveys, surveyService.getAllSurveys());
	}
	
	@Test
	public void convertSurveyToSurveyDTO_null(){
		Assert.assertNull(surveyService.convertSurveyToSurveyDTO(null));
	}
	
	@Test
	public void convertSurveyToSurveyDTO_nullname(){
		Survey survey = new Survey();
		Mockito.when(surveyQuestionService.getNumberOfSurveyQuestions(survey)).thenReturn(5);
		SurveyDTO surveyDto = surveyService.convertSurveyToSurveyDTO(survey);
		Assert.assertNotNull(surveyDto);
		Assert.assertTrue(surveyDto.getName() == null);
	}
	
	@Test
	public void convertSurveyToSurveyDTO(){
		Survey survey = new Survey();
		survey.setName("name");
		Mockito.when(surveyQuestionService.getNumberOfSurveyQuestions(survey)).thenReturn(5);
		SurveyDTO surveyDto = surveyService.convertSurveyToSurveyDTO(survey);
		Assert.assertNotNull(surveyDto);
		Assert.assertTrue(surveyDto.getName().equals("name"));
	}
	
	@Test
	public void getAllSurveyDtos_null(){
		Mockito.when(surveyRepository.findAll()).thenReturn(null);
		Assert.assertNull(surveyService.getAllSurveyDtos());
	}
	
	@Test
	public void getAllSurveyDtos(){
		Survey survey = new Survey();
		Mockito.when(surveyRepository.findAll()).thenReturn(Arrays.asList(survey));
		Mockito.when(surveyQuestionService.getNumberOfSurveyQuestions(survey)).thenReturn(5);
		Assert.assertNotNull(surveyService.getAllSurveyDtos());
	}
	
}
