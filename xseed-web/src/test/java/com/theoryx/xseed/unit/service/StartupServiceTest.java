package com.theoryx.xseed.unit.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;

import static org.junit.Assert.*;

import com.theoryx.xseed.dto.CountryDTO;
import com.theoryx.xseed.dto.MembershipDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.repository.StartupRepository;
import com.theoryx.xseed.service.CountryService;
import com.theoryx.xseed.service.MailService;
import com.theoryx.xseed.service.MembershipService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.StartupServiceImpl;
import com.theoryx.xseed.service.UserService;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;

@RunWith(MockitoJUnitRunner.class)
public class StartupServiceTest {

	@Mock
	private StartupRepository startupRepository;
	@Mock
	private MembershipService membershipService;
	@Mock
	private UserService userService;
	@Mock
	private CountryService countryService;
	@Mock
	private MessageSource messageSource;
	@Mock
	private MailService mailService;
	@Mock
	private StartupService startupS;
	
	@InjectMocks
	private StartupServiceImpl startupService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("locale/language");
		startupService.setMessageSource(messageSource);
	}

	@Test
	public void test_normalizeStartup() {
		test_normalizeStartup_null_startupdto();
		test_normalizeStartup_null_countrydto();
		test_normalizeStartup_null_countrydto_name();
		test_normalizeStartup_ok();
	}

	@Test
	public void test_validateStartupInfo() {
		test_validateStartupInfo_null_startupdto();
		test_validateStartupInfo_null_startupdto_fields();
		test_validateStartupInfo_ok();
	}

	@Test
	public void test_updateStartup() {
		test_updateStartup_null_startupdto();
		test_updateStartup_null_startup();
		test_updateStartup_ok();
	}

	@Test
	public void test_addUser() {
		test_addUser_null_user();
		test_addUser_null_startup();
		test_addUser_membership_exeption();
		test_addUser_ok();
	}

	@Test
	public void testValidateStartupInfoWithInvalidFields(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setName("");
		startupDTO.setEmail("a@*.b");
		startupDTO.setPhone("05");
		startupDTO.setWebsite("www.----.*****");
		startupDTO.setVat("1");
		Assert.assertEquals(5, startupService.validateStartupInfo(startupDTO).size());
	}
	
	@Test
	public void addExistingActiveTestStartupContainsUser(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		startupDTO.getUsers().add(existingUserDTO);
		List<Error> errors = new LinkedList<Error>();
		
		Mockito.when(messageSource.getMessage("error.user-already-added", new Object[0], Locale.getDefault())).thenReturn("The user is already added");
		startupService.addExistingActive(startupDTO, currentUserDTO, existingUserDTO, errors, email);
		Assert.assertEquals("The user is already added", errors.get(0).getMessage());
	}
	
	@Test
	public void addExistingActiveTestStartupNotContaingUser(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setName("CurrentUser1");
		currentUserDTO.setStartup(startupDTO);
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		List<Error> errors = new LinkedList<Error>();
		
		User user = new User();
		Startup startup = new Startup();
		Membership membership = new Membership();
		
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(existingUserDTO, null);
		Mockito.when(userService.convertUserDTOToUser(existingUserDTO)).thenReturn(user);
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(existingUserDTO);
		Mockito.when(countryService.convertCountryDTOToCountry(any(CountryDTO.class))).thenReturn(null);
		Mockito.when(membershipService.convertMembershipToMembershipDTO(any(Membership.class))).thenReturn(new MembershipDTO());
		Mockito.when(startupS.convertStartupDTOToStartup(startupDTO)).thenReturn(startup);
		Mockito.when(membershipService.createMembership(user, startup)).thenReturn(membership);
		Mockito.when(startupS.addUserToStartup(existingUserDTO, startupDTO)).thenReturn(result);
		Mockito.when(mailService.sendNotificationEmailForStartup(MailService.FROM, email, MailService.NOTIFICATION_SUBJECT, "CurrentUser1", startupDTO.getName())).thenReturn(true);
		
		startupService.addExistingActive(startupDTO, currentUserDTO, existingUserDTO, errors, email);
		Assert.assertEquals(existingUserDTO, currentUserDTO.getStartup().getUsers().get(0));
	}
	
	@Test
	public void addExistingActiveUserMailError(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setName("CurrentUser1");
		currentUserDTO.setStartup(startupDTO);
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		List<Error> errors = new LinkedList<Error>();
		
		User user = new User();
		Startup startup = new Startup();
		Membership membership = new Membership();
		
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(existingUserDTO, null);
		Mockito.when(userService.convertUserDTOToUser(existingUserDTO)).thenReturn(user);
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(existingUserDTO);
		Mockito.when(countryService.convertCountryDTOToCountry(any(CountryDTO.class))).thenReturn(null);
		Mockito.when(membershipService.convertMembershipToMembershipDTO(any(Membership.class))).thenReturn(new MembershipDTO());
		Mockito.when(startupS.convertStartupDTOToStartup(startupDTO)).thenReturn(startup);
		Mockito.when(membershipService.createMembership(user, startup)).thenReturn(membership);
		Mockito.when(startupS.addUserToStartup(existingUserDTO, startupDTO)).thenReturn(result);
		Mockito.when(mailService.sendNotificationEmailForStartup(MailService.FROM, email, MailService.NOTIFICATION_SUBJECT, "CurrentUser1", startupDTO.getName())).thenReturn(false);
		Mockito.doNothing().when(membershipService).deleteMembership(any(MembershipDTO.class));
		startupService.addExistingActive(startupDTO, currentUserDTO, existingUserDTO, errors, email);
		Assert.assertEquals(1, errors.size());
	}
	
	@Test
	public void addExistingActiveUserNullParams(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setName("CurrentUser1");
		currentUserDTO.setStartup(startupDTO);
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		List<Error> errors = new LinkedList<Error>();
		
		User user = new User();
		Startup startup = new Startup();
		Membership membership = new Membership();
		
		errors.add(new Error("test"));
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(null, errors);
		Mockito.when(userService.convertUserDTOToUser(existingUserDTO)).thenReturn(user);
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(existingUserDTO);
		Mockito.when(countryService.convertCountryDTOToCountry(any(CountryDTO.class))).thenReturn(null);
		Mockito.when(membershipService.convertMembershipToMembershipDTO(any(Membership.class))).thenReturn(new MembershipDTO());
		Mockito.when(startupS.convertStartupDTOToStartup(startupDTO)).thenReturn(startup);
		Mockito.when(membershipService.createMembership(user, startup)).thenReturn(membership);
		Mockito.when(startupS.addUserToStartup(null, startupDTO)).thenReturn(result);
		
		startupService.addExistingActive(startupDTO, currentUserDTO, null, errors, email);
		Assert.assertEquals(1, errors.size());
	}
	

	@Test
	public void addExistingInactiveTestStartupContainsUser(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		startupDTO.getUsers().add(existingUserDTO);
		List<Error> errors = new LinkedList<Error>();
		
		Mockito.when(messageSource.getMessage("error.user-already-added", new Object[0], Locale.getDefault())).thenReturn("The user is already added");
		startupService.addExistingInactive(startupDTO, currentUserDTO, existingUserDTO, errors, email);
		Assert.assertEquals("The user is already added", errors.get(0).getMessage());
	}
	
	@Test
	public void addExistingInactiveTestStartupNotContaingUser(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setName("CurrentUser1");
		currentUserDTO.setStartup(startupDTO);
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		List<Error> errors = new LinkedList<Error>();
		
		User user = new User();
		Startup startup = new Startup();
		Membership membership = new Membership();
		
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(existingUserDTO, null);
		
		Mockito.when(userService.convertUserDTOToUser(existingUserDTO)).thenReturn(user);
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(existingUserDTO);
		Mockito.when(countryService.convertCountryDTOToCountry(any(CountryDTO.class))).thenReturn(null);
		Mockito.when(membershipService.convertMembershipToMembershipDTO(any(Membership.class))).thenReturn(new MembershipDTO());
		Mockito.when(startupS.convertStartupDTOToStartup(startupDTO)).thenReturn(startup);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("123");
		Mockito.when(membershipService.createMembership(user, startup)).thenReturn(membership);
		Mockito.when(startupS.addUserToStartup(existingUserDTO, startupDTO)).thenReturn(result);
		Mockito.when(mailService.sendInvitationLinkForStartup(MailService.FROM, email,
				MailService.STARTUP_INVITATION_SUBJECT, "123", startupDTO.getName())).thenReturn(true);
		
	
		startupService.addExistingInactive(startupDTO, currentUserDTO, existingUserDTO, errors, email);
		Assert.assertEquals(existingUserDTO, currentUserDTO.getStartup().getUsers().get(0));		
	}
	
	@Test
	public void addExistingInactiveNullParams(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setName("CurrentUser1");
		currentUserDTO.setStartup(startupDTO);
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		List<Error> errors = new LinkedList<Error>();
		
		User user = new User();
		Startup startup = new Startup();
		Membership membership = new Membership();
		errors.add(new Error("Error"));
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(null, errors);
		
		Mockito.when(userService.convertUserDTOToUser(existingUserDTO)).thenReturn(user);
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(existingUserDTO);
		Mockito.when(countryService.convertCountryDTOToCountry(any(CountryDTO.class))).thenReturn(null);
		Mockito.when(membershipService.convertMembershipToMembershipDTO(any(Membership.class))).thenReturn(new MembershipDTO());
		Mockito.when(startupS.convertStartupDTOToStartup(startupDTO)).thenReturn(startup);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("123");
		Mockito.when(membershipService.createMembership(user, startup)).thenReturn(membership);
		Mockito.when(startupS.addUserToStartup(null, startupDTO)).thenReturn(result);
		Mockito.when(mailService.sendInvitationLinkForStartup(MailService.FROM, email,
				MailService.STARTUP_INVITATION_SUBJECT, "123", startupDTO.getName())).thenReturn(true);
		
	
		startupService.addExistingInactive(startupDTO, currentUserDTO, null, errors, email);
		Assert.assertEquals(1, errors.size());		
	}
	
	@Test
	public void addExistingInactiveTestMailError(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setName("CurrentUser1");
		currentUserDTO.setStartup(startupDTO);
		UserDTO existingUserDTO = new UserDTO();
		existingUserDTO.setName("Test1");
		String email = "tets1@abv.bg";
		existingUserDTO.setEmail(email);
		existingUserDTO.setId(1);
		List<Error> errors = new LinkedList<Error>();
		
		User user = new User();
		Startup startup = new Startup();
		Membership membership = new Membership();
		
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(existingUserDTO, null);
		
		Mockito.when(userService.convertUserDTOToUser(existingUserDTO)).thenReturn(user);
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(existingUserDTO);
		Mockito.when(countryService.convertCountryDTOToCountry(any(CountryDTO.class))).thenReturn(null);
		Mockito.when(membershipService.convertMembershipToMembershipDTO(any(Membership.class))).thenReturn(new MembershipDTO());
		Mockito.when(startupS.convertStartupDTOToStartup(startupDTO)).thenReturn(startup);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("123");
		Mockito.when(membershipService.createMembership(user, startup)).thenReturn(membership);
		Mockito.when(startupS.addUserToStartup(existingUserDTO, startupDTO)).thenReturn(result);
		Mockito.when(mailService.sendInvitationLinkForStartup(MailService.FROM, email,
				MailService.STARTUP_INVITATION_SUBJECT, "123", startupDTO.getName())).thenReturn(false);
		Mockito.doNothing().when(membershipService).deleteMembership(any(MembershipDTO.class));
		Mockito.when(userService.deleteUser(any(UserDTO.class))).thenReturn(true);
		startupService.addExistingInactive(startupDTO, currentUserDTO, existingUserDTO, errors, email);
		Assert.assertEquals(1, errors.size());
	}
	
	@Test
	public void addNonExistingUserTestWithErrors(){
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUserDTO = new UserDTO();
		String email = "test1@abv.bg";
		List<Error> errors = new LinkedList<Error>();
		
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(null, errors);
		Mockito.when(userService.createDummyDTO(email)).thenReturn(new UserDTO());
		Mockito.when(startupS.addUserToStartup(any(UserDTO.class), any(StartupDTO.class))).thenReturn(result);
		Mockito.when(startupS.addUserToStartup(any(UserDTO.class), any(StartupDTO.class))).thenReturn(result);
		boolean addResult = startupService.addNonExistigUser(startupDTO, currentUserDTO, errors, email);
		Assert.assertFalse(addResult);
	}
	
	@Test
	public void addNonExistingUserTestWithoutErrors(){
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setUsers(new LinkedList<UserDTO>());
		startupDTO.setName("Name");
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setStartup(startupDTO);

		String email = "test1@abv.bg";
		List<Error> errors = new LinkedList<Error>();
		
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(currentUserDTO, null);
		Mockito.when(userService.createDummyDTO(email)).thenReturn(new UserDTO());
		Mockito.when(startupS.addUserToStartup(any(UserDTO.class), any(StartupDTO.class))).thenReturn(result);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("123");
		Mockito.when(mailService.sendInvitationLinkForStartup(MailService.FROM, email,
						MailService.STARTUP_INVITATION_SUBJECT, "123", startupDTO.getName())).thenReturn(true);
		boolean addResult = startupService.addNonExistigUser(startupDTO, currentUserDTO, errors, email);
		Assert.assertTrue(addResult);		
	}
	
	@Test
	public void testConvertWithNullList(){
		Assert.assertNull(startupService.convert(null));
	}
	
	@Test
	public void testConvert(){
		List<Startup> startups = new ArrayList<Startup>(10);
		Startup startup = new Startup();

		
		startups.add(startup);
		Assert.assertEquals(1, startupService.convert(startups).size());
	}
	
	@Test
	public void createStartupTestNull(){
		StartupDTO startupDto = new StartupDTO();
		Mockito.when(startupRepository.save(any(Startup.class))).thenReturn(null);
		Assert.assertNull(startupService.createStartup(startupDto));
	}
	
	@Test
	public void convertStartupDTOToStartupTestNull(){
		Assert.assertNull(startupService.convertStartupDTOToStartup(null));
	}
	
	@Test
	public void testIsCurrentStartupNameTrue(){
		StartupDTO currentStartup = new StartupDTO();
		currentStartup.setName("Test1");
		Assert.assertTrue(startupService.isCurrentStartupName("Test1", currentStartup));
	}
	
	@Test
	public void testIsCurrentStartupNameFalse(){
		StartupDTO currentStartup = new StartupDTO();
		currentStartup.setName("Test11");
		Assert.assertFalse(startupService.isCurrentStartupName("Test1", currentStartup));
	}
	
	@Test
	public void testStartupNameExistsTrue(){
		Mockito.when(startupRepository.findByName("test")).thenReturn(new Startup());
		Assert.assertTrue(startupService.startupNameExists("test"));
	}
	
	@Test
	public void testStartupNameExistsFalse(){
		Mockito.when(startupRepository.findByName("test")).thenReturn(null);
		Assert.assertFalse(startupService.startupNameExists("test"));
	}
	
	@Test
	public void testFindById(){
		Startup startup = new Startup();
		Mockito.when(startupRepository.findOne(1)).thenReturn(startup);
		Assert.assertEquals(startup, startupService.findById(1));
	}
	
	@Test
	public void testFindByName(){
		Startup startup = new Startup();
		Mockito.when(startupRepository.findByName("test")).thenReturn(startup);
		Assert.assertEquals(startup, startupService.findByName("test"));
	}
	
	@Test
	public void testGetPaginationStartups(){
		List<Startup> startups = new ArrayList<Startup>(5);
		Mockito.when(startupRepository.findPaginationStartups(5,0)).thenReturn(startups);
		Assert.assertEquals(startups, startupService.getPaginationStartups(5, 0));
	}
	
	@Test
	public void testGetAllStartupCount(){
		Mockito.when(startupRepository.count()).thenReturn((long) 10);
		Assert.assertEquals(new Integer(10), startupService.getAllStartupCount());
	}
	
	@Test
	public void testGetAllStartups(){
		List<Startup> startups = new ArrayList<Startup>(5);
		Mockito.when(startupRepository.findAll()).thenReturn(startups);
		Assert.assertEquals(startups, startupService.getAllStartups());
	}
	
	private void test_addUser_null_user() {
		Map<UserDTO, List<Error>> errs = startupService.addUser(null, new Startup());
		assertTrue(errs.containsKey(null));
		assertTrue(errs.get(null).get(0).getMessage().equals("User could not be created"));
	}

	private void test_addUser_null_startup() {
		Map<UserDTO, List<Error>> errs = startupService.addUser(new User(), null);
		assertTrue(errs.containsKey(null));
		assertTrue(errs.get(null).get(0).getMessage().equals("Startup does not exist"));
	}

	private void test_addUser_membership_exeption() {
		Mockito.when(membershipService.createMembership(any(User.class), any(Startup.class))).thenReturn(null);
		Map<UserDTO, List<Error>> errs = startupService.addUser(new User(), new Startup());
		assertTrue(errs.containsKey(null));
		assertTrue(errs.get(null).get(0).getMessage().equals("Can not create membership"));
	}

	private void test_addUser_ok() {
		User user = new User();
		Startup startup = new Startup();

		Membership membership = new Membership(user, startup, UserRole.USER);
		membership.setId(555);
		Mockito.when(membershipService.createMembership(any(User.class), any(Startup.class))).thenReturn(membership);

		Map<UserDTO, List<Error>> errs = startupService.addUser(user, startup);

		assertTrue(errs.size() == 1);
		Entry<UserDTO, List<Error>> entry = errs.entrySet().iterator().next();
		assertTrue(entry.getValue().isEmpty());
		assertTrue(entry.getKey() == null);
	}

	private void test_updateStartup_null_startupdto() {
		Map<StartupDTO, List<Error>> errs = startupService.updateStartup(null);
		assertTrue(errs.containsKey(null));
		assertTrue(errs.get(null).get(0).getMessage().equals("Null startup"));
	}

	private void test_updateStartup_ok() {
		StartupDTO startupdto = new StartupDTO("DodoTest", "dodotest@abv.bg", "012485263", "www.dodotest.com",
				"4125639870");
		startupdto.setId(55);
		Startup startupindb = new Startup("dbstartup", "dbstartup@abv.bg", "0452130689", "www.dbstartup.com",
				"7452103689");
		Startup newStartup = startupService.convertStartupDTOToStartup(startupdto);

		Mockito.when(startupRepository.findOne(any(Integer.class))).thenReturn(startupindb);
		Mockito.when(startupRepository.save(any(Startup.class))).thenReturn(newStartup);

		Map<StartupDTO, List<Error>> errs = startupService.updateStartup(startupdto);
		Assert.assertTrue(errs.size() == 1);
		Entry<StartupDTO, List<Error>> entry = errs.entrySet().iterator().next();
		StartupDTO dto = entry.getKey();
		Assert.assertTrue(dto != null);
		Assert.assertTrue(dto.getClass().getSimpleName().equals("StartupDTO"));
		Assert.assertTrue(entry.getValue().isEmpty());
		Assert.assertTrue(dto.getEmail().equals("dodotest@abv.bg"));
		Assert.assertTrue(dto.getName().equals("DodoTest"));
		Assert.assertTrue(dto.getPhone().equals("012485263"));
		Assert.assertTrue(dto.getVat().equals("4125639870"));
		Assert.assertTrue(dto.getWebsite().equals("www.dodotest.com"));
		Assert.assertTrue(dto.getId().equals(55));
	}
	
	private void test_updateStartup_null_startup() {
		Mockito.when(startupRepository.findOne(any(Integer.class))).thenReturn(null);

		Map<StartupDTO, List<Error>> errs = startupService.updateStartup(new StartupDTO());
		assertTrue(errs.containsKey(null));
		assertTrue(errs.get(null).get(0).getMessage().equals("Could not update the startup"));
	}

	private void test_validateStartupInfo_null_startupdto() {
		List<Error> errs = startupService.validateStartupInfo(null);
		assertTrue(errs.get(0).getMessage().equals("Null startup"));
	}

	private void test_validateStartupInfo_null_startupdto_fields() {
		StartupDTO dto = new StartupDTO();
		List<Error> errs = startupService.validateStartupInfo(dto);
		assertTrue(errs.size() == 1);
	}

	private void test_validateStartupInfo_ok() {
		StartupDTO dto = new StartupDTO("namesdfa", "sfasdfa@abv.bg", "512304156", "www.sdgdfsg.com", "4561237890");
		List<Error> errs = startupService.validateStartupInfo(dto);
		assertTrue(errs.isEmpty());
	}

	private void test_normalizeStartup_null_startupdto() {
		Map<StartupDTO, List<Error>> errs = startupService.normalizeStartup(null);
		assertTrue(errs.containsKey(null));
		String message = errs.get(null).get(0).getMessage();
		assertTrue(message.equals("Null startup"));
	}

	private void test_normalizeStartup_null_countrydto() {
		StartupDTO dto = new StartupDTO();
		Map<StartupDTO, List<Error>> errs = startupService.normalizeStartup(dto);
		assertTrue(errs.containsKey(dto));
		String message = errs.get(dto).get(0).getMessage();
		assertTrue(message.equals("Null country"));
	}

	private void test_normalizeStartup_null_countrydto_name() {
		StartupDTO dto = new StartupDTO();
		dto.setCountry(new CountryDTO());
		Map<StartupDTO, List<Error>> errs = startupService.normalizeStartup(dto);
		assertTrue(errs.containsKey(dto));
		String message = errs.get(dto).get(0).getMessage();
		assertTrue(message.equals("Country name is null"));
	}

	private void test_normalizeStartup_ok() {
		StartupDTO dto = new StartupDTO();
		CountryDTO cdto = new CountryDTO();
		cdto.setName("Germany");
		dto.setCountry(cdto);
		Mockito.when(countryService.getCountryDTOByName(any(CountryDTO.class))).thenReturn(new CountryDTO());

		Map<StartupDTO, List<Error>> errs = startupService.normalizeStartup(dto);
		assertTrue(errs.containsKey(dto));
		assertTrue(errs.get(dto).isEmpty());
	}
}
