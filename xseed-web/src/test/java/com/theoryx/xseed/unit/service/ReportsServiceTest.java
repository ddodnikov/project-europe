package com.theoryx.xseed.unit.service;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.Assert;

import com.theoryx.xseed.dto.ReportRow;
import com.theoryx.xseed.model.AnswerOption;
import com.theoryx.xseed.model.Calculation;
import com.theoryx.xseed.model.GroupCategory;
import com.theoryx.xseed.model.PhaseCategory;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.QuestionCalculation;
import com.theoryx.xseed.model.Snapshot;
import com.theoryx.xseed.model.SnapshotLine;
import com.theoryx.xseed.service.CalculationService;
import com.theoryx.xseed.service.ReportsServiceImpl;
import com.theoryx.xseed.service.SnapshotService;

@RunWith(MockitoJUnitRunner.class)
public class ReportsServiceTest {
	
	@Mock
	private SnapshotService snaphsotService;
	@Mock
	private CalculationService calculationService;
	
	@InjectMocks
	private ReportsServiceImpl reportsService;
	
	private final static int STARTUP_ID = 1;	
	
	//PhaseCategory
	private PhaseCategory pc1;
	private PhaseCategory pc2;
	
	//GroupCategory
	private GroupCategory gc1;
	private GroupCategory gc2;
	private GroupCategory gc3;
	
	//AnswerOption
	private AnswerOption ao1;
	private AnswerOption ao2;
	private AnswerOption ao3;
	private AnswerOption ao4;
	private AnswerOption ao5;
	
	//Questions
	private Question q1;
	private Question q2;
	private Question q3;
	
	//SnapshotLines
	private SnapshotLine line1;
	private SnapshotLine line2;
	private SnapshotLine line3;
	
	//Snapshot
	private Snapshot snapshot1;
	private Snapshot snapshot2;
	
	//questionCalculation
	private QuestionCalculation qc1;
	private QuestionCalculation qc2;
	private QuestionCalculation qc3;
	
	//Snapshots
	private List<Snapshot> snapshots;
	
	//Latest calculation
	private Calculation latestCalculation;
	
	@Before
	public void setUp(){
		pc1 = new PhaseCategory();
		pc1.setName("PC1");
		
		pc2 = new PhaseCategory();
		pc2.setName("PC2");
		 
		gc1 = new GroupCategory();
		gc1.setName("GC1");
		gc1.setPhaseCategory(pc1);
		
		gc2 = new GroupCategory();
		gc2.setName("GC2");
		gc2.setPhaseCategory(pc1);
		
		gc3 = new GroupCategory();
		gc3.setName("GC3");
		gc3.setPhaseCategory(pc2);
		
		ao1 = new AnswerOption();
		ao1.setId(20);
		ao1.setText("AO1");
		
		ao2 = new AnswerOption();
		ao2.setId(22);
		ao2.setText("AO2");
		
		ao3 = new AnswerOption();
		ao3.setId(23);
		ao3.setText("AO3");
		 
		ao4 = new AnswerOption();
		ao4.setId(25);
		ao4.setText("AO4");
 
		ao5 = new AnswerOption();
		ao5.setId(53);
		ao5.setText("AO5");
		
		 
		q1 = new Question();
		q1.setId(1);
		q1.setText("Q1");
		q1.setGroupCategories(new LinkedList<GroupCategory>());
		q1.getGroupCategories().add(gc1);

		q2 = new Question();
		q2.setId(2);
		q2.setText("Q2");
		q2.setGroupCategories(new LinkedList<GroupCategory>());
		q2.getGroupCategories().add(gc3);
		
		q3 = new Question();
		q3.setId(3);
		q3.setText("Q3");
		q3.setGroupCategories(new LinkedList<GroupCategory>());
		q3.getGroupCategories().add(gc3);
		
		line1 = new SnapshotLine();
		line1.setId(1);
		line1.setMultipleSelectedAnswers(new AnswerOption[]{ao1, ao2, ao3, ao4});
		line1.setQuestion(q1);
		
		line2 = new SnapshotLine();
		line2.setId(2);
		line2.setSelected_answer(ao5);
		line2.setQuestion(q2);
		
		line3 = new SnapshotLine();
		line3.setId(3);
		line3.setSelected_answer(ao1);
		line3.setQuestion(q1);
		
		//Snapshot 1
		snapshot1 = new Snapshot();
		snapshot1.setId(1);
		snapshot1.setSnapshotlines(new LinkedList<SnapshotLine>());
		snapshot1.getSnapshotlines().add(line1);
		snapshot1.getSnapshotlines().add(line2);
		
		//Snapshot 2
		snapshot2 = new Snapshot();
		snapshot2.setId(2);
		snapshot2.setSnapshotlines(new LinkedList<SnapshotLine>());
		snapshot2.getSnapshotlines().add(line2);
		snapshot2.getSnapshotlines().add(line3);
		
		snapshots = new LinkedList<Snapshot>();
		snapshots.add(snapshot1);
		snapshots.add(snapshot2);
		
		latestCalculation = new Calculation();
		qc1 = new QuestionCalculation();
		qc1.setId(1);
		qc1.setFormula_f(12.5);
		qc1.setQuestion(q1);
		qc1.setAnswerOption(ao1);
		
		qc2 = new QuestionCalculation();
		qc2.setId(2);
		qc2.setFormula_f(2.5);
		qc2.setQuestion(q2);
		qc2.setAnswerOption(ao5);
		
		qc3 = new QuestionCalculation();
		qc3.setId(3);
		qc3.setFormula_f(1.5);
		qc3.setQuestion(q1);
		qc3.setAnswerOption(ao2);
		
		latestCalculation.setQuestionCalculations(new LinkedList<QuestionCalculation>());
		latestCalculation.getQuestionCalculations().add(qc1);
		latestCalculation.getQuestionCalculations().add(qc2);
	}
	
	@Test
	public void getReportRowsWithValidData(){
		Mockito.when(snaphsotService.findAllSnapshotsByStartupId(STARTUP_ID))
				.thenReturn(snapshots);
		Mockito.when(calculationService.getLatestCalculationByStartupID(STARTUP_ID))
				.thenReturn(latestCalculation);
		LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>> reportRows = reportsService.getReportRows(STARTUP_ID);
		
		Assert.assertTrue(reportRows.containsKey("PC1"));
		Assert.assertTrue(reportRows.containsKey("PC2"));
		
		Assert.assertTrue(reportRows.get("PC1").containsKey("GC1"));
		Assert.assertTrue(reportRows.get("PC2").containsKey("GC3"));
		
		Assert.assertEquals("Q1", reportRows.get("PC1").get("GC1").get(0).getQuestionTitle());
		Assert.assertEquals("Q2", reportRows.get("PC2").get("GC3").get(0).getQuestionTitle());
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getTargetValue(), "AO1");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getTargetValue(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getAnswer(), "AO1");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getAnswer(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getValue(), new Double(12.5));
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getValue(), new Double(2.5));
		
		Assert.assertTrue(reportRows.get("PC1").get("GC1").get(0).isDelta());
		Assert.assertTrue(reportRows.get("PC2").get("GC3").get(0).isDelta());
	}
	
	@Test
	public void getReportRowsTestWithNull(){
		Mockito.when(snaphsotService.findAllSnapshotsByStartupId(null))
			.thenReturn(null);
		Mockito.when(calculationService.getLatestCalculationByStartupID(null))
			.thenReturn(null);
		Assert.assertNull(reportsService.getReportRows(null));
	}

	@Test
	public void getReportRowsTestWith2QuestionsAnd2Answers(){
		line1.setSelected_answer(ao1);
		line1.setMultipleSelectedAnswers(null);
		latestCalculation.getQuestionCalculations().add(qc3);
		
		Mockito.when(snaphsotService.findAllSnapshotsByStartupId(STARTUP_ID))
			.thenReturn(snapshots);
		Mockito.when(calculationService.getLatestCalculationByStartupID(STARTUP_ID))
			.thenReturn(latestCalculation);
		LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>> reportRows = reportsService.getReportRows(STARTUP_ID);
		
		Assert.assertTrue(reportRows.containsKey("PC1"));
		Assert.assertTrue(reportRows.containsKey("PC2"));
		
		Assert.assertTrue(reportRows.get("PC1").containsKey("GC1"));
		Assert.assertTrue(reportRows.get("PC2").containsKey("GC3"));
		
		Assert.assertEquals("Q1", reportRows.get("PC1").get("GC1").get(0).getQuestionTitle());
		Assert.assertEquals("Q2", reportRows.get("PC2").get("GC3").get(0).getQuestionTitle());
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getTargetValue(), "AO1");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getTargetValue(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getAnswer(), "AO1");
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(1).getAnswer(), "AO1");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getAnswer(), "AO5");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(1).getAnswer(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getValue(), new Double(12.5));
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(1).getValue(), new Double(12.5));
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getValue(), new Double(2.5));
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(1).getValue(), new Double(2.5));
		
		Assert.assertTrue(reportRows.get("PC1").get("GC1").get(0).isDelta());
		Assert.assertTrue(reportRows.get("PC2").get("GC3").get(0).isDelta());
		
		latestCalculation.getQuestionCalculations().remove(qc3);
	}
	
	@Test
	public void getReportRowsTestWith2QuestionsAnd2AnswersInvalidData(){
		line1.setSelected_answer(ao1);
		line1.setMultipleSelectedAnswers(null);
		
		Mockito.when(snaphsotService.findAllSnapshotsByStartupId(STARTUP_ID))
			.thenReturn(snapshots);
		Mockito.when(calculationService.getLatestCalculationByStartupID(STARTUP_ID))
			.thenReturn(latestCalculation);
		LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>> reportRows = reportsService.getReportRows(STARTUP_ID);
		
		Assert.assertTrue(reportRows.containsKey("PC1"));
		Assert.assertTrue(reportRows.containsKey("PC2"));
		
		Assert.assertTrue(reportRows.get("PC1").containsKey("GC1"));
		Assert.assertTrue(reportRows.get("PC2").containsKey("GC3"));
		
		Assert.assertEquals("Q1", reportRows.get("PC1").get("GC1").get(0).getQuestionTitle());
		Assert.assertEquals("Q2", reportRows.get("PC2").get("GC3").get(0).getQuestionTitle());
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getTargetValue(), "AO1");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getTargetValue(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getAnswer(), "AO1");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getAnswer(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getValue(), new Double(12.5));
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getValue(), new Double(2.5));
		
		Assert.assertTrue(reportRows.get("PC1").get("GC1").get(0).isDelta());
		Assert.assertTrue(reportRows.get("PC2").get("GC3").get(0).isDelta());
	}
	
	@Test
	public void getReportRowsTestWith2QuestionsAnd3Answers(){
		line1.setSelected_answer(ao1);
		line1.setMultipleSelectedAnswers(null);
		line3.setSelected_answer(ao2);
		latestCalculation.getQuestionCalculations().add(qc3);
		
		Mockito.when(snaphsotService.findAllSnapshotsByStartupId(STARTUP_ID))
			.thenReturn(snapshots);
		Mockito.when(calculationService.getLatestCalculationByStartupID(STARTUP_ID))
			.thenReturn(latestCalculation);
		LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>> reportRows = reportsService.getReportRows(STARTUP_ID);
		
		Assert.assertTrue(reportRows.containsKey("PC1"));
		Assert.assertTrue(reportRows.containsKey("PC2"));
		
		Assert.assertTrue(reportRows.get("PC1").containsKey("GC1"));
		Assert.assertTrue(reportRows.get("PC2").containsKey("GC3"));
		
		Assert.assertEquals("Q1", reportRows.get("PC1").get("GC1").get(0).getQuestionTitle());
		Assert.assertEquals("Q2", reportRows.get("PC2").get("GC3").get(0).getQuestionTitle());
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getTargetValue(), "AO2");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getTargetValue(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getAnswer(), "AO1");
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(1).getAnswer(), "AO2");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getAnswer(), "AO5");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(1).getAnswer(), "AO5");
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(0).getValue(), new Double(12.5));
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getAnswers().get(1).getValue(), new Double(1.5));
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(0).getValue(), new Double(2.5));
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getAnswers().get(1).getValue(), new Double(2.5));
		
		Assert.assertFalse(reportRows.get("PC1").get("GC1").get(0).isDelta());
		Assert.assertTrue(reportRows.get("PC2").get("GC3").get(0).isDelta());
		
		latestCalculation.getQuestionCalculations().remove(qc3);
	}
	
	@Test
	public void getReportRowsTestWith2QuestionsWithDifferentOrder(){
		qc3.setQuestion(q3);
		line3.setSelected_answer(ao2);
		line3.setQuestion(q3);
		latestCalculation.getQuestionCalculations().add(qc3);
		
		Mockito.when(snaphsotService.findAllSnapshotsByStartupId(STARTUP_ID))
				.thenReturn(snapshots);
		Mockito.when(calculationService.getLatestCalculationByStartupID(STARTUP_ID))
				.thenReturn(latestCalculation);
		LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>> reportRows = reportsService.getReportRows(STARTUP_ID);
	
		Assert.assertFalse(reportRows.containsKey("PC1"));
		Assert.assertFalse(reportRows.containsKey("PC2"));
		
		/*Assert.assertTrue(reportRows.get("PC1").containsKey("GC1"));
		Assert.assertTrue(reportRows.get("PC2").containsKey("GC3"));
		
		Assert.assertEquals("Q1", reportRows.get("PC1").get("GC1").get(0).getQuestionTitle());
		Assert.assertEquals("Q2", reportRows.get("PC2").get("GC3").get(0).getQuestionTitle());
		Assert.assertEquals("Q3", reportRows.get("PC2").get("GC3").get(1).getQuestionTitle());
		
		Assert.assertEquals(reportRows.get("PC1").get("GC1").get(0).getTargetValue(), "");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(0).getTargetValue(), "");
		Assert.assertEquals(reportRows.get("PC2").get("GC3").get(1).getTargetValue(), "");
		*/
		
		
		latestCalculation.getQuestionCalculations().remove(qc3);
	}

	
}

