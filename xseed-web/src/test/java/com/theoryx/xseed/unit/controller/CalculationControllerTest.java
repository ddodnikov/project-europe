package com.theoryx.xseed.unit.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.mockito.Matchers.any;


import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.theoryx.xseed.controller.CalculationController;
import com.theoryx.xseed.dto.CalculationDTO;
import com.theoryx.xseed.dto.GroupCalculationDTO;
import com.theoryx.xseed.dto.QuestionCalculationDTO;
import com.theoryx.xseed.dto.QuestionDTO;
import com.theoryx.xseed.dto.StartupCalculationDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.model.AnswerGroup;
import com.theoryx.xseed.model.AnswerOption;
import com.theoryx.xseed.model.Calculation;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.QuestionCalculation;
import com.theoryx.xseed.model.Snapshot;
import com.theoryx.xseed.model.SnapshotLine;
import com.theoryx.xseed.service.CalculationService;
import com.theoryx.xseed.service.GroupCalculationService;
import com.theoryx.xseed.service.QuestionCalculationService;
import com.theoryx.xseed.service.QuestionService;
import com.theoryx.xseed.service.SnapshotService;
import com.theoryx.xseed.service.SnapshotlineService;
import com.theoryx.xseed.service.StartupCalculationService;

@RunWith(MockitoJUnitRunner.class)
public class CalculationControllerTest {

	private MockMvc mockMvc;
	
	@Mock
	private MessageSource messageSource;
	@Mock
	private QuestionService questionService;
	@Mock
	private SnapshotService snapshotService;
	@Mock
	private StartupCalculationService startupCalculationService;
	@Mock
	private CalculationService calculationService;
	@Mock
	private GroupCalculationService groupCalculationService;
	@Mock
	private SnapshotlineService snapshotlineService;
	@Mock
	private QuestionCalculationService questionCalculationService;
	@InjectMocks
	private CalculationController calculationController;
	
	private List<UrlLabelMapping> breadcrumbs;
	
	@Before
	public void setUp(){
		// Process mock annotations
		MockitoAnnotations.initMocks(this);

		//ViewResolver
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp");
		viewResolver.setSuffix(".jsp");

		// Setup Spring test in standalone mode
		// Sets the vewresolver
		mockMvc = MockMvcBuilders.standaloneSetup(calculationController)
				.setViewResolvers(viewResolver).build();
		
		breadcrumbs = new LinkedList<UrlLabelMapping>();
		UrlLabelMapping home = new UrlLabelMapping("/home", "Home", true);
		breadcrumbs.add(home);
	}
	
	@Test
	public void testGetCalculationPage(){
		List<Question> filterQuestions = new LinkedList<Question>();
		List<Question> kpiQuestions = new LinkedList<Question>();
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(questionService.findByKpi(true)).thenReturn(kpiQuestions);
		Mockito.when(questionService.convert(filterQuestions)).thenReturn(new LinkedList<QuestionDTO>());
		Mockito.when(questionService.convert(kpiQuestions)).thenReturn(new LinkedList<QuestionDTO>());
		
		try {
			mockMvc.perform(get("/calculate")
					.requestAttr("breadCrumbs", new Object())
					.sessionAttr("currentUser", new Object()))
			.andExpect(status().isOk())
			.andExpect(view().name("/calculate"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/calculate.jsp"))
			.andExpect(model().attributeExists("filterQuestions"))
			.andExpect(model().attributeExists("kpiQuestions"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attributeExists("defaultName"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testGetCalculationPageWithNullUser(){
		try {
			mockMvc.perform(get("/calculate")
					.requestAttr("breadCrumbs", new Object()))
			.andExpect(status().isOk())
			.andExpect(view().name("/index"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/index.jsp"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFilter(){
		AnswerOption ao1 = new AnswerOption();
		ao1.setId(1);
		AnswerOption ao2 = new AnswerOption();
		ao2.setId(2);
		AnswerOption ao3 = new AnswerOption();
		ao3.setId(3);
		AnswerOption ao4 = new AnswerOption();
		ao4.setId(4);
		
		AnswerGroup ag1 = new AnswerGroup();
		ag1.setId(1);
		ag1.setOptions(new LinkedList<AnswerOption>());
		ag1.getOptions().add(ao1);
		ag1.getOptions().add(ao2);
		AnswerGroup ag2 = new AnswerGroup();
		ag2.setId(2);
		ag2.setOptions(new LinkedList<AnswerOption>());
		ag2.getOptions().add(ao3);
		ag2.getOptions().add(ao4);
		
		Question question1 = new Question();
		question1.setId(1);
		question1.setAnswergroup(ag1);
		Question question2 = new Question();
		question2.setId(2);
		question2.setAnswergroup(ag2);
		
		Question kpiQuestion = new Question();
		
		List<Question> filterQuestions = new LinkedList<Question>();
		filterQuestions.add(question1);
		filterQuestions.add(question2);
		
		List<Snapshot> filteredSnapshots = new LinkedList<Snapshot>();
		filteredSnapshots.add(new Snapshot());
		
		List<SnapshotLine> snapshotLines = new LinkedList<SnapshotLine>();
		SnapshotLine snapshotLine1 = new SnapshotLine();
		snapshotLine1.setMultipleSelectedAnswers(new AnswerOption[]{ao1, ao2});
		SnapshotLine snapshotLine2 = new SnapshotLine();
		snapshotLine2.setMultipleSelectedAnswers(new AnswerOption[]{ao3, ao4});
		snapshotLines.add(snapshotLine1);
		snapshotLines.add(snapshotLine1);
		
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(questionService.findbyId(Integer.valueOf("111"))).thenReturn(kpiQuestion);
		Mockito.when(snapshotService.filterSnapshots(Matchers.anyList(), Matchers.anyList())).thenReturn(filteredSnapshots);
		Mockito.when(calculationService.calculate(filteredSnapshots, kpiQuestion, "CalcName1")).thenReturn(new Calculation());
		Mockito.when(calculationService.convertCalculationToCalculationDTO(any(Calculation.class))).thenReturn(new CalculationDTO());
		
		try {
			mockMvc.perform(post("/calculate")
					.param("multiple_1_1", "value1")
					.param("multiple_1_2", "value2")
					.param("multiple_2_3", "value3")
					.param("multiple_2_4", "value4")
					.param("kpi", "111")
					.param("calculationName", "CalcName1")
					.requestAttr("breadCrumbs", new Object())
					.sessionAttr("currentUser", new Object()))
			.andExpect(status().isOk())
			.andExpect(view().name("/calculation-summary"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-summary.jsp"))
			.andExpect(model().attributeExists("calculation"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFilterWithEmptySnapshotLines(){
		AnswerOption ao1 = new AnswerOption();
		ao1.setId(1);
		AnswerOption ao2 = new AnswerOption();
		ao2.setId(2);
		AnswerOption ao3 = new AnswerOption();
		ao3.setId(3);
		AnswerOption ao4 = new AnswerOption();
		ao4.setId(4);
		
		AnswerGroup ag1 = new AnswerGroup();
		ag1.setId(1);
		ag1.setOptions(new LinkedList<AnswerOption>());
		ag1.getOptions().add(ao1);
		ag1.getOptions().add(ao2);
		AnswerGroup ag2 = new AnswerGroup();
		ag2.setId(2);
		ag2.setOptions(new LinkedList<AnswerOption>());
		ag2.getOptions().add(ao3);
		ag2.getOptions().add(ao4);
		
		Question question1 = new Question();
		question1.setId(1);
		question1.setAnswergroup(ag1);
		Question question2 = new Question();
		question2.setId(2);
		question2.setAnswergroup(ag2);
		
		Question kpiQuestion = new Question();
		
		List<Question> filterQuestions = new LinkedList<Question>();
		filterQuestions.add(question1);
		filterQuestions.add(question2);
		
		List<Snapshot> filteredSnapshots = new LinkedList<Snapshot>();
		filteredSnapshots.add(new Snapshot());
		
		List<SnapshotLine> snapshotLines = new LinkedList<SnapshotLine>();
		SnapshotLine snapshotLine1 = new SnapshotLine();
		snapshotLine1.setMultipleSelectedAnswers(new AnswerOption[]{ao1, ao2});
		SnapshotLine snapshotLine2 = new SnapshotLine();
		snapshotLine2.setMultipleSelectedAnswers(new AnswerOption[]{ao3, ao4});
		snapshotLines.add(snapshotLine1);
		snapshotLines.add(snapshotLine1);
		
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(questionService.findbyId(Integer.valueOf("111"))).thenReturn(null);
		Mockito.when(snapshotService.filterSnapshots(Matchers.anyList(), Matchers.anyList())).thenReturn(filteredSnapshots);
		Mockito.when(calculationService.calculate(filteredSnapshots, kpiQuestion, "CalcName1")).thenReturn(new Calculation());
		Mockito.when(calculationService.convertCalculationToCalculationDTO(any(Calculation.class))).thenReturn(new CalculationDTO());
		Mockito.when(messageSource.getMessage("message.filter", new Object[0], Locale.getDefault())).thenReturn("error");
		try {
			mockMvc.perform(post("/calculate")
					.param("multiple_1_1", "value1")
					.param("multiple_1_2", "value2")
					.param("multiple_2_3", "value3")
					.param("multiple_2_4", "value4")
					.param("calculationName", "CalcName1")
					.requestAttr("breadCrumbs", new Object())
					.sessionAttr("currentUser", new Object()))
			.andExpect(status().isOk())
			.andExpect(view().name("/calculation-summary"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-summary.jsp"))
			.andExpect(model().attributeExists("noResultsFound"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFilterWithEmptyFilteredSnapshots(){
		AnswerOption ao1 = new AnswerOption();
		ao1.setId(1);
		AnswerOption ao2 = new AnswerOption();
		ao2.setId(2);
		AnswerOption ao3 = new AnswerOption();
		ao3.setId(3);
		AnswerOption ao4 = new AnswerOption();
		ao4.setId(4);
		
		AnswerGroup ag1 = new AnswerGroup();
		ag1.setId(1);
		ag1.setOptions(new LinkedList<AnswerOption>());
		ag1.getOptions().add(ao1);
		ag1.getOptions().add(ao2);
		AnswerGroup ag2 = new AnswerGroup();
		ag2.setId(2);
		ag2.setOptions(new LinkedList<AnswerOption>());
		ag2.getOptions().add(ao3);
		ag2.getOptions().add(ao4);
		
		Question question1 = new Question();
		question1.setId(1);
		question1.setAnswergroup(ag1);
		Question question2 = new Question();
		question2.setId(2);
		question2.setAnswergroup(ag2);
		
		Question kpiQuestion = new Question();
		
		List<Question> filterQuestions = new LinkedList<Question>();
		filterQuestions.add(question1);
		filterQuestions.add(question2);
		
		List<Snapshot> filteredSnapshots = new LinkedList<Snapshot>();
		filteredSnapshots.add(new Snapshot());
		
		List<SnapshotLine> snapshotLines = new LinkedList<SnapshotLine>();
		SnapshotLine snapshotLine1 = new SnapshotLine();
		snapshotLine1.setMultipleSelectedAnswers(new AnswerOption[]{ao1, ao2});
		SnapshotLine snapshotLine2 = new SnapshotLine();
		snapshotLine2.setMultipleSelectedAnswers(new AnswerOption[]{ao3, ao4});
		snapshotLines.add(snapshotLine1);
		snapshotLines.add(snapshotLine1);
		
		
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(questionService.findbyId(Integer.valueOf("111"))).thenReturn(null);
		Mockito.when(snapshotService.filterSnapshots(Matchers.anyList(), Matchers.anyList())).thenReturn(new LinkedList<Snapshot>());
		Mockito.when(calculationService.calculate(filteredSnapshots, kpiQuestion, "CalcName1")).thenReturn(new Calculation());
		Mockito.when(calculationService.convertCalculationToCalculationDTO(any(Calculation.class))).thenReturn(new CalculationDTO());
		Mockito.when(messageSource.getMessage("message.no-startups-found", new Object[0], Locale.getDefault())).thenReturn("error");
		
		try {
			mockMvc.perform(post("/calculate")
					.param("multiple_1_1", "value1")
					.param("multiple_1_2", "value2")
					.param("multiple_2_3", "value3")
					.param("multiple_2_4", "value4")
					.param("kpi", "111")
					.param("calculationName", "CalcName1")
					.requestAttr("breadCrumbs", new Object())
					.sessionAttr("currentUser", new Object()))
			.andExpect(status().isOk())
			.andExpect(view().name("/calculation-summary"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-summary.jsp"))
			.andExpect(model().attributeExists("noResultsFound"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFilterWithNullCalculation(){
		AnswerOption ao1 = new AnswerOption();
		ao1.setId(1);
		AnswerOption ao2 = new AnswerOption();
		ao2.setId(2);
		AnswerOption ao3 = new AnswerOption();
		ao3.setId(3);
		AnswerOption ao4 = new AnswerOption();
		ao4.setId(4);
		
		AnswerGroup ag1 = new AnswerGroup();
		ag1.setId(1);
		ag1.setOptions(new LinkedList<AnswerOption>());
		ag1.getOptions().add(ao1);
		ag1.getOptions().add(ao2);
		AnswerGroup ag2 = new AnswerGroup();
		ag2.setId(2);
		ag2.setOptions(new LinkedList<AnswerOption>());
		ag2.getOptions().add(ao3);
		ag2.getOptions().add(ao4);
		
		Question question1 = new Question();
		question1.setId(1);
		question1.setAnswergroup(ag1);
		Question question2 = new Question();
		question2.setId(2);
		question2.setAnswergroup(ag2);
		
		Question kpiQuestion = new Question();
		
		List<Question> filterQuestions = new LinkedList<Question>();
		filterQuestions.add(question1);
		filterQuestions.add(question2);
		
		List<Snapshot> filteredSnapshots = new LinkedList<Snapshot>();
		filteredSnapshots.add(new Snapshot());
		
		List<SnapshotLine> snapshotLines = new LinkedList<SnapshotLine>();
		SnapshotLine snapshotLine1 = new SnapshotLine();
		snapshotLine1.setMultipleSelectedAnswers(new AnswerOption[]{ao1, ao2});
		SnapshotLine snapshotLine2 = new SnapshotLine();
		snapshotLine2.setMultipleSelectedAnswers(new AnswerOption[]{ao3, ao4});
		snapshotLines.add(snapshotLine1);
		snapshotLines.add(snapshotLine1);
		
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(questionService.findbyId(Integer.valueOf("111"))).thenReturn(null);
		Mockito.when(snapshotService.filterSnapshots(Matchers.anyList(), Matchers.anyList())).thenReturn(filteredSnapshots);
		Mockito.when(calculationService.calculate(filteredSnapshots, kpiQuestion, "CalcName1")).thenReturn(new Calculation());
		Mockito.when(calculationService.convertCalculationToCalculationDTO(any(Calculation.class))).thenReturn(null);
		Mockito.when(messageSource.getMessage("calculate.calculation-error", new Object[0], Locale.getDefault())).thenReturn("error");
		try {
			mockMvc.perform(post("/calculate")
					.param("multiple_1_1", "value1")
					.param("multiple_1_2", "value2")
					.param("multiple_2_3", "value3")
					.param("multiple_2_4", "value4")
					.param("kpi", "111")
					.param("calculationName", "CalcName1")
					.requestAttr("breadCrumbs", new Object())
					.sessionAttr("currentUser", new Object()))
			.andExpect(status().isOk())
			.andExpect(view().name("/calculations"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/calculations.jsp"))
			.andExpect(model().attributeExists("calculationError"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetDetails(){
		Calculation calculation = new Calculation();
		CalculationDTO calculationDTO = new CalculationDTO();
		calculationDTO.setId(1);
		calculationDTO.setName("CalculationDTO1");
		List<QuestionCalculation> questionCalculations = new LinkedList<QuestionCalculation>();
		calculation.setQuestionCalculations(questionCalculations);
		
		List<List<List<QuestionCalculation>>> questionCalculationDtos = new LinkedList<List<List<QuestionCalculation>>>();
		Mockito.when(calculationService.findById(1)).thenReturn(calculation);
		Mockito.when(calculationService
			.convertCalculationToCalculationDTO(calculation))
			.thenReturn(calculationDTO);
		Mockito
			.when(questionCalculationService.groupByAnswerGroupAndQuestion(questionCalculations))
			.thenReturn(questionCalculationDtos);
		Mockito.when(questionCalculationService.convert(questionCalculationDtos)).thenReturn(new LinkedList<List<QuestionCalculationDTO>>());
		Mockito.when(groupCalculationService.convert(calculation.getGroupCalculations())).thenReturn(new LinkedList<GroupCalculationDTO>());
		Mockito.when(startupCalculationService.convert(calculation.getStartupCalculations())).thenReturn(new LinkedList<StartupCalculationDTO>());
		
		try {
			mockMvc.perform(get("/calculation-details")
					.param("calculationid", "1")
					.requestAttr("breadCrumbs", breadcrumbs))
			.andExpect(status().isOk())
			.andExpect(view().name("/calculation-details"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-details.jsp"))
			.andExpect(model().attributeExists("questionCalculations"))
			.andExpect(model().attributeExists("groupCalculations"))
			.andExpect(model().attributeExists("startupCalculations"))
			.andExpect(model().attributeExists("calculation"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetCalculations(){
		List<Calculation> calculations = new LinkedList<Calculation>();
		Mockito.when(calculationService.findAll()).thenReturn(calculations);
		Mockito.when(calculationService.convert(calculations)).thenReturn(new LinkedList<CalculationDTO>());
		
		try {
			mockMvc.perform(get("/calculations")
					.param("calculationid", "1")
					.requestAttr("breadCrumbs", breadcrumbs))
			.andExpect(status().isOk())
			.andExpect(view().name("/calculations"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/calculations.jsp"))
			.andExpect(model().attributeExists("calculations"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCheckFilterResultFound(){
		String questionsJson = new String("[{\"questionId\":1, \"answerIds\":[1,2,3,4]}]");
		List<Question> filterQuestions = new LinkedList<Question>();
		List<SnapshotLine> snapshotlines = new LinkedList<SnapshotLine>();
		List<Snapshot> filteredSnapshots = new LinkedList<Snapshot>();
		filteredSnapshots.add(new Snapshot());
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(snapshotlineService.createFilterSnapshotlines(any(List.class), any(List.class)))
				.thenReturn(snapshotlines);
		Mockito.when(snapshotService.filterSnapshots(filterQuestions, snapshotlines))
				.thenReturn(filteredSnapshots);
		
		try {
			mockMvc.perform(post("/checkfilter")
					//.accept(MediaType.APPLICATION_JSON)
					.content(questionsJson)
					.contentType(MediaType.APPLICATION_JSON)
					.param("calculationid", "1")
					.requestAttr("breadCrumbs", breadcrumbs))
				.andReturn().getResponse().getContentAsString().equals("{\"result\":\"resultsFound\"}");
					
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCheckFilterNoResultFound(){
		String questionsJson = new String("[{\"questionId\":1, \"answerIds\":[1,2,3,4]}]");
		//List<AjaxQuestion> questions = new LinkedList<AjaxQuestion>();
		List<Question> filterQuestions = new LinkedList<Question>();
		List<SnapshotLine> snapshotlines = new LinkedList<SnapshotLine>();
		List<Snapshot> filteredSnapshots = new LinkedList<Snapshot>();
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(snapshotlineService.createFilterSnapshotlines(any(List.class), any(List.class)))
				.thenReturn(snapshotlines);
		Mockito.when(snapshotService.filterSnapshots(filterQuestions, snapshotlines))
				.thenReturn(filteredSnapshots);
		
		try {
			mockMvc.perform(post("/checkfilter")
					//.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
					.content(questionsJson)
					.param("calculationid", "1")
					.requestAttr("breadCrumbs", breadcrumbs))
				.andReturn().getResponse().getContentAsString().equals("{\"result\":\"noResultsFound\"}");
					
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testCheckFilterMissingInput(){
		String questionsJson = new String("[{\"questionId\":1, \"answerIds\":[]}]");
		List<Question> filterQuestions = new LinkedList<Question>();
		List<SnapshotLine> snapshotlines = new LinkedList<SnapshotLine>();
		List<Snapshot> filteredSnapshots = new LinkedList<Snapshot>();
		filteredSnapshots.add(new Snapshot());
		Mockito.when(questionService.findByFilter(true)).thenReturn(filterQuestions);
		Mockito.when(snapshotlineService.createFilterSnapshotlines(any(List.class), any(List.class)))
				.thenReturn(snapshotlines);
		Mockito.when(snapshotService.filterSnapshots(filterQuestions, snapshotlines))
				.thenReturn(filteredSnapshots);
		
		try {
			mockMvc.perform(post("/checkfilter")
					//.accept(MediaType.APPLICATION_JSON)
					.contentType(MediaType.APPLICATION_JSON)
					.content(questionsJson)
					.param("calculationid", "1")
					.requestAttr("breadCrumbs", breadcrumbs))
				.andReturn().getResponse().getContentAsString().equals("{\"result\":\"missingInput\"}");
					
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
}
