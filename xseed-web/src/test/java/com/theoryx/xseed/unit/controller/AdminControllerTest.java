package com.theoryx.xseed.unit.controller;

import static org.hamcrest.Matchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.theoryx.xseed.controller.AdminController;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.service.MailService;
import com.theoryx.xseed.service.QuestionService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class AdminControllerTest {

	private MockMvc mockMvc;
	
	@Mock
	private UserService userService;
	@Mock
	private StartupService startupService;
	@Mock
	private MailService mailService;
	@Mock
	private QuestionService questionService;
	@InjectMocks
	private AdminController adminController;

	@Before
	public void setUp() {	
		
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
		
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("locale/language");

		//ViewResolver
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp");
		viewResolver.setSuffix(".jsp");

		// Setup Spring test in standalone mode
		// Sets the vewresolver
		mockMvc = MockMvcBuilders.standaloneSetup(adminController).setViewResolvers(viewResolver).build();

		adminController.setMessageSource(messageSource);
	}

	@Test
	public void test_getAdminLoginPage() {
		try {
			mockMvc.perform(get("/admin"))
				.andExpect(status().isOk())
				.andExpect(view().name("/admin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/admin.jsp"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getAddingAddminsPage() {
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(get("/addadmin").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/addadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/addadmin.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_showAdmins() {
		Mockito.when(userService.getAllAdmins()).thenReturn(new ArrayList<User>());
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(get("/admins").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/admins"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/admins.jsp"))
				.andExpect(model().attributeExists("admins"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_showAdmins_null() {
		Mockito.when(userService.getAllAdmins()).thenReturn(null);
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(get("/admins").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/admins"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/admins.jsp"))
				.andExpect(model().attributeExists("admins"))
				.andExpect(model().attribute("admins", hasSize(0)))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getAlgoQuestions() {
		Mockito.when(questionService.findAll()).thenReturn(new ArrayList<Question>());
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(get("/algoquestions").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/algoquestions"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/algoquestions.jsp"))
				.andExpect(model().attributeExists("algoQuestions"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_saveAlgoQuestions_noparam_algo() {
		Question q = new Question();
		q.setId(5);
		q.setAlgo(true);
		Mockito.when(questionService.findAll()).thenReturn(Arrays.asList(q));
		Mockito.when(questionService.save(any(Question.class))).thenReturn(new Question());
		try {
			mockMvc.perform(post("/algoquestions").param("6", "true"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/algoquestions"))
				.andExpect(redirectedUrl("/algoquestions"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_saveAlgoQuestions_noparam_notalgo() {
		Question q = new Question();
		q.setId(5);
		q.setAlgo(false);
		Mockito.when(questionService.findAll()).thenReturn(Arrays.asList(q));
		Mockito.when(questionService.save(any(Question.class))).thenReturn(new Question());
		try {
			mockMvc.perform(post("/algoquestions").param("6", "true"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/algoquestions"))
				.andExpect(redirectedUrl("/algoquestions"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_saveAlgoQuestions_param_algo() {
		Question q = new Question();
		q.setId(5);
		q.setAlgo(true);
		Mockito.when(questionService.findAll()).thenReturn(Arrays.asList(q));
		Mockito.when(questionService.save(any(Question.class))).thenReturn(new Question());
		try {
			mockMvc.perform(post("/algoquestions").param("5", "true"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/algoquestions"))
				.andExpect(redirectedUrl("/algoquestions"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_saveAlgoQuestions_param_notalgo() {
		Question q = new Question();
		q.setId(5);
		q.setAlgo(false);
		Mockito.when(questionService.findAll()).thenReturn(Arrays.asList(q));
		Mockito.when(questionService.save(any(Question.class))).thenReturn(new Question());
		try {
			mockMvc.perform(post("/algoquestions").param("5", "true"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/algoquestions"))
				.andExpect(redirectedUrl("/algoquestions"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createAdmin_invalidemail() {
		Mockito.when(userService.getUserByEmail("445dfbfd")).thenReturn(null);
		try {
			mockMvc.perform(get("/createadmin").param("token", "ssdf").param("email", "445dfbfd"))
				.andExpect(status().isOk())
				.andExpect(view().name("/createadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/createadmin.jsp"))
				.andExpect(model().attribute("errors", nullValue()))
				.andExpect(model().attribute("userdto", nullValue()))
				.andExpect(model().attributeDoesNotExist("email"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createAdmin_loggedinuser() {
		Mockito.when(userService.getUserByEmail("445dfbfd")).thenReturn(null);
		try {
			mockMvc.perform(get("/createadmin").param("token", "ssdf").param("email", "445dfbfd").sessionAttr("currentUser", new UserDTO()))
				.andExpect(status().isOk())
				.andExpect(view().name("/createadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/createadmin.jsp"))
				.andExpect(model().attribute("errors", nullValue()))
				.andExpect(model().attribute("userdto", nullValue()))
				.andExpect(model().attributeDoesNotExist("email"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createAdmin_noerrors_active_sametoken() {
		UserDTO testdto = new UserDTO();
		testdto.setEmail("testnew@gmail.com");
		testdto.setActive(true);

		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("aaaa");
		String token = userService.generateActivationToken(new UserDTO());

		Map<UserDTO, List<Error>> errors = new HashMap<UserDTO, List<Error>>();
		errors.put(testdto, new LinkedList<Error>());
		Mockito.when(userService.getUserByEmail(any(String.class))).thenReturn(errors);
		
		try {
			mockMvc.perform(get("/createadmin").param("token", token).param("email", "testnew@gmail.com"))
				.andExpect(status().isOk())
				.andExpect(view().name("/login"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", instanceOf(List.class)))
				.andExpect(model().attributeDoesNotExist("email"))
				.andExpect(model().attributeDoesNotExist("userdto"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createAdmin_noerrors_active_difftoken() {
		UserDTO testdto = new UserDTO();
		testdto.setEmail("tesastnew@gmail.com");
		testdto.setActive(true);
		
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("aaaa");
		String token = userService.generateActivationToken(testdto);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("bbbb");
		
		Map<UserDTO, List<Error>> errors = new HashMap<UserDTO, List<Error>>();
		errors.put(testdto, new LinkedList<Error>());
		Mockito.when(userService.getUserByEmail(any(String.class))).thenReturn(errors);
		
		try {
			mockMvc.perform(get("/createadmin").param("token", token).param("email", "testnew@gmail.com"))
				.andExpect(status().isOk())
				.andExpect(view().name("/login"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", instanceOf(List.class)))
				.andExpect(model().attributeDoesNotExist("userdto"))
				.andExpect(model().attributeDoesNotExist("email"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createAdmin_noerrors_inactive_sametoken() {
		UserDTO testdto = new UserDTO();
		testdto.setEmail("testnew@gmail.com");
		testdto.setActive(false);
		
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("aaaa");
		String token = userService.generateActivationToken(testdto);
		
		Map<UserDTO, List<Error>> errors = new HashMap<UserDTO, List<Error>>();
		errors.put(testdto, new LinkedList<Error>());
		Mockito.when(userService.getUserByEmail(any(String.class))).thenReturn(errors);
		
		try {
			mockMvc.perform(get("/createadmin").param("token", token).param("email", "testnew@gmail.com"))
				.andExpect(status().isOk())
				.andExpect(view().name("/createadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/createadmin.jsp"))
				.andExpect(model().attributeExists("email"))
				.andExpect(model().attributeExists("userdto"))
				.andExpect(model().attribute("userdto", instanceOf(UserDTO.class)))
				.andExpect(model().attributeDoesNotExist("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createAdmin_noerrors_inactive_difftoken() {
		UserDTO testdto = new UserDTO();
		testdto.setEmail("tesdfstnew@gmail.com");
		testdto.setActive(false);
		
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("aaaa");
		String token = userService.generateActivationToken(testdto);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("bbbb");
		
		Map<UserDTO, List<Error>> errors = new HashMap<UserDTO, List<Error>>();
		errors.put(testdto, new LinkedList<Error>());
		Mockito.when(userService.getUserByEmail(any(String.class))).thenReturn(errors);
		
		try {
			mockMvc.perform(get("/createadmin").param("token", token).param("email", "testnew@gmail.com"))
				.andExpect(status().isOk())
				.andExpect(view().name("/login"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", instanceOf(List.class)))
				.andExpect(model().attributeDoesNotExist("userdto"))
				.andExpect(model().attributeDoesNotExist("email"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addAddmin_invalid() {
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(post("/addadmin").param("email", "..14@.com").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/addadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/addadmin.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", instanceOf(List.class)))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addAddmin_valid_exists() {
		Map<UserDTO, List<Error>> errors = new HashMap<UserDTO, List<Error>>();
		errors.put(new UserDTO(), Arrays.asList(new Error()));
		Mockito.when(userService.ifExist(any(String.class))).thenReturn(true);
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(post("/addadmin").param("email", "testnew@gmail.com").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/addadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/addadmin.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", instanceOf(List.class)))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addAddmin_valid_notexixt_false() {
		Mockito.when(userService.ifExist(any(String.class))).thenReturn(false);
		Mockito.when(userService.createUser(any(UserDTO.class))).thenReturn(null);
		Mockito.when(userService.convertUserToUserDTO(any(User.class))).thenReturn(null);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn(null);
		Mockito.when(mailService.sendAdminCreationEmail(any(String.class), any(String.class), any(String.class), any(String.class))).thenReturn(false);
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(post("/addadmin").param("email", "testnew@gmail.com").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/addadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/addadmin.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attributeDoesNotExist("success"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", instanceOf(List.class)))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addAddmin_valid_notexists_true() {
		Mockito.when(userService.ifExist(any(String.class))).thenReturn(false);
		Mockito.when(userService.createUser(any(UserDTO.class))).thenReturn(null);
		Mockito.when(userService.convertUserToUserDTO(any(User.class))).thenReturn(null);
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn(null);
		Mockito.when(mailService.sendAdminCreationEmail(any(String.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(post("/addadmin").param("email", "testnew@gmail.com").requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/addadmin"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/addadmin.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attributeExists("success"))
				.andExpect(model().attribute("errors", hasSize(0)))
				.andExpect(model().attribute("success", hasToString("Email was sent successfully")))
				.andExpect(model().attribute("errors", instanceOf(List.class)))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsers(){
		List<User> users = new LinkedList<User>();
		users.add(new User());
		users.add(new User());
		users.add(new User());
		
		Mockito.when(userService.getUsersCount()).thenReturn(3);
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Mockito.when(userService.convertUserToUserDTO(any(User.class))).thenReturn(new UserDTO());
		
		List<User> users2 = new LinkedList<User>();
		users2.add(new User());
		
		Mockito.when( userService.getPaginationUsers(1,0)).thenReturn(users2);
		
		try {
			mockMvc.perform(get("/showallusers")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "1")
					.param("show", "1"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallusers"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallusers.jsp"))
			.andExpect(model().attributeExists("usersPerPage"))
			.andExpect(model().attributeExists("usersCount"))
			.andExpect(model().attributeExists("users"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attributeExists("pages"))
			.andExpect(model().attributeExists("nextPrevPages"));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Test
	public void testGetAllUsersWithLimitAll(){	
		List<User> users = new LinkedList<User>();
		users.add(new User());
		users.add(new User());
		users.add(new User());
		
		Mockito.when(userService.getUsersCount()).thenReturn(3);
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Mockito.when(userService.convertUserToUserDTO(any(User.class))).thenReturn(new UserDTO());
		
		try {
			mockMvc.perform(get("/showallusers")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "0")
					.param("show", "All"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallusers"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallusers.jsp"))
			.andExpect(model().attributeExists("usersPerPage"))
			.andExpect(model().attributeExists("usersCount"))
			.andExpect(model().attributeExists("users"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithLimitGreaterThanMaxUsers(){
		List<User> users = new LinkedList<User>();
		users.add(new User());
		users.add(new User());
		users.add(new User());
		
		Mockito.when(userService.getUsersCount()).thenReturn(3);
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Mockito.when(userService.convertUserToUserDTO(any(User.class))).thenReturn(new UserDTO());
		
		try {
			mockMvc.perform(get("/showallusers")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "0")
					.param("show", "5"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallusers"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallusers.jsp"))
			.andExpect(model().attributeExists("usersPerPage"))
			.andExpect(model().attributeExists("usersCount"))
			.andExpect(model().attributeExists("users"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithPageZero(){
		List<User> users = new LinkedList<User>();
		users.add(new User());
		users.add(new User());
		users.add(new User());
		
		Mockito.when(userService.getUsersCount()).thenReturn(3);
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Mockito.when(userService.convertUserToUserDTO(any(User.class))).thenReturn(new UserDTO());
		
		List<User> users2 = new LinkedList<User>();
		users2.add(new User());
		
		Mockito.when( userService.getPaginationUsers(1,0)).thenReturn(users2);
		
		try {
			mockMvc.perform(get("/showallusers")
					.header("referer", "")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "0")
					.param("show", "1"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallusers?page=1&show=1"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithPageGreaterThanMax(){
		List<User> users = new LinkedList<User>();
		users.add(new User());
		users.add(new User());
		users.add(new User());
		
		Mockito.when(userService.getUsersCount()).thenReturn(3);
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Mockito.when(userService.convertUserToUserDTO(any(User.class))).thenReturn(new UserDTO());
		
		List<User> users2 = new LinkedList<User>();
		users2.add(new User());
		
		Mockito.when( userService.getPaginationUsers(1,0)).thenReturn(users2);
		
		try {
			mockMvc.perform(get("/showallusers")
					.header("referer", "page=1")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "4")
					.param("show", "1"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallusers?page=3&show=1"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartups(){
		List<Startup> startups = new LinkedList<Startup>();
		startups.add(new Startup());
		startups.add(new Startup());
		startups.add(new Startup());
		
		Mockito.when(startupService.getAllStartupCount()).thenReturn(3);
		Mockito.when(startupService.getAllStartups()).thenReturn(startups);
		Mockito.when(startupService.convertStartupToStartupDTO(any(Startup.class))).thenReturn(new StartupDTO());
		
		List<Startup> startups2 = new LinkedList<Startup>();
		startups2.add(new Startup());
		Mockito.when(startupService.getPaginationStartups(1,0)).thenReturn(startups2);
		
		try {
			mockMvc.perform(get("/showallstartups")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "1")
					.param("show", "1"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallstartups"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallstartups.jsp"))
			.andExpect(model().attributeExists("startupsPerPage"))
			.andExpect(model().attributeExists("startupsCount"))
			.andExpect(model().attributeExists("startups"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attributeExists("pages"))
			.andExpect(model().attributeExists("nextPrevPages"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithLimitAll(){
		List<Startup> startups = new LinkedList<Startup>();
		startups.add(new Startup());
		startups.add(new Startup());
		startups.add(new Startup());
		
		Mockito.when(startupService.getAllStartupCount()).thenReturn(3);
		Mockito.when(startupService.getAllStartups()).thenReturn(startups);
		Mockito.when(startupService.convertStartupToStartupDTO(any(Startup.class))).thenReturn(new StartupDTO());
		
		try {
			mockMvc.perform(get("/showallstartups")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "0")
					.param("show", "All"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallstartups"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallstartups.jsp"))
			.andExpect(model().attributeExists("startupsPerPage"))
			.andExpect(model().attributeExists("startupsCount"))
			.andExpect(model().attributeExists("startups"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithLimitGreaterThanMaxStartups(){
		List<Startup> startups = new LinkedList<Startup>();
		startups.add(new Startup());
		startups.add(new Startup());
		startups.add(new Startup());
		
		Mockito.when(startupService.getAllStartupCount()).thenReturn(3);
		Mockito.when(startupService.getAllStartups()).thenReturn(startups);
		Mockito.when(startupService.convertStartupToStartupDTO(any(Startup.class))).thenReturn(new StartupDTO());
		
		try {
			mockMvc.perform(get("/showallstartups")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "0")
					.param("show", "5"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallstartups"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallstartups.jsp"))
			.andExpect(model().attributeExists("startupsPerPage"))
			.andExpect(model().attributeExists("startupsCount"))
			.andExpect(model().attributeExists("startups"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithPageZero(){
		List<Startup> startups = new LinkedList<Startup>();
		startups.add(new Startup());
		startups.add(new Startup());
		startups.add(new Startup());
		
		Mockito.when(startupService.getAllStartupCount()).thenReturn(3);
		Mockito.when(startupService.getAllStartups()).thenReturn(startups);
		Mockito.when(startupService.convertStartupToStartupDTO(any(Startup.class))).thenReturn(new StartupDTO());
		
		List<Startup> startups2 = new LinkedList<Startup>();
		startups2.add(new Startup());
		Mockito.when(startupService.getPaginationStartups(1,0)).thenReturn(startups2);
		
		try {
			mockMvc.perform(get("/showallstartups")
					.header("referer", "")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "0")
					.param("show", "1"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallstartups?page=1&show=1"));;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithPageGreaterThanMax(){
		List<Startup> startups = new LinkedList<Startup>();
		startups.add(new Startup());
		startups.add(new Startup());
		startups.add(new Startup());
		
		Mockito.when(startupService.getAllStartupCount()).thenReturn(3);
		Mockito.when(startupService.getAllStartups()).thenReturn(startups);
		Mockito.when(startupService.convertStartupToStartupDTO(any(Startup.class))).thenReturn(new StartupDTO());
		
		List<Startup> startups2 = new LinkedList<Startup>();
		startups2.add(new Startup());
		Mockito.when(startupService.getPaginationStartups(1,0)).thenReturn(startups2);
		
		try {
			mockMvc.perform(get("/showallstartups")
					.header("referer", "page=1")
					.requestAttr("breadCrumbs", new Object())
					.param("page", "4")
					.param("show", "1"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallstartups?page=3&show=1"));;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
