package com.theoryx.xseed.unit.service;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;

import static org.mockito.Matchers.any;

import com.theoryx.xseed.dto.SurveyQuestionDTO;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.Survey;
import com.theoryx.xseed.model.SurveyQuestion;
import com.theoryx.xseed.repository.SurveyQuestionRepository;
import com.theoryx.xseed.service.QuestionService;
import com.theoryx.xseed.service.SurveyQuestionServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class SurveyQuestionServiceTest {

	@Mock
	private SurveyQuestionRepository surveyQuestionRepository;
	@Mock
	private QuestionService questionService;
	@InjectMocks
	private SurveyQuestionServiceImpl surveyQuestionService;
	
	private static final int SURVEY_ID = 1;
	private static final int NUMBER_OF_STARTUPS = 10;
	@BeforeMethod
	public void initMocks(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getNumberOfSurveyQuestionsTest(){
		Mockito.when(surveyQuestionRepository.getNumberOfSurveyQuestions(SURVEY_ID)).thenReturn(NUMBER_OF_STARTUPS);
		Survey survey = new Survey();
		survey.setId(SURVEY_ID);
		
		Assert.assertEquals(NUMBER_OF_STARTUPS, surveyQuestionService.getNumberOfSurveyQuestions(survey));
	}
	
	@Test
	public void getNumberOfSurveyQuestionsTestWithNull(){
		Mockito.when(surveyQuestionRepository.getNumberOfSurveyQuestions(SURVEY_ID)).thenReturn(NUMBER_OF_STARTUPS);
		Assert.assertEquals(0, surveyQuestionService.getNumberOfSurveyQuestions(null));
	}
	
	@Test
	public void getNumberOfSurveyQuestionsTestWithInvalidData(){
		Mockito.when(surveyQuestionRepository.getNumberOfSurveyQuestions(null)).thenReturn(0);
		Survey survey = new Survey();
		survey.setId(null);
		Assert.assertEquals(0, surveyQuestionService.getNumberOfSurveyQuestions(survey));
	}

	@Test
	public void getSurveyQuestionsByQuestionsTest(){
		SurveyQuestion surveyQ1 = new SurveyQuestion();
		surveyQ1.setId(1);
		SurveyQuestion surveyQ2 = new SurveyQuestion();
		surveyQ2.setId(2);
		SurveyQuestion surveyQ3 = new SurveyQuestion();
		surveyQ3.setId(3);
		SurveyQuestion surveyQ4 = new SurveyQuestion();
		surveyQ4.setId(4);
		List<Question> questions = new LinkedList<Question>();
		Question question1 = new Question();
		question1.setId(1);
		questions.add(question1);
		Question question2 = new Question();
		question2.setId(2);
		questions.add(question2);
		Question question3 = new Question();
		question3.setId(3);
		questions.add(question3);
		Question question4 = new Question();
		question4.setId(4);
		questions.add(question4);
		
		Mockito.when(surveyQuestionRepository.findByQuestionId(1)).thenReturn(surveyQ1);
		Mockito.when(surveyQuestionRepository.findByQuestionId(2)).thenReturn(surveyQ2);
		Mockito.when(surveyQuestionRepository.findByQuestionId(3)).thenReturn(surveyQ3);
		Mockito.when(surveyQuestionRepository.findByQuestionId(4)).thenReturn(surveyQ4);
		
		//valid list
		Assert.assertEquals(4, surveyQuestionService.getSurveyQuestionsByQuestions(questions).size());
		
	}
	
	@Test
	public void getSurveyQuestionsByQuestionsTestNull(){
		//null list
		Assert.assertNull(surveyQuestionService.getSurveyQuestionsByQuestions(null));
		//null elements
		List<Question> questions = new LinkedList<Question>();
		questions.add(null);
		questions.add(null);
		Assert.assertNull(surveyQuestionService.getSurveyQuestionsByQuestions(questions));
		
		questions.clear();
		Question question1 = new Question();
		question1.setId(null);
		questions.add(question1);
		Mockito.when(surveyQuestionRepository.findByQuestionId(null)).thenReturn(null);
		Assert.assertNull(surveyQuestionService.getSurveyQuestionsByQuestions(questions));
	}

	@Test
	public void sortByPageAndPositionTest(){
		List<SurveyQuestion> questions = new LinkedList<SurveyQuestion>();
		SurveyQuestion sq1 = new SurveyQuestion();
		sq1.setPageNumber(3);
		sq1.setPosition(3);
		questions.add(sq1);
		SurveyQuestion sq2 = new SurveyQuestion();
		sq2.setPageNumber(1);
		sq2.setPosition(1);
		questions.add(sq2);
		SurveyQuestion sq3 = new SurveyQuestion();
		sq3.setPageNumber(2);
		sq3.setPosition(2);
		questions.add(sq3);
		
		questions = surveyQuestionService.sortByPageAndPosition(questions);
		Assert.assertEquals(1, questions.get(0).getPageNumber());
		Assert.assertEquals(1, questions.get(0).getPosition());
		
		Assert.assertEquals(2, questions.get(1).getPageNumber());
		Assert.assertEquals(2, questions.get(1).getPosition());
		
		Assert.assertEquals(3, questions.get(2).getPageNumber());
		Assert.assertEquals(3, questions.get(2).getPosition());
	}
	
	@Test
	public void sortByPageAndPositionTestInvalidData(){
		List<SurveyQuestion> questions = new LinkedList<SurveyQuestion>();
		SurveyQuestion sq1 = new SurveyQuestion();
		sq1.setPageNumber(3);
		sq1.setPosition(3);
		questions.add(sq1);
		SurveyQuestion sq2 = new SurveyQuestion();
		sq2.setPageNumber(1);
		sq2.setPosition(1);
		questions.add(sq2);
		SurveyQuestion sq3 = new SurveyQuestion();
		sq3.setPageNumber(2);
		sq3.setPosition(2);
		questions.add(sq3);
		
		Assert.assertNull(surveyQuestionService.sortByPageAndPosition(null));
		
		questions.add(null);
		Assert.assertEquals(1, surveyQuestionService.sortByPageAndPosition(questions).get(0).getPageNumber());
		
	}

	@Test
	public void getSortedDtosByPageTest(){
		List<SurveyQuestion> questions = new LinkedList<SurveyQuestion>();
		SurveyQuestion sq1 = new SurveyQuestion();
		sq1.setPageNumber(3);
		sq1.setPosition(3);
		questions.add(sq1);
		SurveyQuestion sq2 = new SurveyQuestion();
		sq2.setPageNumber(1);
		sq2.setPosition(1);
		questions.add(sq2);
		SurveyQuestion sq3 = new SurveyQuestion();
		sq3.setPageNumber(2);
		sq3.setPosition(2);
		questions.add(sq3);
		
		SurveyQuestion sq4 = new SurveyQuestion();
		sq4.setPageNumber(1);
		sq4.setPosition(2);
		questions.add(sq4);
		
		Assert.assertEquals(2, surveyQuestionService.getSortedDtosByPage(questions).get(0).get(1).getPosition());
	}
	
	@Test
	public void getSortedDtosByPageTestWithNull(){
		Assert.assertTrue(surveyQuestionService.getSortedDtosByPage(null).isEmpty());
	}
	
	@Test
	public void getSortedDtosByPageTestWithInvalidData(){
		List<SurveyQuestion> questions = new LinkedList<SurveyQuestion>();
		Assert.assertTrue(surveyQuestionService.getSortedDtosByPage(questions).isEmpty());
		
		questions.add(null);
		questions.add(null);
		Assert.assertTrue(surveyQuestionService.getSortedDtosByPage(questions).isEmpty());		
	}
	
	@Test
	public void getSurveyQuestionDtos(){
		SurveyQuestion s1 = new SurveyQuestion();
		s1.setPosition(23);
		SurveyQuestion s2 = new SurveyQuestion();
		s2.setPosition(9);
		SurveyQuestion s3 = new SurveyQuestion();
		s3.setPosition(18);
		List<SurveyQuestion> questions = Arrays.asList(s1,s2,s3);
		
		List<SurveyQuestionDTO> list = surveyQuestionService.getSurveyQuestionDtos(questions);
		Assert.assertTrue(list.size() == 3);
		Assert.assertTrue(list.get(0).getPosition() == 9);
		Assert.assertTrue(list.get(1).getPosition() == 18);
		Assert.assertTrue(list.get(2).getPosition() == 23);
	}
	
	@Test
	public void getSurveyQuestionDtosByQuestions(){
		Question s1 = new Question();
		s1.setId(23);
		Question s2 = new Question();
		s2.setId(9);
		Question s3 = new Question();
		s3.setId(18);
		List<Question> questions = Arrays.asList(s1,s2,s3);
		
		SurveyQuestion q = new SurveyQuestion();
		q.setId(44);
		Mockito.when(surveyQuestionRepository.findByQuestionId(any(Integer.class))).thenReturn(q);
		
		List<SurveyQuestionDTO> list = surveyQuestionService.getSurveyQuestionDtosByQuestions(questions);
		Assert.assertTrue(list.size() == 3);
		Assert.assertTrue(list.get(0).getId() == 44);
		Assert.assertTrue(list.get(1).getId() == 44);
		Assert.assertTrue(list.get(2).getId() == 44);
	}
}
