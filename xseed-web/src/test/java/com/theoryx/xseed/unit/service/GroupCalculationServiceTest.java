package com.theoryx.xseed.unit.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.theoryx.xseed.dto.GroupCalculationDTO;
import com.theoryx.xseed.model.Calculation;
import com.theoryx.xseed.model.GroupCalculation;
import com.theoryx.xseed.model.Snapshot;
import com.theoryx.xseed.model.StartupCalculation;
import com.theoryx.xseed.repository.GroupCalculationRepository;
import com.theoryx.xseed.service.GroupCalculationServiceImpl;
import com.theoryx.xseed.service.StartupCalculationService;
import com.theoryx.xseed.utils.NumberUtils;

import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class GroupCalculationServiceTest {

	@InjectMocks
	private GroupCalculationServiceImpl groupCalculationService;
	@Mock
	StartupCalculationService startupCalculationService;
	@Mock
	GroupCalculationRepository groupCalculationRepository;

	@Test
	public void test_convertGroupCalculationToGroupCalculationDTO_ok() {
		GroupCalculation groupCalculation = new GroupCalculation(66, null, 111.555555, 222.666666, 333.777777,
				444.888888, 555.999999, 666.183964);
		GroupCalculationDTO dto = groupCalculationService
				.convertGroupCalculationToGroupCalculationDTO(groupCalculation);
		Assert.assertTrue(dto.getClass().getSimpleName().equals("GroupCalculationDTO"));
		Assert.assertTrue(dto.getFormula_a().equals(111.556));
		Assert.assertTrue(dto.getFormula_b().equals(222.667));
		Assert.assertTrue(dto.getFormula_c().equals(333.778));
		Assert.assertTrue(dto.getFormula_d().equals(444.889));
		Assert.assertTrue(dto.getFormula_e().equals(556.000));
		Assert.assertTrue(dto.getFormula_f().equals(666.184));
		Assert.assertTrue(dto.getNumberOfStartups() == 66);
	}

	@Test
	public void test_convertGroupCalculationToGroupCalculationDTO_null_groupCalculation() {
		GroupCalculationDTO dto = groupCalculationService.convertGroupCalculationToGroupCalculationDTO(null);
		Assert.assertTrue(dto == null);
	}

	@Test
	public void test_convert_ok() {
		List<GroupCalculation> groupCalculations = new ArrayList<GroupCalculation>();
		GroupCalculation groupCalculation = new GroupCalculation(66, null, 111.555555, 222.666666, 333.777777,
				444.888888, 555.999999, 666.183964);
		GroupCalculation groupCalculation2 = new GroupCalculation(45128, null, 123.123123, 56.566424, 125.346289,
				134.1648554, 756.465975, 1797.452445);
		groupCalculations.add(groupCalculation);
		groupCalculations.add(groupCalculation2);

		List<GroupCalculationDTO> dtos = groupCalculationService.convert(groupCalculations);
		Assert.assertTrue(dtos.size() == 2);
		for (int i = 0; i < dtos.size(); i++) {
			Assert.assertTrue(dtos.get(i).getClass().getSimpleName().equals("GroupCalculationDTO"));
			Assert.assertTrue(dtos.get(i).getFormula_a().equals(NumberUtils.round(groupCalculations.get(i).getFormula_a())));
			Assert.assertTrue(dtos.get(i).getFormula_b().equals(NumberUtils.round(groupCalculations.get(i).getFormula_b())));
			Assert.assertTrue(dtos.get(i).getFormula_c().equals(NumberUtils.round(groupCalculations.get(i).getFormula_c())));
			Assert.assertTrue(dtos.get(i).getFormula_d().equals(NumberUtils.round(groupCalculations.get(i).getFormula_d())));
			Assert.assertTrue(dtos.get(i).getFormula_e().equals(NumberUtils.round(groupCalculations.get(i).getFormula_e())));
			Assert.assertTrue(dtos.get(i).getFormula_f().equals(NumberUtils.round(groupCalculations.get(i).getFormula_f())));
			Assert.assertTrue(dtos.get(i).getNumberOfStartups() == groupCalculations.get(i).getNumberOfStartups());
		}
	}
	
	@Test
	public void test_convert_null_list() {
		List<GroupCalculationDTO> dtos = groupCalculationService.convert(null);
		Assert.assertTrue(dtos.isEmpty());
	}
	
	@Test
	public void test_createGroupCalculationLine_null_group() {
		GroupCalculation g = groupCalculationService.createGroupCalculationLine(null, new ArrayList<Snapshot>(), new Calculation());
		Assert.assertTrue(g == null);
	}
	
	@Test
	public void test_createGroupCalculationLine_empty_group() {
		GroupCalculation g = groupCalculationService.createGroupCalculationLine(new ArrayList<Snapshot>(), Arrays.asList(new Snapshot()), new Calculation());
		Assert.assertTrue(g == null);
	}
	
	@Test
	public void test_createGroupCalculationLine_null_filteredSnapshots() {
		GroupCalculation g = groupCalculationService.createGroupCalculationLine(new ArrayList<Snapshot>(), null, new Calculation());
		Assert.assertTrue(g == null);
	}
	
	@Test
	public void test_createGroupCalculationLine_empty_filteredSnapshots() {
		GroupCalculation g = groupCalculationService.createGroupCalculationLine(Arrays.asList(new Snapshot()), null, new Calculation());
		Assert.assertTrue(g == null);
	}
	
	@Test
	public void test_createGroupCalculationLine_null_calculation() {
		GroupCalculation g = groupCalculationService.createGroupCalculationLine(Arrays.asList(new Snapshot()), Arrays.asList(new Snapshot()), null);
		Assert.assertTrue(g == null);
	}
	
	@Test
	public void test_createGroupCalculationLine_calculation_not_real() {
		Calculation c = new Calculation();
		c.setId(55555);
		Snapshot sn = new Snapshot();
		sn.setId(5);
		Mockito.when(startupCalculationService.findBySnapshotIdAndCalculationId(any(Integer.class), any(Integer.class))).thenReturn(null);
		GroupCalculation g = groupCalculationService.createGroupCalculationLine(Arrays.asList(sn), Arrays.asList(new Snapshot()), c);
		Assert.assertTrue(g == null);
	}
	
	@Test
	public void test_createGroupCalculationLine_calculation_not_null() {
		Calculation c = new Calculation();
		c.setId(55555);
		StartupCalculation sc = new StartupCalculation();
		sc.setFormula_a(56.123);
		Snapshot s1 = new Snapshot();s1.setId(1);
		Snapshot s2 = new Snapshot();s2.setId(2);
		Snapshot s3 = new Snapshot();s3.setId(3);
		Snapshot s4 = new Snapshot();s4.setId(4);
		Snapshot s5 = new Snapshot();s5.setId(5);
		Mockito.when(startupCalculationService.findBySnapshotIdAndCalculationId(any(Integer.class), any(Integer.class))).thenReturn(sc);
		Mockito.when(groupCalculationRepository.save(any(GroupCalculation.class))).thenReturn(new GroupCalculation());
		GroupCalculation g = groupCalculationService.createGroupCalculationLine(Arrays.asList(s1,s2,s3), Arrays.asList(s4,s5), c);
		Assert.assertTrue(g != null);
	}
	
	@Test
	public void test_createGroupCalculations() {
		GroupCalculation gc = new GroupCalculation();
		gc.setFormula_e(56.123);
		Snapshot s1 = new Snapshot();s1.setId(1);
		Snapshot s2 = new Snapshot();s2.setId(2);
		Snapshot s3 = new Snapshot();s3.setId(3);
		Snapshot s4 = new Snapshot();s4.setId(4);
		Snapshot s5 = new Snapshot();s5.setId(5);
		List<List<Snapshot>> sn = Arrays.asList(Arrays.asList(s1,s2),Arrays.asList(s3,s4),Arrays.asList(s5));
		StartupCalculation sc = new StartupCalculation();
		sc.setFormula_a(56.123);
		Mockito.when(startupCalculationService.findBySnapshotIdAndCalculationId(any(Integer.class), any(Integer.class))).thenReturn(sc);
		Mockito.when(groupCalculationRepository.save(any(GroupCalculation.class))).thenReturn(gc);
		Double g = groupCalculationService.createGroupCalculations(sn, Arrays.asList(s1,s2,s5), new Calculation());
		Assert.assertTrue(g != null);
		Assert.assertTrue(g == 3*56.123);
	}

}
