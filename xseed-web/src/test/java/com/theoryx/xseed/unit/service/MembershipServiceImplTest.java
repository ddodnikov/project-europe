package com.theoryx.xseed.unit.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.repository.MembershipRepository;
import com.theoryx.xseed.service.MembershipServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class MembershipServiceImplTest {

	@InjectMocks
	private MembershipServiceImpl membershipService;
	@Mock
	private MembershipRepository membershipRepository;
	
	@Test
	public void createMembership_null(){
		Mockito.when(membershipRepository.save(any(Membership.class))).thenReturn(null);
		Assert.isNull(membershipService.createMembership(null, null));
	}
	
	@Test
	public void createMembership_notnull(){
		Mockito.when(membershipRepository.save(any(Membership.class))).thenReturn(new Membership());
		Assert.notNull(membershipService.createMembership(null, null));
		Assert.isTrue(membershipService.createMembership(null, null).getClass().getSimpleName().equals("Membership"));
	}
	
	@Test
	public void saveMembership_null(){
		Mockito.when(membershipRepository.save(any(Membership.class))).thenReturn(null);
		Assert.isNull(membershipService.saveMembership(null));
	}
	
	@Test
	public void saveMembership_notnull(){
		Mockito.when(membershipRepository.save(any(Membership.class))).thenReturn(new Membership());
		Assert.notNull(membershipService.saveMembership(null));
		Assert.isTrue(membershipService.saveMembership(null).getClass().getSimpleName().equals("Membership"));
	}
	
	@Test
	public void checkNoMembership_null(){
		Assert.isTrue(!membershipService.checkNoMembership(null));
	}
	
	@Test
	public void checkNoMembership_notnull_empty(){
		UserDTO user = new UserDTO();
		user.setId(2);
		Mockito.when(membershipRepository.findByUserId(any(Integer.class))).thenReturn(new ArrayList<Membership>());
		Assert.isTrue(membershipService.checkNoMembership(user));
	}
	
	@Test
	public void checkNoMembership_notnull_notempty(){
		UserDTO user = new UserDTO();
		user.setId(2);
		List<Membership> list = new ArrayList<Membership>();
		list.add(new Membership());
		Mockito.when(membershipRepository.findByUserId(any(Integer.class))).thenReturn(list);
		Assert.isTrue(!membershipService.checkNoMembership(user));
	}
	
	@Test
	public void convertMembershipToMembershipDTOTest(){
		Membership membership = new Membership();
		membership.setId(1);
		Assert.isTrue(new Integer(1).equals(membershipService.convertMembershipToMembershipDTO(membership).getId()));
	}
	
	@Test
	public void convertMembershipToMembershipDTOTestWithNull(){
		Assert.isNull(membershipService.convertMembershipToMembershipDTO(null).getId());
	}
	
	@Test
	public void convertMembershipToMembershipDTOTestWithEmptyData(){
		Assert.isNull(membershipService.convertMembershipToMembershipDTO(new Membership()).getId());
	}
	
	@Test
	public void getMembershipByUserIdAndStartupId_null(){
		Assert.isNull(membershipService.getMembershipByUserIdAndStartupId(null, null));
	}
	
	@Test
	public void getMembershipByUserIdAndStartupId_notnull(){
		Mockito.when(membershipRepository.findByUserIdAndStartupId(1, 1)).thenReturn(new Membership());
		Assert.notNull(membershipService.getMembershipByUserIdAndStartupId(1, 1));
	}
	
	@Test
	public void getMembershipByUserAndStartup_null(){
		Assert.isNull(membershipService.getMembershipByUserAndStartup(null, new StartupDTO()));
	}
	
	@Test
	public void getMembershipByUserAndStartup_notnull(){
		StartupDTO startup = new StartupDTO();
		startup.setId(1);
		UserDTO user = new UserDTO();
		user.setId(1);
		Mockito.when(membershipRepository.findByUserIdAndStartupId(1, 1)).thenReturn(new Membership());
		Assert.notNull(membershipService.getMembershipByUserAndStartup(user, startup));
	}
	
	@Test
	public void getMembershipByUserAndStartup_null2(){
		StartupDTO startup = new StartupDTO();
		startup.setId(1);
		UserDTO user = new UserDTO();
		user.setId(1);
		Mockito.when(membershipRepository.findByUserIdAndStartupId(1, 1)).thenReturn(null);
		Assert.isNull(membershipService.getMembershipByUserAndStartup(user, startup));
	}
	
	@Test
	public void getMembershipByUserAndStartup2_null(){
		Assert.isNull(membershipService.getMembershipByUserAndStartup(null, new Startup()));
	}
	
	@Test
	public void getMembershipByUserAndStartup2_notnull(){
		Startup startup = new Startup();
		startup.setId(1);
		User user = new User();
		user.setId(1);
		Mockito.when(membershipRepository.findByUserIdAndStartupId(1, 1)).thenReturn(new Membership());
		Assert.notNull(membershipService.getMembershipByUserAndStartup(user, startup));
	}

}
