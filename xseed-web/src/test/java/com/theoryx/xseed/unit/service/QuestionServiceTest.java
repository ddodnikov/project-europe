package com.theoryx.xseed.unit.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import com.theoryx.xseed.dto.AnswerOptionDTO;
import com.theoryx.xseed.dto.QuestionDTO;
import com.theoryx.xseed.model.AnswerGroup;
import com.theoryx.xseed.model.AnswerOption;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.QuestionAnswerType;
import com.theoryx.xseed.model.QuestionCategory;
import com.theoryx.xseed.model.Snapshot;
import com.theoryx.xseed.model.SnapshotLine;
import com.theoryx.xseed.repository.QuestionRepository;
import com.theoryx.xseed.service.AnswerOptionService;
import com.theoryx.xseed.service.QuestionServiceImpl;
import com.theoryx.xseed.service.SnapshotlineServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class QuestionServiceTest {

	@InjectMocks
	private QuestionServiceImpl questionService;
	@Mock
	private QuestionRepository questionRepository;
	@Mock
	private AnswerOptionService answerOptionService;
	@InjectMocks
	private SnapshotlineServiceImpl snapshotlineService;

	@Test
	public void test_convertQuestionToQuestionDto_ok() {
		Question question = new Question();
		question.setId(5);
		question.setText("aiugaidvbouad");
		question.setHasOther(true);
		question.setAlgo(false);
		QuestionCategory cat = new QuestionCategory(15, QuestionAnswerType.MULTIPLE_CHOICE);
		question.setCategory(cat);

		QuestionDTO dto = questionService.convertQuestionToQuestionDto(question);

		Assert.isTrue(dto.getClass().getSimpleName().equals("QuestionDTO"));
		Assert.isTrue(dto.getText().equals(question.getText()));
		Assert.isTrue(dto.getId().equals(question.getId()));
		Assert.isTrue(dto.isAlgo() == question.isAlgo());
		Assert.isTrue(dto.isHasOther() == question.isHasOther());
		Assert.isTrue(dto.getType().equals(question.getCategory().getType().toString()));
		Assert.isTrue(dto.getAnswers() == null);
	}
	
	@Test
	public void test_convertQuestionToQuestionDto_notnullgroup() {
		Question question = new Question();
		question.setId(5);
		question.setText("aiugaidvbouad");
		question.setHasOther(true);
		question.setAlgo(false);
		question.setAnswergroup(new AnswerGroup());
		QuestionCategory cat = new QuestionCategory(15, QuestionAnswerType.MULTIPLE_CHOICE);
		question.setCategory(cat);
		Mockito.when(answerOptionService.getAnswerOptionDtos(question)).thenReturn(new ArrayList<AnswerOptionDTO>());

		QuestionDTO dto = questionService.convertQuestionToQuestionDto(question);

		Assert.isTrue(dto.getClass().getSimpleName().equals("QuestionDTO"));
		Assert.isTrue(dto.getText().equals(question.getText()));
		Assert.isTrue(dto.getId().equals(question.getId()));
		Assert.isTrue(dto.isAlgo() == question.isAlgo());
		Assert.isTrue(dto.isHasOther() == question.isHasOther());
		Assert.isTrue(dto.getType().equals(question.getCategory().getType().toString()));
		Assert.isTrue(dto.getAnswers() != null);
	}

	@Test
	public void test_convert_ok() {
		List<Question> questions = new ArrayList<Question>();
		Question question1 = new Question();
		question1.setId(5);
		question1.setText("aiugaidvbouad");
		question1.setHasOther(true);
		question1.setAlgo(false);
		QuestionCategory cat1 = new QuestionCategory(15, QuestionAnswerType.MULTIPLE_CHOICE);
		question1.setCategory(cat1);
		questions.add(question1);
		questions.add(question1);

		List<QuestionDTO> questiondtos = questionService.convert(questions);
		Assert.isTrue(questiondtos.size() == 2);
		for (int i = 0; i < questiondtos.size(); i++) {
			Assert.isTrue(questiondtos.get(i).getClass().getSimpleName().equals("QuestionDTO"));
			Assert.isTrue(questiondtos.get(i).getText().equals(questions.get(i).getText()));
			Assert.isTrue(questiondtos.get(i).getId().equals(questions.get(i).getId()));
			Assert.isTrue(questiondtos.get(i).isAlgo() == questions.get(i).isAlgo());
			Assert.isTrue(questiondtos.get(i).isHasOther() == questions.get(i).isHasOther());
			Assert.isTrue(questiondtos.get(i).getType().equals(questions.get(i).getCategory().getType().toString()));
			Assert.isTrue(questiondtos.get(i).getAnswers() == null);
		}
	}

	@Test
	public void test_convertQuestionToQuestionDto_null_question() {
		QuestionDTO dto = questionService.convertQuestionToQuestionDto(null);
		Assert.isTrue(dto == null);
	}

	@Test(expected = NullPointerException.class)
	public void test_convertQuestionToQuestionDto_null_category() {
		Question question = new Question();
		questionService.convertQuestionToQuestionDto(question);
	}

	@Test(expected = NullPointerException.class)
	public void test_convertQuestionToQuestionDto_null_category_type() {
		Question question = new Question();
		question.setCategory(new QuestionCategory(42, null));
		questionService.convertQuestionToQuestionDto(question);
	}

	@Test
	public void test_convert_ok_null_list() {
		List<QuestionDTO> questiondtos = questionService.convert(null);
		Assert.isTrue(questiondtos == null);
	}
	
	@Test
	public void test_convert_ok_empty_list() {
		List<QuestionDTO> questiondtos = questionService.convert(new ArrayList<Question>());
		Assert.isTrue(questiondtos.isEmpty());
	}
	
	@Test
	public void test_convertQuestionDTOToQuestion_null() {
		Assert.isNull(questionService.convertQuestionDTOToQuestion(null));
	}
	
	@Test
	public void test_convertQuestionDTOToQuestion_nullid() {
		QuestionDTO q = new QuestionDTO();
		Assert.notNull(questionService.convertQuestionDTOToQuestion(q));
	}
	
	@Test
	public void test_convertQuestionDTOToQuestion_notnullid() {
		QuestionDTO q = new QuestionDTO();
		q.setId(1);
		Mockito.when(questionRepository.findOne(1)).thenReturn(new Question());
		Assert.notNull(questionService.convertQuestionDTOToQuestion(q));
	}
	
	@Test
	public void test_filterAlgoQuestions() {
		List<Question> algoQuestions = getAlgoQuestions();
		List<Snapshot> filteredSnapshots = getFilteredSnapshots(algoQuestions);
		questionService.setSnapshotlineService(snapshotlineService);
		List<Question> qs = questionService.filterAlgoQuestions(filteredSnapshots, algoQuestions);
		Assert.notNull(qs);
		Assert.isTrue(qs.size() == 1);
	}
	
	private List<Question> getAlgoQuestions(){
		List<Question> algoQuestions = new ArrayList<Question>();
		Question q1 = new Question();
		q1.setId(1);
		QuestionCategory cat1 = new QuestionCategory(1, QuestionAnswerType.YES_NO);
		q1.setCategory(cat1);
		algoQuestions.add(q1);
		Question q2 = new Question();
		q2.setId(2);
		QuestionCategory cat2 = new QuestionCategory(2, QuestionAnswerType.SINGLE_CHOICE);
		q2.setCategory(cat2);
		algoQuestions.add(q2);
		Question q3 = new Question();
		q3.setId(3);
		QuestionCategory cat3 = new QuestionCategory(3, QuestionAnswerType.TEXT);
		q3.setCategory(cat3);
		algoQuestions.add(q3);
		Question q4 = new Question();
		q4.setId(4);
		QuestionCategory cat4 = new QuestionCategory(4, QuestionAnswerType.YES_NO);
		q4.setCategory(cat4);
		algoQuestions.add(q4);
		return algoQuestions;
	}
	
	private List<Snapshot> getFilteredSnapshots(List<Question> algoQuestions){
		List<Snapshot> filteredSnapshots = new ArrayList<Snapshot>();
		
		Snapshot s1 = new Snapshot();
		
		SnapshotLine l1 = new SnapshotLine();
		l1.setQuestion(algoQuestions.get(0));
		AnswerOption a1 = new AnswerOption();
		a1.setId(1);
		l1.setSelected_answer(a1);
		
		SnapshotLine l2 = new SnapshotLine();
		l2.setQuestion(new Question());
		AnswerOption a2 = new AnswerOption();
		a2.setId(2);
		l2.setSelected_answer(a2);
		
		SnapshotLine l7 = new SnapshotLine();
		l7.setQuestion(algoQuestions.get(3));
		AnswerOption a7 = new AnswerOption();
		a7.setId(7);
		l7.setSelected_answer(a7);
		
		s1.setSnapshotlines(Arrays.asList(l1,l2,l7));
		Snapshot s2 = new Snapshot();
		
		SnapshotLine l3 = new SnapshotLine();
		l3.setQuestion(algoQuestions.get(0));
		AnswerOption a3 = new AnswerOption();
		a3.setId(3);
		l3.setSelected_answer(a3);
		
		SnapshotLine l4 = new SnapshotLine();
		l4.setQuestion(algoQuestions.get(1));
		AnswerOption a4 = new AnswerOption();
		a4.setId(4);
		l4.setSelected_answer(a4);
		
		SnapshotLine l8 = new SnapshotLine();
		l8.setQuestion(algoQuestions.get(3));
		AnswerOption a8 = new AnswerOption();
		a8.setId(7);
		l8.setSelected_answer(a8);
		
		s2.setSnapshotlines(Arrays.asList(l3,l4,l8));
		Snapshot s3 = new Snapshot();
		
		SnapshotLine l5 = new SnapshotLine();
		l5.setQuestion(algoQuestions.get(0));
		AnswerOption a5 = new AnswerOption();
		a5.setId(5);
		l5.setSelected_answer(a5);
		
		SnapshotLine l6 = new SnapshotLine();
		l6.setQuestion(algoQuestions.get(1));
		AnswerOption a6 = new AnswerOption();
		a6.setId(6);
		l6.setSelected_answer(a6);
		
		SnapshotLine l9 = new SnapshotLine();
		l9.setQuestion(algoQuestions.get(3));
		AnswerOption a9 = new AnswerOption();
		a9.setId(7);
		l9.setSelected_answer(a9);
		
		s2.setSnapshotlines(Arrays.asList(l5,l6,l9));
		filteredSnapshots.add(s1);
		filteredSnapshots.add(s2);
		filteredSnapshots.add(s3);
		return filteredSnapshots;
	}
	
	@Test
	public void test_findAll_empty() {
		Mockito.when(questionRepository.findAll()).thenReturn(new ArrayList<Question>());
		Assert.isTrue(questionService.findAll().isEmpty());
	}
	
	@Test
	public void test_findAll_null() {
		Mockito.when(questionRepository.findAll()).thenReturn(null);
		Assert.isNull(questionService.findAll());
	}
	
	@Test
	public void test_findAll_notempty() {
		List<Question> qs = new ArrayList<Question>();
		Question q1 = new Question();
		q1.setId(2);
		Question q2 = new Question();
		q2.setId(1);
		qs.add(q1);
		qs.add(q2);
		Mockito.when(questionRepository.findAll()).thenReturn(qs);
		Assert.isTrue(questionService.findAll().size() == 2);
	}
	
	@Test
	public void test_save() {
		Question q = null;
		Mockito.when(questionRepository.save(q)).thenReturn(new Question());
		Assert.notNull(questionService.save(null));
	}
}
