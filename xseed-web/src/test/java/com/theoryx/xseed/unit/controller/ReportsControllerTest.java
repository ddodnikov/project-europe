package com.theoryx.xseed.unit.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.theoryx.xseed.controller.ReportsController;
import com.theoryx.xseed.dto.ReportAnswer;
import com.theoryx.xseed.dto.ReportRow;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.service.ReportsService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ReportsControllerTest {

	private MockMvc mockMvc;
	
	@Mock
	private ReportsService reportsService;
	@Mock
	private MessageSource messageSource;
	@InjectMocks
	private ReportsController reportsController;
	
	private List<UrlLabelMapping> breadcrumbs;
	private LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>> rows;
	
	@Before
	public void setUp(){
		// Process mock annotations
		MockitoAnnotations.initMocks(this);

		//ViewResolver
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp");
		viewResolver.setSuffix(".jsp");

		// Setup Spring test in standalone mode
		// Sets the vewresolver
		mockMvc = MockMvcBuilders.standaloneSetup(reportsController).setViewResolvers(viewResolver).build();
		
		//breadcrumbs;
		breadcrumbs = new LinkedList<UrlLabelMapping>();
		UrlLabelMapping home = new UrlLabelMapping("/home", "Home", true);
		breadcrumbs.add(home);
		
		//reportRows
		rows = new LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>>();
		rows.put("PC1", new LinkedHashMap<String, LinkedList<ReportRow>>());
		rows.get("PC1").put("GC1", new LinkedList<ReportRow>());
		rows.get("PC1").get("GC1").add(new ReportRow());
		rows.get("PC1").get("GC1").get(0).setAnswers(new LinkedList<ReportAnswer>());
		rows.get("PC1").get("GC1").get(0).getAnswers().add(new ReportAnswer());
		rows.get("PC1").get("GC1").get(0).getAnswers().add(new ReportAnswer());
	}
	
	@Test
	public void testGetAllReports(){
		try {
			mockMvc.perform(get("/reports")
					.requestAttr("breadCrumbs", breadcrumbs)
			).andExpect(status().isOk())	
					.andExpect(view().name("/reports"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/reports.jsp"))
					.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetReport(){
		Mockito.when(reportsService.getReportRows(1)).thenReturn(rows);
		StartupDTO startup =  new StartupDTO();
		startup.setId(1);
		try {
			mockMvc.perform(get("/progressreport")
					.sessionAttr("currentStartup", startup))
			.andExpect(status().isOk())
			.andExpect(view().name("/report-details"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/report-details.jsp"))
			.andExpect(model().attributeExists("data"))
			.andExpect(model().attributeExists("months"))
			.andExpect(model().attributeExists("phaseRows"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetReportErrors(){
		Mockito.when(reportsService.getReportRows(null)).thenReturn(null);
		
		try {
			mockMvc.perform(get("/progressreport")
					.sessionAttr("currentStartup", new StartupDTO()))
			.andExpect(status().isOk())
			.andExpect(view().name("/report-details"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/report-details.jsp"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

