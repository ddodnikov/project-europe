package com.theoryx.xseed.unit.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.theoryx.xseed.controller.SurveyController;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.SurveyDTO;
import com.theoryx.xseed.dto.SurveyQuestionDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.AnswerGroup;
import com.theoryx.xseed.model.AnswerOption;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.QuestionAnswerType;
import com.theoryx.xseed.model.QuestionCategory;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.Survey;
import com.theoryx.xseed.model.SurveyQuestion;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.service.AnswerOptionService;
import com.theoryx.xseed.service.SnapshotService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.SurveyQuestionService;
import com.theoryx.xseed.service.SurveyService;
import com.theoryx.xseed.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class SurveyControllerTest {
	
	private MockMvc mockMvc;

	@Mock
	private MessageSource messageSource;
	@Mock
	private SurveyService surveyService;
	@Mock
	private UserService userService;
	@Mock
	private StartupService startupService;
	@Mock
	private SnapshotService snapshotService;
	@Mock
	private SurveyQuestionService surveyQuestionService;
	@Mock
	private AnswerOptionService answerOptionService;
	
	@InjectMocks
	private SurveyController surveyController;
	
	@Before
	public void setUp() {	
		
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
		
		ResourceBundleMessageSource messageSource2 = new ResourceBundleMessageSource();
		messageSource2.setBasename("locale/language");

		//ViewResolver
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp");
		viewResolver.setSuffix(".jsp");

		// Setup Spring test in standalone mode
		// Sets the vewresolver
		mockMvc = MockMvcBuilders.standaloneSetup(surveyController).setViewResolvers(viewResolver).build();

		messageSource = messageSource2;
		surveyController.setMessageSource(messageSource);
	}
	
	@Test
	public void test_getSurveysPage_nouser() {
		try {
			mockMvc.perform(get("/surveys"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveysPage_user() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		Mockito.when(surveyService.getAllSurveyDtos()).thenReturn(new ArrayList<SurveyDTO>());
		try {
			mockMvc.perform(get("/surveys")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", new UserDTO()))
				.andExpect(status().isOk())
				.andExpect(view().name("/surveys"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/surveys.jsp"))
				.andExpect(model().attributeExists("surveys"))
				.andExpect(model().attribute("surveys", hasSize(0)))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage_nouser() {
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.param("id", "4"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage_user_noquestions() {
		Mockito.when(surveyService.getById(4)).thenReturn(null);
		SurveyDTO s = new SurveyDTO();
		s.setNumberOfQuestions(0);
		Mockito.when(surveyService.convertSurveyToSurveyDTO(any(Survey.class))).thenReturn(s);
		Mockito.when(surveyService.getAllSurveyDtos()).thenReturn(new ArrayList<SurveyDTO>());
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.sessionAttr("currentUser", new UserDTO())
					.param("id", "4"))
				.andExpect(status().isOk())
				.andExpect(view().name("/surveys"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/surveys.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attributeExists("surveys"))
				.andExpect(model().attribute("surveys", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage_user_questions() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		breadcrumbs.add(new UrlLabelMapping("", "", false));
		Mockito.when(surveyService.getById(4)).thenReturn(new Survey());
		SurveyDTO s = new SurveyDTO();
		s.setNumberOfQuestions(5);
		Mockito.when(surveyService.convertSurveyToSurveyDTO(any(Survey.class))).thenReturn(s);
		Mockito.when(surveyQuestionService.getSortedDtosByPage(null)).thenReturn(new ArrayList<List<SurveyQuestionDTO>>());
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", new UserDTO())
					.param("id", "4"))
				.andExpect(status().isOk())
				.andExpect(view().name("/startsurvey"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/startsurvey.jsp"))
				.andExpect(model().attributeExists("surveyQuestionDtosPaged"))
				.andExpect(model().attribute("surveyQuestionDtosPaged", hasSize(0)))
				.andExpect(model().attributeExists("currentSurvey"))
				.andExpect(model().attributeExists("snapshotName"))
				.andExpect(model().attributeExists("surveyQuestionsDTOSNumberOfPages"))
				.andExpect(model().attribute("surveyQuestionsDTOSNumberOfPages", equalTo(0)))
				.andExpect(model().attributeDoesNotExist("errors"))
				.andExpect(model().attributeDoesNotExist("surveys"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(1)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage_user_questions_nomapping() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		Mockito.when(surveyService.getById(4)).thenReturn(new Survey());
		SurveyDTO s = new SurveyDTO();
		s.setNumberOfQuestions(5);
		Mockito.when(surveyService.convertSurveyToSurveyDTO(any(Survey.class))).thenReturn(s);
		Mockito.when(surveyQuestionService.getSortedDtosByPage(null)).thenReturn(new ArrayList<List<SurveyQuestionDTO>>());
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", new UserDTO())
					.param("id", "4"))
				.andExpect(status().isOk())
				.andExpect(view().name("/startsurvey"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/startsurvey.jsp"))
				.andExpect(model().attributeExists("surveyQuestionDtosPaged"))
				.andExpect(model().attribute("surveyQuestionDtosPaged", hasSize(0)))
				.andExpect(model().attributeExists("currentSurvey"))
				.andExpect(model().attributeExists("snapshotName"))
				.andExpect(model().attributeExists("surveyQuestionsDTOSNumberOfPages"))
				.andExpect(model().attribute("surveyQuestionsDTOSNumberOfPages", equalTo(0)))
				.andExpect(model().attributeDoesNotExist("errors"))
				.andExpect(model().attributeDoesNotExist("surveys"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage_user_questions_emptymapping() {
		Mockito.when(surveyService.getById(4)).thenReturn(new Survey());
		SurveyDTO s = new SurveyDTO();
		s.setNumberOfQuestions(5);
		Mockito.when(surveyService.convertSurveyToSurveyDTO(any(Survey.class))).thenReturn(s);
		Mockito.when(surveyQuestionService.getSortedDtosByPage(null)).thenReturn(new ArrayList<List<SurveyQuestionDTO>>());
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.sessionAttr("currentUser", new UserDTO())
					.param("id", "4"))
				.andExpect(status().isOk())
				.andExpect(view().name("/startsurvey"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/startsurvey.jsp"))
				.andExpect(model().attributeExists("surveyQuestionDtosPaged"))
				.andExpect(model().attribute("surveyQuestionDtosPaged", hasSize(0)))
				.andExpect(model().attributeExists("currentSurvey"))
				.andExpect(model().attributeExists("snapshotName"))
				.andExpect(model().attributeExists("surveyQuestionsDTOSNumberOfPages"))
				.andExpect(model().attribute("surveyQuestionsDTOSNumberOfPages", equalTo(0)))
				.andExpect(model().attributeDoesNotExist("errors"))
				.andExpect(model().attributeDoesNotExist("surveys"))
				.andExpect(model().attributeDoesNotExist("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_submitSurvey_null() {
		Mockito.when(surveyService.getById(4)).thenReturn(null);
		try {
			mockMvc.perform(post("/submit/survey")
					.param("id", "4"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/snapshots"))
				.andExpect(redirectedUrl("/snapshots"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_submitSurvey_notnull_nullsnapshotLines() {
		Mockito.when(surveyService.getById(4)).thenReturn(new Survey());
		Mockito.when(surveyQuestionService.sortByPageAndPosition(null)).thenReturn(null);
		String json = "{\"message\":\"" + 
				messageSource.getMessage("error.submit", new Object[0], Locale.getDefault()) + 
				"\",\"status\":\"error\"}";
		try {
			mockMvc.perform(post("/submit/survey")
					.param("id", "4"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/snapshots"))
				.andExpect(redirectedUrl("/snapshots"))
				.andExpect(flash().attributeExists("message"))
				.andExpect(flash().attribute("message", isA(JSONObject.class)))
				.andExpect(flash().attribute("message", hasToString(json)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_submitSurvey_notnull_notnull() {
		Mockito.when(surveyService.getById(4)).thenReturn(new Survey());
		Mockito.when(surveyQuestionService.sortByPageAndPosition(null)).thenReturn(new ArrayList<SurveyQuestion>());
		Mockito.when(userService.convertUserDTOToUser(any(UserDTO.class))).thenReturn(null);
		Mockito.when(startupService.convertStartupDTOToStartup(any(StartupDTO.class))).thenReturn(null);
		Mockito.when(snapshotService.save(any(String.class), any(Survey.class), any(User.class), any(Startup.class), any(List.class))).thenReturn(null);
		String json = "{\"message\":\"" + 
				messageSource.getMessage("success.snapshot-saved", new Object[0], Locale.getDefault()) + 
				"\",\"status\":\"success\"}";
		try {
			mockMvc.perform(post("/submit/survey")
					.sessionAttr("currentUser", new UserDTO())
					.sessionAttr("currentStartup", new StartupDTO())
					.param("snapshotName", "asdasdadsw")
					.param("id", "4"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/snapshots"))
				.andExpect(redirectedUrl("/snapshots"))
				.andExpect(flash().attributeExists("message"))
				.andExpect(flash().attribute("message", isA(JSONObject.class)))
				.andExpect(flash().attribute("message", hasToString(json)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_submitSurvey_fullinfo() {
		Survey s = new Survey();
		List<SurveyQuestion> list = sq(s);
		s.setQuestions(list);
		s.setId(4);
		
		Mockito.when(surveyService.getById(any(Integer.class))).thenReturn(s);
		Mockito.when(answerOptionService.findById(4)).thenReturn(null);
		Mockito.when(answerOptionService.findByText(any(String.class))).thenReturn(new AnswerOption());
		Mockito.when(userService.convertUserDTOToUser(any(UserDTO.class))).thenReturn(null);
		Mockito.when(startupService.convertStartupDTOToStartup(any(StartupDTO.class))).thenReturn(null);
		Mockito.when(snapshotService.save(any(String.class), any(Survey.class), any(User.class), any(Startup.class), any(List.class))).thenReturn(null);
		Mockito.when(surveyQuestionService.sortByPageAndPosition(any(List.class))).thenReturn(list);
		
		String json = "{\"message\":\"" + 
				messageSource.getMessage("success.snapshot-saved", new Object[0], Locale.getDefault()) + 
				"\",\"status\":\"success\"}";
		try {
			mockMvc.perform(post("/submit/survey")
					.sessionAttr("currentUser", new UserDTO())
					.sessionAttr("currentStartup", new StartupDTO())
					.param("snapshotName", "asdasdadsw")
					.param("id", "4")
					.param("text_56", "TextQuestion")
					.param("radio_54", "33")
					.param("yesno_57", "YesNoQuestion")
					.param("multiple_55_111", "MultipleQuestion"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/snapshots"))
				.andExpect(redirectedUrl("/snapshots"))
				.andExpect(flash().attributeExists("message"))
				.andExpect(flash().attribute("message", isA(JSONObject.class)))
				.andExpect(flash().attribute("message", hasToString(json)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<SurveyQuestion> sq(Survey survey){
		List<SurveyQuestion> list = new ArrayList<SurveyQuestion>();
		
		QuestionAnswerType type = QuestionAnswerType.SINGLE_CHOICE;
		QuestionCategory cat = new QuestionCategory(4, type);
		Question q = new Question(54, cat);
		SurveyQuestion sq = new SurveyQuestion(survey, q, 1, 1);
		
		QuestionAnswerType type2 = QuestionAnswerType.MULTIPLE_CHOICE;
		QuestionCategory cat2 = new QuestionCategory(5, type2);
		Question q2 = new Question(55, cat2);
		AnswerGroup g = new AnswerGroup();
		AnswerOption o = new AnswerOption();
		o.setId(111);
		g.setOptions(Arrays.asList(o));
		q2.setAnswergroup(g);
		SurveyQuestion sq2 = new SurveyQuestion(survey, q2, 1, 2);
		
		QuestionAnswerType type3 = QuestionAnswerType.TEXT;
		QuestionCategory cat3 = new QuestionCategory(6, type3);
		Question q3 = new Question(56, cat3);
		SurveyQuestion sq3 = new SurveyQuestion(survey, q3, 2, 1);
		
		QuestionAnswerType type4 = QuestionAnswerType.YES_NO;
		QuestionCategory cat4 = new QuestionCategory(7, type4);
		Question q4 = new Question(57, cat4);
		SurveyQuestion sq4 = new SurveyQuestion(survey, q4, 2, 2);
		
		list.add(sq);
		list.add(sq2);
		list.add(sq3);
		list.add(sq4);
		
		return list;
	}

}
