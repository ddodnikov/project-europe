package com.theoryx.xseed.unit.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.Matchers.any;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.theoryx.xseed.controller.SnapshopController;
import com.theoryx.xseed.dto.SnapshotDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.dto.UserDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.theoryx.xseed.service.SnapshotService;

@RunWith(MockitoJUnitRunner.class)
public class SnapshotControllerTest {

	private MockMvc mockMvc;

	@Mock
	private SnapshotService snapshotService;
	
	@InjectMocks
	private SnapshopController snapshopController;

	@Before
	public void setUp() {

		// Process mock annotations
		MockitoAnnotations.initMocks(this);

		// ViewResolver
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp");
		viewResolver.setSuffix(".jsp");

		// Setup Spring test in standalone mode
		// Sets the vewresolver
		mockMvc = MockMvcBuilders.standaloneSetup(snapshopController).setViewResolvers(viewResolver).build();
	}

	@Test
	public void test_getAllSnapshots_nouser() {
		try {
			mockMvc.perform(get("/snapshots"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getAllSnapshots_user() {
		Mockito.when(snapshotService.findAllSnapshotsByStartup(null)).thenReturn(new ArrayList<SnapshotDTO>());
		try {
			List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(get("/snapshots").sessionAttr("currentUser", new UserDTO())
					.sessionAttr("currentStartup", new StartupDTO()).requestAttr("breadCrumbs", breadcrumbs))
					.andExpect(status().isOk())
					.andExpect(view().name("/snapshots"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/snapshots.jsp"))
					.andExpect(model().attributeExists("snapshots"))
					.andExpect(model().attribute("snapshots", instanceOf(List.class)))
					.andExpect(model().attributeExists("breadcrumbs"))
					.andExpect(model().attribute("breadcrumbs", instanceOf(List.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getSnapshotDetails_nouser() {
		try {
			mockMvc.perform(get("/snapshot-details/4")).andExpect(status().isFound())
					.andExpect(view().name("redirect:/403")).andExpect(redirectedUrl("/403"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getSnapshotDetails_user_nullstartupdto() {
		Mockito.when(snapshotService.getByIdAndStartup(any(String.class), any(StartupDTO.class))).thenReturn(null);
		try {
			mockMvc.perform(get("/snapshot-details/4").sessionAttr("currentUser", new UserDTO())
					.sessionAttr("currentStartup", new StartupDTO()))
					.andExpect(status().isFound())
					.andExpect(view().name("redirect:/403"))
					.andExpect(redirectedUrl("/403"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getSnapshotDetails_user_ok() {
		Mockito.when(snapshotService.getByIdAndStartup(any(String.class), any(StartupDTO.class))).thenReturn(new SnapshotDTO());
		try {
			LinkedList<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			mockMvc.perform(get("/snapshot-details/4").sessionAttr("currentUser", new UserDTO())
				.sessionAttr("currentStartup", new StartupDTO()).requestAttr("breadCrumbs", breadcrumbs))
					.andExpect(status().isOk())
					.andExpect(view().name("/snapshotdetails"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/snapshotdetails.jsp"))
					.andExpect(model().attributeExists("snapshot"))
					.andExpect(model().attribute("snapshot", instanceOf(SnapshotDTO.class)))
					.andExpect(model().attributeExists("breadcrumbs"))
					.andExpect(model().attribute("breadcrumbs", hasSize(0)))
					.andExpect(model().attribute("breadcrumbs", instanceOf(LinkedList.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSnapshotDetails_user_nullmappings() {
		Mockito.when(snapshotService.getByIdAndStartup(any(String.class), any(StartupDTO.class))).thenReturn(new SnapshotDTO());
		try {
			LinkedList<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
			breadcrumbs.add(new UrlLabelMapping("zdfbxdfb", "zfbzdfb", false));
			mockMvc.perform(get("/snapshot-details/4").sessionAttr("currentUser", new UserDTO())
				.sessionAttr("currentStartup", new StartupDTO()).requestAttr("breadCrumbs", breadcrumbs))
					.andExpect(status().isOk())
					.andExpect(view().name("/snapshotdetails"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/snapshotdetails.jsp"))
					.andExpect(model().attributeExists("snapshot"))
					.andExpect(model().attribute("snapshot", instanceOf(SnapshotDTO.class)))
					.andExpect(model().attributeExists("breadcrumbs"))
					.andExpect(model().attribute("breadcrumbs", hasSize(1)))
					.andExpect(model().attribute("breadcrumbs", instanceOf(LinkedList.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
