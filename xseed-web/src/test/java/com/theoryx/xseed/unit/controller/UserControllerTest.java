package com.theoryx.xseed.unit.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import org.testng.Assert;

import static org.mockito.Matchers.any;
import static org.hamcrest.Matchers.*;

import com.theoryx.xseed.controller.UserController;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.service.MailService;
import com.theoryx.xseed.service.MembershipService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserService;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

	private MockMvc mockMvc;
	
	@Mock
	private MessageSource messageSource;
	@Mock
	private UserService userService;
	@Mock
	private StartupService startupService;
	@Mock
	private MembershipService membershipService;
	@Mock
	private UserDetailsService userDetailsService;
	@Mock
	private MailService mailService;
	@Mock
	private Authentication auth;
	@InjectMocks
	private UserController userController;
	
	private List<UrlLabelMapping> breadcrumbs;
	
	@Before
	public void setUp(){
		// Process mock annotations
		MockitoAnnotations.initMocks(this);

		//ViewResolver
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp");
		viewResolver.setSuffix(".jsp");

		// Setup Spring test in standalone mode
		// Sets the vewresolver
		mockMvc = MockMvcBuilders.standaloneSetup(userController)
				.setViewResolvers(viewResolver).build();
		
		breadcrumbs = new LinkedList<UrlLabelMapping>();
		UrlLabelMapping home = new UrlLabelMapping("/home", "Home", true);
		breadcrumbs.add(home);
	}
	
	@Test
	public void testGetIndexPageWithNull(){
		Mockito.when(auth.getPrincipal()).thenReturn(null);
		try {
			mockMvc.perform(get("/"))
			.andExpect(status().isOk())
			.andExpect(view().name("/index"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/index.jsp"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetLoginPageWithNull(){
		Mockito.when(auth.getPrincipal()).thenReturn(null);
		try {
			mockMvc.perform(get("/login"))
			.andExpect(status().isOk())
			.andExpect(view().name("/login"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetRegisterPageWithNull(){
		Mockito.when(auth.getPrincipal()).thenReturn(null);
		try {
			mockMvc.perform(get("/register"))
			.andExpect(status().isOk())
			.andExpect(view().name("/register"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/register.jsp"))
			.andExpect(model().attributeExists("userdto"))
			.andExpect(model().attribute("userdto", isA(UserDTO.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetEditProfilePage(){
		Mockito.when(userService.getCurrentUserDTO()).thenReturn(new UserDTO());
		try {
			mockMvc.perform(get("/editprofile")
					.sessionAttr("currentUser", new UserDTO())
					.requestAttr("breadCrumbs", breadcrumbs))
			.andExpect(status().isOk())
			.andExpect(view().name("/editprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editprofile.jsp"))
			.andExpect(model().attributeExists("userdto"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attribute("userdto", isA(UserDTO.class)))
			.andExpect(model().attribute("breadcrumbs", isA(List.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetEditProfilePageWithNull(){
		Mockito.when(userService.getCurrentUserDTO()).thenReturn(new UserDTO());
		try {
			mockMvc.perform(get("/editprofile")
					.requestAttr("breadCrumbs", breadcrumbs))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRegisterWithErrors(){
		List<Error> errors = new LinkedList<Error>();
		errors.add(new Error());
		errors.add(new Error());
		Mockito.when(userService.validateRegisterInput(any(UserDTO.class))).thenReturn(errors);
		try {
		mockMvc.perform(post("/register")
				.param("name", "User1")
				.param("email", "user1@abv.bg")
				.param("startup.name", "Startup1"))
		.andExpect(status().isOk())
		.andExpect(view().name("/register"))
		.andExpect(forwardedUrl("/WEB-INF/jsp/register.jsp"))
		.andExpect(model().attributeExists("errors"))
		.andExpect(model().attributeExists("name"))
		.andExpect(model().attributeExists("email"))
		.andExpect(model().attributeExists("startup.name"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		errors.clear();
		User user = new User();
		Startup startup = new Startup();
		user.setStartups(new LinkedList<Startup>());
		user.getStartups().add(startup);
		Mockito.when(userService.validateRegisterInput(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.createUser(any(UserDTO.class))).thenReturn(user);
		Mockito.when(startupService.convertStartupToStartupDTO(startup)).thenReturn(new StartupDTO());
		Mockito.when(startupService.createStartup(any(StartupDTO.class))).thenReturn(new Startup());
		Mockito.when(membershipService.saveMembership(any(Membership.class))).thenReturn(null);
		try {
			mockMvc.perform(post("/register"))
			.andExpect(status().isOk())
			.andExpect(view().name("/register"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/register.jsp"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testRegisterWithoutErrorsNoNull(){
		List<Error> errors = new LinkedList<Error>();
		User user = new User();
		Startup startup = new Startup();
		user.setStartups(new LinkedList<Startup>());
		user.getStartups().add(startup);
		Mockito.when(userService.validateRegisterInput(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.createUser(any(UserDTO.class))).thenReturn(user);
		Mockito.when(startupService.createStartup(any(StartupDTO.class))).thenReturn(new Startup());
		Mockito.when(membershipService.saveMembership(any(Membership.class))).thenReturn(new Membership());
		
		Mockito.doNothing().when(userService).authenticateUserAndInitializeSessionByUsername(any(String.class), any(UserDetailsService.class));
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(new UserDTO());
		Mockito.when(startupService.convertStartupToStartupDTO(startup)).thenReturn(new StartupDTO());
		
		try {
			HttpSession session = mockMvc.perform(post("/register"))
			.andExpect(status().isFound())
			.andExpect(view().name("redirect:/home"))
			.andExpect(redirectedUrl("/home"))
			.andExpect(request().sessionAttribute("currentUser", isA(UserDTO.class)))
			.andExpect(request().sessionAttribute("currentStartup", isA(StartupDTO.class)))
			.andReturn()
			.getRequest()
			.getSession();
			
			Assert.assertNotNull(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRegisterWithoutErrorsOneIsNull(){
		List<Error> errors = new LinkedList<Error>();
		User user = new User();
		Startup startup = new Startup();
		user.setStartups(new LinkedList<Startup>());
		user.getStartups().add(startup);
		Mockito.when(userService.validateRegisterInput(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.createUser(any(UserDTO.class))).thenReturn(user);
		Mockito.when(startupService.createStartup(any(StartupDTO.class))).thenReturn(new Startup());
		Mockito.when(membershipService.saveMembership(any(Membership.class))).thenReturn(null);
		
		Mockito.doNothing().when(userService).authenticateUserAndInitializeSessionByUsername(any(String.class), any(UserDetailsService.class));
		Mockito.when(userService.convertUserToUserDTO(user)).thenReturn(new UserDTO());
		Mockito.when(startupService.convertStartupToStartupDTO(startup)).thenReturn(new StartupDTO());
		String error = messageSource.getMessage("error.registration", new Object[0], Locale.getDefault());
		try {
			HttpSession session = mockMvc.perform(post("/register"))
			.andExpect(status().isOk())
			.andExpect(view().name("/register"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/register.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attribute("errors", isA(List.class)))
			.andExpect(model().attribute("errors", hasSize(1)))
			.andExpect(model().attribute("errors", hasItem(hasProperty("message", equalTo(error)))))
			.andReturn()
			.getRequest()
			.getSession();
			
			Assert.assertNotNull(session);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEditProfileOnlyNameErrors(){
		List<Error> errors = new LinkedList<Error>();
		errors.add(new Error());
		Mockito.when(userService.validateEditProfileInput(any(UserDTO.class))).thenReturn(errors);
		try {
			mockMvc.perform(post("/editprofile")
					.requestAttr("breadCrumbs", new Object())
					.param("name", "User1")
					.param("password", "")
					.param("confirmationPassword", "")
					.param("email", "Email@abv.bg")
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
			.andExpect(status().isOk())
			.andExpect(view().name("/editprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editprofile.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attributeExists("name"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEditProfileOnlyNameNoErrorsNotNull(){
		List<Error> errors = new LinkedList<Error>();
		Mockito.when(userService.validateEditProfileInput(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.updateUserInfo(any(UserDTO.class), any(UserDTO.class), any(User.class))).thenReturn(new UserDTO());
		try {
			mockMvc.perform(post("/editprofile")
					.requestAttr("breadCrumbs", new Object())
					.param("name", "User1")
					.param("password", "")
					.param("confirmationPassword", "")
					.param("email", "Email@abv.bg")
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
			.andExpect(status().isFound())
			.andExpect(view().name("redirect:/editprofile"))
			.andExpect(redirectedUrl("/editprofile"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(request().sessionAttribute("currentUser", isA(UserDTO.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEditProfileNotOnlyNameNoErrorsNotNull(){
		List<Error> errors = new LinkedList<Error>();
		Mockito.when(userService.validateEditProfileInput(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.updateUserInfo(any(UserDTO.class), any(UserDTO.class), any(User.class))).thenReturn(new UserDTO());
		try {
			mockMvc.perform(post("/editprofile")
					.requestAttr("breadCrumbs", new Object())
					.param("name", "User1")
					.param("password", "123123")
					.param("confirmationPassword", "")
					.param("email", "Email@abv.bg")
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
			.andExpect(status().isFound())
			.andExpect(view().name("redirect:/login"))
			.andExpect(redirectedUrl("/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEditProfileNotOnlyNameNoErrorsNull(){
		List<Error> errors = new LinkedList<Error>();
		Mockito.when(userService.validateEditProfileInput(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.updateUserInfo(any(UserDTO.class), any(UserDTO.class), any(User.class))).thenReturn(null);
		String error = messageSource.getMessage("error.profile-edit", new Object[0], Locale.getDefault());
		try {
			mockMvc.perform(post("/editprofile")
					.requestAttr("breadCrumbs", new Object())
					.param("name", "User1")
					.param("password", "")
					.param("confirmationPassword", "123123")
					.param("email", "Email@abv.bg")
					.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE))
			.andExpect(status().isOk())
			.andExpect(view().name("/editprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editprofile.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attribute("errors", hasSize(1)))
			.andExpect(model().attribute("errors", hasItem(hasProperty("message", equalTo(error)))));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetHomePageNoUser(){
		UserDTO user = new UserDTO();
		StartupDTO startup = new StartupDTO();
		user.setStartup(startup);
		Mockito.when(userService.getCurrentUserDTO()).thenReturn(user);
		try {
			HttpSession session = mockMvc.perform(get("/home"))
			.andExpect(status().isOk())
			.andExpect(view().name("/home"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/home.jsp"))
			.andReturn().getRequest().getSession();
			
			Assert.assertNotNull(session.getAttribute("currentUser"));
			Assert.assertNotNull(session.getAttribute("currentStartup"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetHomePageUser(){
		UserDTO user = new UserDTO();
		StartupDTO startup = new StartupDTO();
		user.setStartup(startup);
		Mockito.when(userService.getCurrentUserDTO()).thenReturn(user);
		try {
			mockMvc.perform(get("/home").sessionAttr("currentUser", user))
			.andExpect(status().isOk())
			.andExpect(view().name("/home"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/home.jsp"))
			.andReturn().getRequest().getSession();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetForbiddenPage(){
		try {
			mockMvc.perform(get("/403"))
			.andExpect(status().isOk())
			.andExpect(view().name("/forbidden"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/forbidden.jsp"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testResetPassword(){
		try {
			mockMvc.perform(get("/reset-password"))
			.andExpect(status().isOk())
			.andExpect(view().name("/reset-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/reset-password.jsp"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSendResetPasswordEmail(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		List<Error> errors = new LinkedList<Error>();
		result.put(user, errors);
		Mockito.when(messageSource.getMessage("success.email", new Object[0], Locale.getDefault())).thenReturn("Text");
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		Mockito.when(userService.generatePasswordToken(user)).thenReturn("123");
		Mockito.when(mailService
				.sendChangePasswordLink(any(String.class), any(String.class), any(String.class), any(String.class)))
				.thenReturn(true);
		try {
			mockMvc.perform(post("/reset-password")
					.param("email", "test1@abv.bg"))
			.andExpect(status().isOk())
			.andExpect(view().name("/reset-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/reset-password.jsp"))
			.andDo(print())
			.andExpect(model().attributeExists("success"))
			.andExpect(model().attribute("success", equalTo("Text")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSendResetPasswordEmailWithErrors(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		List<Error> errors = new LinkedList<Error>();
		result.put(null, errors);
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		try {
			mockMvc.perform(post("/reset-password")
					.param("email", "test1@abv.bg"))
					.andExpect(status().isOk())
					.andExpect(view().name("/reset-password"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/reset-password.jsp"))
					.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSendResetPasswordEmailWithMailError(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		List<Error> errors = new LinkedList<Error>();
		result.put(user, errors);
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		Mockito.when(userService.generatePasswordToken(user)).thenReturn("123");
		Mockito.when(mailService
				.sendChangePasswordLink("xseed.theoryx@gmail.com", "test1@abv.bg", "Reset password", "123"))
				.thenReturn(false);
		try {
			mockMvc.perform(post("/reset-password")
					.param("email", "test1@abv.bg"))
			.andExpect(status().isOk())
			.andExpect(view().name("/reset-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/reset-password.jsp"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateNewPassword(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		List<Error> errors = new LinkedList<Error>();
		result.put(user, errors);
		
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		Mockito.when(userService.generatePasswordToken(user)).thenReturn("123");
		
		try {
			mockMvc.perform(get("/forgottenpassword")
					.param("token", "123")
					.param("email", "test1@abv.bg"))
			.andExpect(status().isOk())
			.andExpect(view().name("/forgotten-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/forgotten-password.jsp"))
			.andExpect(model().attributeExists("email"))
			.andExpect(model().attributeExists("token"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateNewPasswordWithError(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		List<Error> errors = new LinkedList<Error>();
		result.put(null, errors);
		
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		
		try {
			mockMvc.perform(get("/forgottenpassword")
					.param("token", "123")
					.param("email", "test1@abv.bg"))
			.andExpect(status().isOk())
			.andExpect(view().name("/forgotten-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/forgotten-password.jsp"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testCreateNewPasswordWithInvalidTokenError(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		List<Error> errors = new LinkedList<Error>();
		result.put(user, errors);
		
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		Mockito.when(userService.generatePasswordToken(user)).thenReturn("1234");
		
		try {
			mockMvc.perform(get("/forgottenpassword")
					.param("token", "123")
					.param("email", "test1@abv.bg"))
			.andExpect(status().isOk())
			.andExpect(view().name("/forgotten-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/forgotten-password.jsp"))
			.andExpect(model().attributeExists("email"))
			.andExpect(model().attributeExists("token"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateNewPasswordPost(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		List<Error> errors = new LinkedList<Error>();
		result.put(user, errors);
		
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		Mockito.when(userService.updateUser(user)).thenReturn(true);
		
		try {
			mockMvc.perform(post("/forgottenpassword")
					.param("email", "test1@abv.bg")
					.param("password", "1234")
					.param("confirmationPassword","1234")
					.param("token", "123"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testCreateNewPasswordPostWithValidationErrors(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		List<Error> errors = new LinkedList<Error>();
		result.put(null, errors);
		
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		
		try {
			mockMvc.perform(post("/forgottenpassword")
					.param("email", "test1@abv.bg")
					.param("password", "1234")
					.param("confirmationPassword","1234")
					.param("token", "123"))
			.andExpect(status().isOk())
			.andExpect(view().name("/forgotten-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/forgotten-password.jsp"))
			.andExpect(model().attributeExists("email"))
			.andExpect(model().attributeExists("token"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateNewPassowrdPostWithUpdateError(){
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		List<Error> errors = new LinkedList<Error>();
		result.put(user, errors);
		
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		Mockito.when(userService.updateUser(user)).thenReturn(false);
		
		try {
			mockMvc.perform(post("/forgottenpassword")
					.param("email", "test1@abv.bg")
					.param("password", "1234")
					.param("confirmationPassword","1234")
					.param("token", "123"))
			.andExpect(status().isOk())
			.andExpect(view().name("/forgotten-password"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/forgotten-password.jsp"))
			.andExpect(model().attributeExists("email"))
			.andExpect(model().attributeExists("token"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetLogoutPage(){
		try {
			mockMvc.perform(get("/logout"))
			.andExpect(status().isFound())
			.andExpect(view().name("redirect:/index"))
			.andExpect(redirectedUrl("/index"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUpdateNewUserInfo(){
		List<Error> errors = new LinkedList<Error>();
		Mockito.when(userService.validateNewUserProfile(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.updateUser(any(UserDTO.class))).thenReturn(true);
		
		try {
			mockMvc.perform(post("/updatenewuser"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testUpdateNewUserInfoWithValidationErrors(){
		List<Error> errors = new LinkedList<Error>();
		errors.add(new Error());
		
		Mockito.when(userService.validateNewUserProfile(any(UserDTO.class))).thenReturn(errors);
		
		try {
			mockMvc.perform(post("/updatenewuser")
					.param("email", "test1@abv.bg"))
			.andExpect(status().isOk())
			.andExpect(view().name("/editnewuserprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editnewuserprofile.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attributeExists("userdto"))
			.andExpect(model().attributeExists("email"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testUpdateNewUserInfoWithUpdateErrors(){
		List<Error> errors = new LinkedList<Error>();
		Mockito.when(userService.validateNewUserProfile(any(UserDTO.class))).thenReturn(errors);
		Mockito.when(userService.updateUser(any(UserDTO.class))).thenReturn(false);
		
		try {
			mockMvc.perform(post("/updatenewuser"))
			.andExpect(status().isOk())
			.andExpect(view().name("/editnewuserprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editnewuserprofile.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attributeExists("userdto"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetInvitationPageNoUser(){
		Mockito.when(userService.generateProtectionToken("123123")).thenReturn("protection");
		try {
			mockMvc.perform(get("/invitation").param("email", "123123").param("token", "123456"))
			.andExpect(status().isFound())
			.andExpect(view().name("redirect:/activation?token=123456&email=123123&referer=protection"))
			.andExpect(redirectedUrl("/activation?token=123456&email=123123&referer=protection"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetInvitationPageUser(){
		Mockito.when(userService.generateProtectionToken("123123")).thenReturn("protection");
		try {
			mockMvc.perform(get("/invitation").param("email", "123123").param("token", "123456").sessionAttr("currentUser", new UserDTO()))
			.andExpect(status().isOk())
			.andExpect(view().name("/invitation"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/invitation.jsp"))
			.andExpect(model().attributeExists("token"))
			.andExpect(model().attribute("token", equalTo("123456")))
			.andExpect(model().attributeExists("email"))
			.andExpect(model().attribute("email", equalTo("123123")))
			.andExpect(model().attributeExists("referer"))
			.andExpect(model().attribute("referer", equalTo("protection")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetActivationPageDiffReferer(){
		Mockito.when(userService.generateProtectionToken("test1@abv.bg")).thenReturn("protection");
		try {
			mockMvc.perform(get("/activation")
					.param("email", "test1@abv.bg")
					.param("token", "123456")
					.param("referer", "protectionno"))
			.andExpect(status().isFound())
			.andExpect(view().name("redirect:/login"))
			.andExpect(redirectedUrl("/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetActivationPageSameRefererNoUserNullUserDto(){
		Mockito.when(userService.generateProtectionToken("test1@abv.bg")).thenReturn("protection");
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(null, new ArrayList<Error>());
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		try {
			mockMvc.perform(get("/activation")
					.param("email", "test1@abv.bg")
					.param("token", "123456")
					.param("referer", "protection"))
			.andExpect(status().isOk())
			.andExpect(view().name("/editnewuserprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editnewuserprofile.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attribute("errors", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetActivationPageSameRefererUserNullUserDto(){
		Mockito.when(userService.generateProtectionToken("test1@abv.bg")).thenReturn("protection");
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		result.put(null, new ArrayList<Error>());
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		try {
			mockMvc.perform(get("/activation")
					.param("email", "test1@abv.bg")
					.param("token", "123456")
					.param("referer", "protection")
					.sessionAttr("currentUser", new UserDTO()))
			.andExpect(status().isOk())
			.andExpect(view().name("/editnewuserprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editnewuserprofile.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attribute("errors", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetActivationPageSameRefererUserNotNullUserDtoactive(){
		Mockito.when(userService.generateProtectionToken("test1@abv.bg")).thenReturn("protection");
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		user.setActive(true);
		result.put(user, new ArrayList<Error>());
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		String error = messageSource.getMessage("error.account-already-activated", new Object[0],
				Locale.getDefault());
		try {
			mockMvc.perform(get("/activation")
					.param("email", "test1@abv.bg")
					.param("token", "123456")
					.param("referer", "protection")
					.sessionAttr("currentUser", new UserDTO())
					.sessionAttr("org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN", new DefaultCsrfToken("d", "d", "d")))
			.andExpect(status().isOk())
			.andExpect(view().name("/login"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attribute("errors", hasSize(1)))
			.andExpect(model().attribute("errors", hasItem(hasProperty("message", equalTo(error)))));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetActivationPageSameRefererNoUserNotNullUserDtoinactiveSameToken(){
		Mockito.when(userService.generateProtectionToken("test1@abv.bg")).thenReturn("protection");
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("123456");
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		user.setActive(false);
		result.put(user, new ArrayList<Error>());
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		try {
			mockMvc.perform(get("/activation")
					.param("email", "test1@abv.bg")
					.param("token", "123456")
					.param("referer", "protection"))
			.andExpect(status().isOk())
			.andExpect(view().name("/editnewuserprofile"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editnewuserprofile.jsp"))
			.andExpect(model().attributeExists("email"))
			.andExpect(model().attribute("email", equalTo("test1@abv.bg")))
			.andExpect(model().attributeExists("userdto"))
			.andExpect(model().attribute("userdto", isA(UserDTO.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetActivationPageSameRefererNoUserNotNullUserDtoinactiveDiffToken(){
		Mockito.when(userService.generateProtectionToken("test1@abv.bg")).thenReturn("protection");
		Mockito.when(userService.generateActivationToken(any(UserDTO.class))).thenReturn("452361");
		Map<UserDTO, List<Error>> result = new HashMap<UserDTO, List<Error>>();
		UserDTO user = new UserDTO();
		user.setActive(false);
		StartupDTO startup = new StartupDTO();
		startup.setName("efer");
		user.setStartup(startup);
		result.put(user, new ArrayList<Error>());
		Mockito.when(userService.getUserByEmail("test1@abv.bg")).thenReturn(result);
		Mockito.when(mailService.sendInvitationLinkForStartup
				(any(String.class), any(String.class), any(String.class), any(String.class), any(String.class))).thenReturn(true);
		String error = messageSource.getMessage("error.new-activation-link", new Object[0], Locale.getDefault());
		try {
			mockMvc.perform(get("/activation")
					.param("email", "test1@abv.bg")
					.param("token", "123456")
					.param("referer", "protection"))
			.andExpect(status().isOk())
			.andExpect(view().name("/login"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
			.andExpect(model().attributeExists("errors"))
			.andExpect(model().attribute("errors", hasItem(hasProperty("message", equalTo(error)))));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
