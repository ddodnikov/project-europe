package com.theoryx.xseed.unit.controller;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.theoryx.xseed.controller.StartupController;
import com.theoryx.xseed.dto.CountryDTO;
import com.theoryx.xseed.dto.MembershipDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Error;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.service.CountryService;
import com.theoryx.xseed.service.MailService;
import com.theoryx.xseed.service.MembershipService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class StartupControllerTest {

	private MockMvc mockMvc;

	@Mock
	private MessageSource messageSource;
	@Mock
	private MailService mailService;
	@Mock
	private StartupService startupService;
	@Mock
	private CountryService countryService;
	@Mock
	private UserService userService;
	@Mock
	private MembershipService membershipService;
	
	@InjectMocks
	private StartupController startupController;
	
	
	private static final String EMAIL = "ivanchooo@abv.bg";
	private static final String INVALID_EMAIL = "";
	@Before
	public void setUp() {	
		
		// Process mock annotations
		MockitoAnnotations.initMocks(this);
		
		ResourceBundleMessageSource messageSource2 = new ResourceBundleMessageSource();
		messageSource2.setBasename("locale/language");

		//ViewResolver
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/jsp");
		viewResolver.setSuffix(".jsp");

		// Setup Spring test in standalone mode
		// Sets the vewresolver
		mockMvc = MockMvcBuilders.standaloneSetup(startupController).setViewResolvers(viewResolver).build();

		messageSource = messageSource2;
		startupController.setMessageSource(messageSource);
	}
	
	@Test
	public void test_getEditStartupPage_nullcountry() {
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUserDTO = new UserDTO();
		MembershipDTO mem = new MembershipDTO();
		mem.setRole(UserRole.ADMIN);
		currentUserDTO.setMembership(mem);
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		Mockito.when(countryService.getAllContryDTOs()).thenReturn(new ArrayList<CountryDTO>());
		try {
			mockMvc.perform(get("/editstartup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUserDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/editstartup"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("countries"))
				.andExpect(model().attribute("countries", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getEditStartupPage() {
		StartupDTO startupDTO = new StartupDTO();
		startupDTO.setCountry(new CountryDTO());
		
		UserDTO currentUserDTO = new UserDTO();
		MembershipDTO mem = new MembershipDTO();
		mem.setRole(UserRole.ADMIN);
		currentUserDTO.setMembership(mem);
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		Mockito.when(countryService.getAllContryDTOs()).thenReturn(new ArrayList<CountryDTO>());
		try {
			mockMvc.perform(get("/editstartup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUserDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/editstartup"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("countries"))
				.andExpect(model().attribute("countries", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addUserToStartupGetWithAdminRole() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		try {
			mockMvc.perform(get("/add-user-to-startup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/adduser"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addUserToStartupGetWithUserRole(){
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		membershipDTO.setRole(UserRole.USER);
		currentUser.setMembership(membershipDTO);
		try {
			mockMvc.perform(get("/add-user-to-startup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_showStartupUsersGetWithAdminRole() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		try {
			mockMvc.perform(get("/showusers")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/startupusers"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/startupusers.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void set_showStartupUsersWithUserRole(){
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		membershipDTO.setRole(UserRole.USER);
		currentUser.setMembership(membershipDTO);
		try {
			mockMvc.perform(get("/showusers")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void test_editStartup_errors() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		Map<StartupDTO, List<Error>> result = new HashMap<StartupDTO, List<Error>>();
		result.put(null, new ArrayList<Error>());
		Mockito.when(startupService.normalizeStartup(any(StartupDTO.class))).thenReturn(result);
		
		Mockito.when(countryService.getAllContryDTOs()).thenReturn(new ArrayList<CountryDTO>());
		try {
			mockMvc.perform(post("/editstartup")
					.requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/editstartup"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(0)))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("countries"))
				.andExpect(model().attribute("countries", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editStartup_noerrors_empty_null() {
		StartupDTO startupDTO = new StartupDTO();
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		Map<StartupDTO, List<Error>> result = new HashMap<StartupDTO, List<Error>>();
		result.put(new StartupDTO(), new ArrayList<Error>());
		Map<StartupDTO, List<Error>> updateResult = new HashMap<StartupDTO, List<Error>>();
		updateResult.put(null, new ArrayList<Error>());
		
		Mockito.when(startupService.normalizeStartup(any(StartupDTO.class))).thenReturn(result);
		Mockito.when(startupService.validateStartupInfo(any(StartupDTO.class))).thenReturn(new ArrayList<Error>());
		Mockito.when(startupService.updateStartup(any(StartupDTO.class))).thenReturn(updateResult);
		Mockito.when(countryService.getAllContryDTOs()).thenReturn(new ArrayList<CountryDTO>());
		try {
			mockMvc.perform(post("/editstartup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/editstartup"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(0)))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("countries"))
				.andExpect(model().attribute("countries", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editStartup_noerrors_empty_notnull() {
		StartupDTO startupDTO = new StartupDTO();
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		Map<StartupDTO, List<Error>> result = new HashMap<StartupDTO, List<Error>>();
		result.put(new StartupDTO(), new ArrayList<Error>());
		Map<StartupDTO, List<Error>> updateResult = new HashMap<StartupDTO, List<Error>>();
		updateResult.put(new StartupDTO(), new ArrayList<Error>());
		
		Mockito.when(startupService.normalizeStartup(any(StartupDTO.class))).thenReturn(result);
		Mockito.when(startupService.validateStartupInfo(any(StartupDTO.class))).thenReturn(new ArrayList<Error>());
		Mockito.when(startupService.updateStartup(any(StartupDTO.class))).thenReturn(updateResult);
		Mockito.when(countryService.getAllContryDTOs()).thenReturn(new ArrayList<CountryDTO>());
		try {
			mockMvc.perform(post("/editstartup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/editstartup"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
				.andExpect(request().sessionAttribute("currentStartup", notNullValue()))
				.andExpect(request().sessionAttribute("currentStartup", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("countries"))
				.andExpect(model().attribute("countries", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editStartup_noerrors_notempty() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		Map<StartupDTO, List<Error>> result = new HashMap<StartupDTO, List<Error>>();
		result.put(new StartupDTO(), new ArrayList<Error>());
		List<Error> errors = new ArrayList<Error>();
		errors.add(new Error());
		
		Mockito.when(startupService.normalizeStartup(any(StartupDTO.class))).thenReturn(result);
		Mockito.when(startupService.validateStartupInfo(any(StartupDTO.class))).thenReturn(errors);
		Mockito.when(countryService.getAllContryDTOs()).thenReturn(new ArrayList<CountryDTO>());
		try {
			mockMvc.perform(post("/editstartup")
					.requestAttr("breadCrumbs", breadcrumbs))
				.andExpect(status().isOk())
				.andExpect(view().name("/editstartup"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("countries"))
				.andExpect(model().attribute("countries", hasSize(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addUserToStartupWithUserRole(){
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		membershipDTO.setRole(UserRole.USER);
		currentUser.setMembership(membershipDTO);
		
		try {
			mockMvc.perform(post("/add-user-to-startup")
					.param("email", ".9*/@abv..bg")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/403"));
			} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void test_addUserToStartup_invalidemail() {
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		Error error = new Error(messageSource.getMessage("error.invalid-email", new Object[0], Locale.getDefault()));

		try {
			mockMvc.perform(post("/add-user-to-startup")
					.param("email", ".9*/@abv..bg")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/adduser"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", contains(error)))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(0)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void addUserToStartupTestExistingActiveUser() {
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO existingUser = new UserDTO();
		
		existingUser.setActive(true);
		membershipDTO.setRole(UserRole.ADMIN);
		
		startupDTO.setName("TestStartup");
		startupDTO.setUsers(new LinkedList<UserDTO>());
		
		currentUser.setMembership(membershipDTO);
		currentUser.setName("TestUser");
		currentUser.setStartup(startupDTO);
		List<Error> errors = new LinkedList<Error>();
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		Map<UserDTO, List<Error>> userResult = new HashMap<UserDTO, List<Error>>();
		userResult.put(existingUser, null);
		
		Mockito.when(userService.ifExist(EMAIL)).thenReturn(true);
		Mockito.when(userService.getUserByEmail(EMAIL)).thenReturn(userResult);
		Mockito.doNothing().when(startupService).addExistingActive(startupDTO, currentUser, existingUser, errors, EMAIL);		
		
		try {
			mockMvc.perform(post("/add-user-to-startup")
					.param("email", EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/adduser"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void addUserToStartupTestExistingInactive(){
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO existingUser = new UserDTO();
		
		existingUser.setActive(false);
		membershipDTO.setRole(UserRole.ADMIN);
		
		startupDTO.setName("TestStartup");
		startupDTO.setUsers(new LinkedList<UserDTO>());
		
		currentUser.setMembership(membershipDTO);
		currentUser.setName("TestUser");
		currentUser.setStartup(startupDTO);
		List<Error> errors = new LinkedList<Error>();
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		Map<UserDTO, List<Error>> userResult = new HashMap<UserDTO, List<Error>>();
		userResult.put(existingUser, null);
		
		Mockito.when(userService.ifExist(EMAIL)).thenReturn(true);
		Mockito.when(userService.getUserByEmail(EMAIL)).thenReturn(userResult);
		Mockito.doNothing().when(startupService).addExistingInactive(startupDTO, currentUser, existingUser, errors, EMAIL);		
		
		try {
			mockMvc.perform(post("/add-user-to-startup")
					.param("email", EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/adduser"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void addUserToStartupTestNonExisiting(){
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUser = new UserDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO existingUser = new UserDTO();
		
		existingUser.setActive(false);
		membershipDTO.setRole(UserRole.ADMIN);
		
		startupDTO.setName("TestStartup");
		startupDTO.setUsers(new LinkedList<UserDTO>());
		
		currentUser.setMembership(membershipDTO);
		currentUser.setName("TestUser");
		currentUser.setStartup(startupDTO);
		List<Error> errors = new LinkedList<Error>();
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		Map<UserDTO, List<Error>> userResult = new HashMap<UserDTO, List<Error>>();
		userResult.put(existingUser, null);
		
		Mockito.when(userService.ifExist(EMAIL)).thenReturn(false);
		Mockito.when(userService.getUserByEmail(EMAIL)).thenReturn(userResult);
		Mockito.when(startupService.addNonExistigUser(startupDTO, currentUser, errors, EMAIL)).thenReturn(true);		
		
		
		try {
			mockMvc.perform(post("/add-user-to-startup")
					.param("email", EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/adduser"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeExists("success"));
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}
	
	@Test
	public void addUserToStartupTestWithUserRole(){
		StartupDTO startupDTO = new StartupDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		membershipDTO.setRole(UserRole.USER);
		currentUser.setMembership(membershipDTO);
		
		try {
			mockMvc.perform(post("/add-user-to-startup")
					.param("email", INVALID_EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/403"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddUserToStartupWithInvalidEmail(){
		StartupDTO startupDTO = new StartupDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		
		try {
			mockMvc.perform(post("/add-user-to-startup")
					.param("email", INVALID_EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentStartup", startupDTO)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/adduser"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupGETWithAdminRole(){
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		
		try {
			mockMvc.perform(get("/remove-user-from-startup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/removeusers"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/removeusers.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupGETWithUserRole(){
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		membershipDTO.setRole(UserRole.USER);
		currentUser.setMembership(membershipDTO);
		
		try {
			mockMvc.perform(get("/remove-user-from-startup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupWithNullEmails(){
		UserDTO currentUser = new UserDTO();
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		try {
			mockMvc.perform(post("/remove-user-from-startup")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser))
				.andExpect(status().isOk())
				.andExpect(view().name("/removeusers"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupWithInvalidEmail(){
		StartupDTO startupDTO = new StartupDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		
		try {
			mockMvc.perform(post("/remove-user-from-startup")
					.param("removed_emails", "")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/removeusers"))
				.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupWithNotContaingUser(){
		StartupDTO startupDTO = new StartupDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		UserDTO existingUser = new UserDTO();
		
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		startupDTO.setUsers(new LinkedList<UserDTO>());
		
		Mockito.when(userService.getByEmail(EMAIL)).thenReturn(new User());
		Mockito.when(userService.convertUserToLightUserDTO(any(User.class))).thenReturn(existingUser);
		
		try {
			mockMvc.perform(post("/remove-user-from-startup")
					.param("removed_emails", EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/removeusers"))
				.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupWithInvalidMembership(){
		StartupDTO startupDTO = new StartupDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		UserDTO existingUser = new UserDTO();
		
		membershipDTO.setRole(UserRole.ADMIN);
		currentUser.setMembership(membershipDTO);
		startupDTO.setUsers(new LinkedList<UserDTO>());
		startupDTO.getUsers().add(existingUser);
		
		Mockito.when(userService.getByEmail(EMAIL)).thenReturn(new User());
		Mockito.when(userService.convertUserToLightUserDTO(any(User.class))).thenReturn(existingUser);
		Mockito.when(membershipService.getMembershipByUserAndStartup(any(UserDTO.class), any(StartupDTO.class))).thenReturn(null);
		try {
			mockMvc.perform(post("/remove-user-from-startup")
					.param("removed_emails", EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/removeusers"))
				.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Test
	public void testRemoveUserFromStartupDeleteMembership(){
		StartupDTO startupDTO = new StartupDTO();
		MembershipDTO membershipDTO = new MembershipDTO();
		MembershipDTO deleteMembershipDTO = new MembershipDTO();
		UserDTO currentUser = new UserDTO();			
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		UserDTO existingUser = new UserDTO();
		
		membershipDTO.setRole(UserRole.ADMIN);
		deleteMembershipDTO.setRole(UserRole.USER);
		currentUser.setMembership(membershipDTO);
		startupDTO.setUsers(new LinkedList<UserDTO>());
		startupDTO.getUsers().add(existingUser);
		
		
		Mockito.when(userService.getByEmail(EMAIL)).thenReturn(new User());
		Mockito.when(userService.convertUserToLightUserDTO(any(User.class))).thenReturn(existingUser);
		Mockito.when(membershipService.getMembershipByUserAndStartup(any(UserDTO.class), any(StartupDTO.class))).thenReturn(deleteMembershipDTO);
		Mockito.doNothing().when(membershipService).deleteMembership(any(MembershipDTO.class));
		Mockito.when(membershipService.checkNoMembership(any(UserDTO.class))).thenReturn(false);
		
		try {
			mockMvc.perform(post("/remove-user-from-startup")
					.param("removed_emails", EMAIL)
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUser)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/removeusers"));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Test
	public void showUsersInStartupTest(){
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		StartupDTO startupDTO = new StartupDTO();
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setEmail(EMAIL);
		User currentUser = new User();
		List<Startup> userStartups = new LinkedList<Startup>();
		currentUser.setStartups(userStartups);
		
		List<StartupDTO> startups = new LinkedList<StartupDTO>();
		StartupDTO startupDTO1 = new StartupDTO();
		startupDTO1.setId(1);
		
		List<Membership> memberships = new LinkedList<Membership>();
		Membership mem = new Membership();
		mem.setDefault(true);
		
		Startup s = new Startup();
		s.setId(1);
		mem.setStartup(s);
		mem.setUser(currentUser);
		memberships.add(mem);
		currentUser.setMemberships(memberships);
		StartupDTO sDTO = new StartupDTO();
		sDTO.setId(1);
		
		startups.add(sDTO);

		Mockito.when(userService.getByEmail(EMAIL)).thenReturn(currentUser);
		Mockito.when(startupService.convert(userStartups)).thenReturn(startups);
		try {
			mockMvc.perform(get("/startups")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", startupDTO))
				.andExpect(status().isOk())
				.andExpect(view().name("/startups"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/startups.jsp"))
				.andExpect(model().attributeExists("defaultStartup"))
				.andExpect(model().attributeExists("userStartups"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Test
	public void setDafaultAndCurrentStartupTest(){
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		StartupDTO currentStartupDTO = new StartupDTO();
		currentStartupDTO.setId(1);
		
		MembershipDTO membershipDTO = new MembershipDTO();
		
		UserDTO currentUserDTO = new UserDTO();
		currentUserDTO.setId(1);
		currentUserDTO.setEmail(EMAIL);		
		currentUserDTO.setMembership(membershipDTO);
		
		Membership mem = new Membership();
		mem.setRole(UserRole.ADMIN);
		
		Startup newCurrentStartup = new Startup();
		newCurrentStartup.setId(2);
		
		User currentUser = new User();
		currentUser.setMemberships(new LinkedList<Membership>());
		
		Mockito.when(startupService.findById(2)).thenReturn(newCurrentStartup);
		Mockito.when(startupService.convertStartupToStartupDTO(any(Startup.class))).thenReturn(new StartupDTO());
		Mockito.when(membershipService.getMembershipByUserIdAndStartupId(1,2)).thenReturn(mem);
		Mockito.when( userService.getByEmail(EMAIL)).thenReturn(currentUser);
		try {
			mockMvc.perform(post("/setstartups")
					.param("current", "2")
					.param("default", "2")
					.requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", currentStartupDTO))
				.andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/startups"));
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
