package com.theoryx.xseed.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.sql.DataSource;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.Assert;

import com.theoryx.xseed.XseedWebApplication;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.service.QuestionService;
import com.theoryx.xseed.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = XseedWebApplication.class)
@WebAppConfiguration
@ActiveProfiles("integration_test_h2")
public class AdminControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private UserService userService;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private DataSource dataSource;

	private static Connection connection;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() {
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_addAddmin() {
		String success = messageSource.getMessage("success.email", new Object[0], Locale.getDefault());
		try {
			MockHttpServletRequestBuilder request = post("/addadmin").param("email", "admin2@gmail.com");

			mockMvc.perform(request).andExpect(status().isOk()).andExpect(view().name("/addadmin"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/addadmin.jsp")).andExpect(model().attributeExists("errors"))
					.andExpect(model().attributeExists("breadcrumbs")).andExpect(model().attributeExists("success"))
					.andExpect(model().attribute("breadcrumbs", isA(List.class)))
					.andExpect(model().attribute("success", hasToString(success)))
					.andExpect(model().attribute("errors", isA(List.class)))
					.andExpect(model().attribute("breadcrumbs", hasSize(greaterThanOrEqualTo(1))));

			User user = userService.getByEmail("admin2@gmail.com");
			Assert.assertTrue(user != null);
			userService.deleteUserById(user.getId());
			user = userService.getByEmail("admin2@gmail.com");
			Assert.assertTrue(user == null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetAddingAddminsPage(){
		try {
			mockMvc.perform(get("/addadmin"))
			.andExpect(status().isOk())
			.andExpect(view().name("/addadmin"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/addadmin.jsp"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createAdmin_correcttoken() {
		UserDTO newUser = new UserDTO("Test new admin name", "kojihu@abv.bg", "123456", "123456", false,
				UserRole.ADMIN);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);

		String email = user.getEmail();
		String token = userService.generateActivationToken(newUser);
		try {
			MockHttpServletRequestBuilder request = get("/createadmin").param("token", token).param("email", email);

			mockMvc.perform(request)
					.andExpect(status().isOk())
					.andExpect(view().name("/createadmin"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/createadmin.jsp"))
					.andExpect(model().attributeExists("email"))
					.andExpect(model().attributeExists("userdto"))
					.andExpect(model().attributeDoesNotExist("errors"))
					.andExpect(model().attribute("userdto", isA(UserDTO.class)))
					.andExpect(model().attribute("email", equalTo(email)));

			user = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(user != null);
			userService.deleteUserById(user.getId());
			user = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(user == null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_createAdmin_incorrecttoken() {
		UserDTO newUser = new UserDTO("Test new admin name", "kojihu@abv.bg", "123456", "123456", false,
				UserRole.ADMIN);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);

		String email = user.getEmail();
		String token = "efascuasdyu";
		try {
			MockHttpServletRequestBuilder request = get("/createadmin").param("token", token).param("email", email);

			mockMvc.perform(request)
					.andExpect(status().isOk())
					.andExpect(view().name("/login"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
					.andExpect(model().attributeExists("errors"))
					.andExpect(model().attributeDoesNotExist("userdto"))
					.andExpect(model().attributeDoesNotExist("email"))
					.andExpect(model().attribute("errors", isA(List.class)))
					.andExpect(model().attribute("errors", hasSize(1)));

			user = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(user != null);
			userService.deleteUserById(user.getId());
			user = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(user == null);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_saveAlgoQuestions() {
		List<Question> algoQuestions = questionService.findAll();
		Collections.sort(algoQuestions, new Comparator<Question>() {
			@Override
			public int compare(Question o1, Question o2) {
				return o1.getId() - o2.getId();
			}
		});
		try {
			MockHttpServletRequestBuilder request = post("/algoquestions");
			for (int i = 0; i < algoQuestions.size(); i++) {
				if (i % 3 == 0) {
					request.param(algoQuestions.get(i).getId().toString(), "notnull");
				}
			}

			mockMvc.perform(request)
					.andExpect(status().isFound())
					.andExpect(view().name("redirect:/algoquestions"))
					.andExpect(redirectedUrl("/algoquestions"));

			List<Question> algoQuestions2 = questionService.findAll();
			Collections.sort(algoQuestions2, new Comparator<Question>() {
				@Override
				public int compare(Question o1, Question o2) {
					return o1.getId() - o2.getId();
				}
			});
			for (int i = 0; i < algoQuestions2.size(); i++) {
				if (i % 3 == 0) {
					Assert.assertTrue(algoQuestions2.get(i).isAlgo() == true);
				} else {
					Assert.assertTrue(algoQuestions2.get(i).isAlgo() == false);
				}
			}
			for (Question question : algoQuestions) {
				questionService.save(question);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getAlgoQuestions() {
		Integer algoQuestions = questionService.findAll().size();
		try {
			MockHttpServletRequestBuilder request = get("/algoquestions");

			mockMvc.perform(request)
					.andExpect(status().isOk())
					.andExpect(view().name("/algoquestions"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/algoquestions.jsp"))
					.andExpect(model().attributeExists("breadcrumbs"))
					.andExpect(model().attributeExists("algoQuestions"))
					.andExpect(model().attribute("breadcrumbs", isA(List.class)))
					.andExpect(model().attribute("breadcrumbs", hasSize(greaterThan(1))))
					.andExpect(model().attribute("algoQuestions", isA(List.class)))
					.andExpect(model().attribute("algoQuestions", hasSize(algoQuestions)));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_showAdmins() {
		Integer admins = userService.getAllAdmins().size();
		try {
			MockHttpServletRequestBuilder request = get("/admins");

			mockMvc.perform(request)
					.andExpect(status().isOk())
					.andExpect(view().name("/admins"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/admins.jsp"))
					.andExpect(model().attributeExists("breadcrumbs"))
					.andExpect(model().attributeExists("admins"))
					.andExpect(model().attribute("breadcrumbs", isA(List.class)))
					.andExpect(model().attribute("breadcrumbs", hasSize(greaterThan(1))))
					.andExpect(model().attribute("admins", isA(List.class)))
					.andExpect(model().attribute("admins", hasSize(admins)));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithLimitAll(){
		try {
			mockMvc.perform(get("/showallusers")
					.param("show", "All")
					.param("page", "0"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallusers"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallusers.jsp"))
			.andExpect(model().attributeExists("usersPerPage"))
			.andExpect(model().attributeExists("usersCount"))
			.andExpect(model().attributeExists("users"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithLimit2PerPage(){
		try {
			mockMvc.perform(get("/showallusers")
					.param("show", "2")
					.param("page", "1"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallusers"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallusers.jsp"))
			.andExpect(model().attributeExists("usersPerPage"))
			.andExpect(model().attributeExists("usersCount"))
			.andExpect(model().attributeExists("users"))
			.andExpect(model().attributeExists("pages"))
			.andExpect(model().attributeExists("nextPrevPages"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithLimitGTMax(){
		try {
			mockMvc.perform(get("/showallusers")
					.param("show", "15")
					.param("page", "1"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallusers"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallusers.jsp"))
			.andExpect(model().attributeExists("usersPerPage"))
			.andExpect(model().attributeExists("usersCount"))
			.andExpect(model().attributeExists("users"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithPageZero(){
		try {
			mockMvc.perform(get("/showallusers")
					.param("show", "2")
					.param("page", "0"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallusers?page=1&show=2"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllUsersWithPageGTMaxPage(){
		try {
			mockMvc.perform(get("/showallusers")
					.param("show", "2")
					.param("page", "10"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallusers?page=7&show=2"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetAllStartupsWithLimitAll(){
		try {
			mockMvc.perform(get("/showallstartups")
					.param("show", "All")
					.param("page", "0"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallstartups"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallstartups.jsp"))
			.andExpect(model().attributeExists("startupsPerPage"))
			.andExpect(model().attributeExists("startupsCount"))
			.andExpect(model().attributeExists("startups"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithLimit2PerPage(){
		try {
			mockMvc.perform(get("/showallstartups")
					.param("show", "2")
					.param("page", "1"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallstartups"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallstartups.jsp"))
			.andExpect(model().attributeExists("startupsPerPage"))
			.andExpect(model().attributeExists("startupsCount"))
			.andExpect(model().attributeExists("startups"))
			.andExpect(model().attributeExists("pages"))
			.andExpect(model().attributeExists("nextPrevPages"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithLimitGTMax(){
		try {
			mockMvc.perform(get("/showallstartups")
					.param("show", "14")
					.param("page", "1"))
			.andExpect(status().isOk())
			.andExpect(view().name("/showallstartups"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/showallstartups.jsp"))
			.andExpect(model().attributeExists("startupsPerPage"))
			.andExpect(model().attributeExists("startupsCount"))
			.andExpect(model().attributeExists("startups"))
			.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithPageZero(){
		try {
			mockMvc.perform(get("/showallstartups")
					.param("show", "2")
					.param("page", "0"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallstartups?page=1&show=2"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllStartupsWithPageGTMaxPage(){
		try {
			mockMvc.perform(get("/showallstartups")
					.param("show", "2")
					.param("page", "10"))
			.andExpect(status().is3xxRedirection())
			.andExpect(view().name("redirect:/showallstartups?page=7&show=2"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCreateAdminAlreadyActive(){
		try {
			mockMvc.perform(get("/createadmin")
					.param("email", "admin1@abv.bg")
					.param("token", "1234"))
			.andExpect(status().isOk())
			.andExpect(view().name("/login"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
