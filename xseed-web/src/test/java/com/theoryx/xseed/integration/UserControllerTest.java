package com.theoryx.xseed.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.sql.DataSource;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.Assert;

import com.theoryx.xseed.XseedWebApplication;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Membership;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.service.MembershipService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = XseedWebApplication.class)
@WebAppConfiguration
@ActiveProfiles("integration_test_h2")
public class UserControllerTest {
	
	private MockMvc mockMvc;
	
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private StartupService startupService;
	@Autowired
	private UserService userService;
	@Autowired
	private MembershipService membershipService;
	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	private DataSource dataSource;
	
	private static Connection connection;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() {
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_register() {
		try {
			MockHttpServletRequestBuilder request = post("/register")
					.param("name", "Test user new")
					.param("email", "newuser@gmail.com")
					.param("password", "333333")
					.param("confirmationPassword", "333333")
					.param("startup.name", "New user startup");
			
			mockMvc.perform(request)
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/home"))
				.andExpect(redirectedUrl("/home"))
				.andExpect(model().attributeDoesNotExist("breadcrumbs"))
				.andExpect(request().sessionAttribute("currentStartup", notNullValue()))
				.andExpect(request().sessionAttribute("currentStartup", isA(StartupDTO.class)))
				.andExpect(request().sessionAttribute("currentStartup", hasProperty("name", equalTo("New user startup"))))
				.andExpect(request().sessionAttribute("currentUser", notNullValue()))
				.andExpect(request().sessionAttribute("currentUser", isA(UserDTO.class)))
				.andExpect(request().sessionAttribute("currentUser", hasProperty("name", equalTo("Test user new"))));
			
			User test = userService.getByEmail("newuser@gmail.com");
			Assert.assertTrue(test != null);
			membershipService.deleteMembership(test.getMemberships().get(0));
			startupService.deleteStartup(startupService.findByName("New user startup").getId());
			userService.deleteUserById(test.getId());
			User test2 = userService.getByEmail("newuser@gmail.com");
			Assert.assertTrue(test2 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_register_errors() {
		try {
			MockHttpServletRequestBuilder request = post("/register")
					.param("name", "T")
					.param("email", "newusergmail.com")
					.param("password", "333332")
					.param("confirmationPassword", "333333")
					.param("startup.name", "New user startup");
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/register"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/register.jsp"))
				.andExpect(model().attributeExists("name"))
				.andExpect(model().attributeExists("email"))
				.andExpect(model().attributeExists("startup.name"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("name", equalTo("T")))
				.andExpect(model().attribute("email", equalTo("newusergmail.com")))
				.andExpect(model().attribute("startup.name", equalTo("New user startup")))
				.andExpect(model().attribute("errors", hasSize(3)));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editProfile_nameonly() {
		UserDTO newUser = new UserDTO("Test new user name", "kojihu@abv.bg", "123456", "123456", true, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);
		UserDTO currentUser = userService.convertUserToUserDTO(user);
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
				user.getHashedPassword(), user.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
		try {
			MockHttpServletRequestBuilder request = post("/editprofile").principal(authToken).contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.param("name", "Test new user name 2")
					.param("password", "")
					.param("confirmationPassword", "")
					.sessionAttr("currentUser", currentUser);
			
			mockMvc.perform(request)
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/editprofile"))
				.andExpect(redirectedUrl("/editprofile"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(greaterThanOrEqualTo(1))))
				.andExpect(request().sessionAttribute("currentUser", notNullValue()))
				.andExpect(request().sessionAttribute("currentUser", isA(UserDTO.class)))
				.andExpect(request().sessionAttribute("currentUser", hasProperty("name", equalTo("Test new user name 2"))));
			
			User test = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(test != null);
			userService.deleteUserById(test.getId());
			User test2 = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(test2 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editProfile_pluspass() {
		UserDTO newUser = new UserDTO("Test new user name", "kojihu@abv.bg", "123456", "123456", true, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);
		UserDTO currentUser = userService.convertUserToUserDTO(user);
		String oldPass = user.getHashedPassword();
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(user.getEmail(),
				user.getHashedPassword(), user.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
		try {
			MockHttpServletRequestBuilder request = post("/editprofile").principal(authToken).contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.param("name", "Test new user name 2")
					.param("password", "123456")
					.param("confirmationPassword", "123456")
					.sessionAttr("currentUser", currentUser);
			
			mockMvc.perform(request)
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"))
				.andExpect(model().attributeDoesNotExist("breadcrumbs"))
				.andExpect(request().sessionAttribute("currentUser", nullValue()));
			
			User test = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(test != null);
			String newPass = test.getHashedPassword();
			Assert.assertTrue(!newPass.equals(oldPass));
			userService.deleteUserById(test.getId());
			User test2 = userService.getByEmail("kojihu@abv.bg");
			Assert.assertTrue(test2 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editProfile_errors() {
		try {
			MockHttpServletRequestBuilder request = post("/editprofile").contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
					.param("name", "Test new user name 2")
					.param("password", "1234567")
					.param("confirmationPassword", "123456")
					.requestAttr("breadCrumbs", new ArrayList<Error>());
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/editprofile"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editprofile.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeExists("name"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<GrantedAuthority> getGrantedAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		return authorities;
	}
	
	@Test
	public void test_sendResetPasswordEmail() {
		UserDTO newUser = new UserDTO("Test new user name", "d.dodnikov@theoryx.com", "123456", "123456", true, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);

		try {
			MockHttpServletRequestBuilder request = post("/reset-password")
					.param("email", user.getEmail());
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/reset-password"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/reset-password.jsp"))
				.andExpect(model().attributeExists("success"))
				.andExpect(model().attribute("success", hasToString(messageSource.getMessage("success.email", new Object[0], Locale.getDefault()))));
			
			User test = userService.getByEmail("d.dodnikov@theoryx.com");
			Assert.assertTrue(test != null);
			userService.deleteUserById(test.getId());
			User test2 = userService.getByEmail("d.dodnikov@theoryx.com");
			Assert.assertTrue(test2 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createNewPassword_post() {
		User user = userService.getByEmail("test1@abv.bg");
		Assert.assertTrue(user != null);
		UserDTO dto = userService.convertUserToUserDTO(user);
		String token = userService.generatePasswordToken(dto);
		String newPass = "222222";

		try {
			MockHttpServletRequestBuilder request = post("/forgottenpassword")
					.param("password", newPass)
					.param("confirmationPassword", newPass)
					.param("email", user.getEmail())
					.param("token", token);
			
			mockMvc.perform(request)
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"));

			String oldPassEncrypt = user.getHashedPassword();
			user.setHashedPassword(oldPassEncrypt);
			User def = userService.save(user);
			Assert.assertTrue(def.getHashedPassword().equals(oldPassEncrypt));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_updateNewUserInfo() {
		UserDTO newUser = new UserDTO("", "test2@gmail.com", "asdfghjkl", "asdfghjkl", false, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);

		try {
			MockHttpServletRequestBuilder request = post("/updatenewuser")
					.param("password", "333333")
					.param("confirmationPassword", "333333")
					.param("email", "test2@gmail.com")
					.param("isActive", "true")
					.param("name", "Test 2 test");
			
			mockMvc.perform(request)
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"));

			User user2 = userService.getByEmail("test2@gmail.com");
			Assert.assertTrue(user2.getName().equals("Test 2 test"));
			
			userService.deleteUserById(user2.getId());
			User user3 = userService.getByEmail("test2@gmail.com");
			Assert.assertTrue(user3 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editAddedUser_correcttoken() {
		UserDTO newUser = new UserDTO("", "test2@gmail.com", "asdfghjkl", "asdfghjkl", false, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);
		UserDTO dto = userService.convertUserToUserDTO(user);
		String token = userService.generateActivationToken(dto);
		String protectionToken = userService.generateProtectionToken(user.getEmail());
		try {
			/*MockHttpServletRequestBuilder request = get("/activation")
					.param("email", user.getEmail())
					.param("token", token);*/
			
			mockMvc.perform(get("/activation")
					.param("email", user.getEmail())
					.param("token", token)
					.param("referer", protectionToken))
				.andExpect(status().isOk())
				.andExpect(view().name("/editnewuserprofile"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editnewuserprofile.jsp"))
				.andExpect(model().attributeExists("email"))
				.andExpect(model().attributeExists("userdto"))
				.andExpect(model().attribute("email", is("test2@gmail.com")))
				.andExpect(model().attribute("userdto", isA(UserDTO.class)));

			userService.deleteUserById(user.getId());
			User user3 = userService.getByEmail("test2@gmail.com");
			Assert.assertTrue(user3 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editAddedUser_incorrecttoken() {
		UserDTO newUser = new UserDTO("", "test2@gmail.com", "asdfghjkl", "asdfghjkl", false, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);
		
		StartupDTO startupDto = new StartupDTO("tttt", "", "", "", "");
		Startup startup = startupService.createStartup(startupDto);
		Assert.assertTrue(startup != null);
		
		Membership membership = new Membership(user, startup, UserRole.USER);
		membership = membershipService.saveMembership(membership);
		Assert.assertTrue(membership != null);
		
		String token = "asdvdvdvd";
		String error = messageSource.getMessage("error.new-activation-link", new Object[0], Locale.getDefault());
		String protectionToken = userService.generateProtectionToken(user.getEmail());
		
		try {
			MockHttpServletRequestBuilder request = get("/activation")
					.param("token", token)
					.param("email", user.getEmail())
					.param("referer", protectionToken);
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/login"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/login.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasSize(1)))
				.andExpect(model().attribute("errors", hasItem(hasProperty("message", hasToString(error)))));

			membershipService.deleteMembership(membership);
			
			userService.deleteUserById(user.getId());
			User user2 = userService.getByEmail("test2@gmail.com");
			Assert.assertTrue(user2 == null);
			
			startupService.deleteStartup(startup.getId());
			Startup startup2 = startupService.findByName("tttt");
			Assert.assertTrue(startup2 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getAllStartups() {
		Integer startupcount = startupService.getAllStartupCount();

		try {
			MockHttpServletRequestBuilder request = get("/showallstartups")
					.param("page", "1")
					.param("show", "25");
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/showallstartups"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/showallstartups.jsp"))
				.andExpect(model().attributeDoesNotExist("pages"))
				.andExpect(model().attributeExists("startupsPerPage"))
				.andExpect(model().attributeExists("startupsCount"))
				.andExpect(model().attributeExists("startups"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeDoesNotExist("nextPrevPages"))
				.andExpect(model().attribute("startups", isA(List.class)))
				.andExpect(model().attribute("startups", hasSize(startupcount)))
				.andExpect(model().attribute("startupsCount", isA(Integer.class)))
				.andExpect(model().attribute("startupsCount", equalTo(startupcount)))
				.andExpect(model().attribute("startupsPerPage", isA(Integer.class)))
				.andExpect(model().attribute("startupsPerPage", equalTo(25)))
				.andExpect(model().attribute("breadcrumbs", isA(List.class)))
				.andExpect(model().attribute("breadcrumbs", hasSize(greaterThan(1))));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getAllUsers() {
		Integer allUsersCount = userService.getUsersCount();

		try {
			MockHttpServletRequestBuilder request = get("/showallusers")
					.param("page", "1")
					.param("show", "15");
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/showallusers"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/showallusers.jsp"))
				.andExpect(model().attributeDoesNotExist("pages"))
				.andExpect(model().attributeExists("usersPerPage"))
				.andExpect(model().attributeExists("usersCount"))
				.andExpect(model().attributeExists("users"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeDoesNotExist("nextPrevPages"))
				.andExpect(model().attribute("users", isA(List.class)))
				.andExpect(model().attribute("users", hasSize(allUsersCount)))
				.andExpect(model().attribute("usersCount", isA(Integer.class)))
				.andExpect(model().attribute("usersCount", equalTo(allUsersCount)))
				.andExpect(model().attribute("usersPerPage", isA(Integer.class)))
				.andExpect(model().attribute("usersPerPage", equalTo(15)))
				.andExpect(model().attribute("breadcrumbs", isA(List.class)))
				.andExpect(model().attribute("breadcrumbs", hasSize(greaterThan(1))));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createNewPassword_get_correnttoken() {
		UserDTO newUser = new UserDTO("Ivaneeee", "test2@gmail.com", "asdfghjkl", "asdfghjkl", true, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);
		UserDTO dto = userService.convertUserToUserDTO(user);
		String token = userService.generatePasswordToken(dto);

		try {
			MockHttpServletRequestBuilder request = get("/forgottenpassword")
					.param("token", token)
					.param("email", user.getEmail());
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/forgotten-password"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/forgotten-password.jsp"))
				.andExpect(model().attributeExists("token"))
				.andExpect(model().attributeExists("email"))
				.andExpect(model().attributeDoesNotExist("errors"))
				.andExpect(model().attribute("token", equalTo(token)))
				.andExpect(model().attribute("email", equalTo(user.getEmail())));
			
			userService.deleteUserById(user.getId());
			User user3 = userService.getByEmail("test2@gmail.com");
			Assert.assertTrue(user3 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_createNewPassword_get_incorrenttoken() {
		UserDTO newUser = new UserDTO("Ivaneeee", "test2@gmail.com", "asdfghjkl", "asdfghjkl", true, UserRole.USER);
		User user = userService.createUser(newUser);
		Assert.assertTrue(user != null);
		String token = "incorrecttoken";

		try {
			MockHttpServletRequestBuilder request = get("/forgottenpassword")
					.param("token", token)
					.param("email", user.getEmail());
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/forgotten-password"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/forgotten-password.jsp"))
				.andExpect(model().attributeExists("token"))
				.andExpect(model().attributeExists("email"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("token", equalTo(token)))
				.andExpect(model().attribute("email", equalTo(user.getEmail())))
				.andExpect(model().attribute("errors", isA(List.class)))
				.andExpect(model().attribute("errors", hasSize(1)));
			
			userService.deleteUserById(user.getId());
			User user3 = userService.getByEmail("test2@gmail.com");
			Assert.assertTrue(user3 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_chooseHowToProceedWithtivation_nouser(){
		String protectionToken = userService.generateProtectionToken("email");
		try {
			MockHttpServletRequestBuilder request = get("/invitation")
					.param("token", "token")
					.param("email", "email");
			
			mockMvc.perform(request)
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/activation?token=token&email=email&referer="+protectionToken))
				.andExpect(redirectedUrl("/activation?token=token&email=email&referer="+protectionToken));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_chooseHowToProceedWithtivation(){	
		String protectionToken = userService.generateProtectionToken("email");
		try {
			MockHttpServletRequestBuilder request = get("/invitation")
					.param("token", "token")
					.param("email", "email")
					.sessionAttr("currentUser", new UserDTO());
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/invitation"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/invitation.jsp"))
				.andExpect(model().attributeExists("token"))
				.andExpect(model().attributeExists("email"))
				.andExpect(model().attributeExists("referer"))
				.andExpect(model().attribute("token", equalTo("token")))
				.andExpect(model().attribute("email", equalTo("email")))
				.andExpect(model().attribute("referer", equalTo(protectionToken)));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
