package com.theoryx.xseed.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.theoryx.xseed.XseedWebApplication;
import com.theoryx.xseed.dto.CountryDTO;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = XseedWebApplication.class)
@WebAppConfiguration
@ActiveProfiles("integration_test_h2")
public class SnapshotControllerTest {

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private DataSource dataSource;

	private MockMvc mockMvc;
	private static Connection connection;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() {
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetAllSnapshotsWithValidUser() {
		CountryDTO countryDTO = new CountryDTO();
		countryDTO.setId(26);
		countryDTO.setName("Bulgaria");

		UserDTO currentUser = new UserDTO();
		StartupDTO currentStartupDTO = new StartupDTO();
		currentStartupDTO.setId(1);
		currentStartupDTO.setName("test1_startup");
		currentStartupDTO.setEmail("");
		currentStartupDTO.setVat("");
		currentStartupDTO.setWebsite("");
		currentStartupDTO.setCountry(countryDTO);

		try {
			mockMvc.perform(get("/snapshots").sessionAttr("currentUser", currentUser).sessionAttr("currentStartup",
					currentStartupDTO)).andExpect(status().isOk()).andExpect(view().name("/snapshots"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/snapshots.jsp"))
					.andExpect(model().attributeExists("snapshots"))
					.andExpect(model().attribute("snapshots", hasSize(equalTo(2))))
					.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetAllSnapshotsWithNullUser() {
		try {
			mockMvc.perform(get("/snapshots")).andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetSnapshotDetails() {
		CountryDTO countryDTO = new CountryDTO();
		countryDTO.setId(26);
		countryDTO.setName("Bulgaria");

		UserDTO currentUser = new UserDTO();
		StartupDTO currentStartupDTO = new StartupDTO();
		currentStartupDTO.setId(1);
		currentStartupDTO.setName("test1_startup");
		currentStartupDTO.setEmail("");
		currentStartupDTO.setVat("");
		currentStartupDTO.setWebsite("");
		currentStartupDTO.setCountry(countryDTO);

		try {
			mockMvc.perform(get("/snapshot-details/1").param("id", "1").sessionAttr("currentUser", currentUser)
					.sessionAttr("currentStartup", currentStartupDTO)).andExpect(status().isOk())
					.andExpect(view().name("/snapshotdetails"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/snapshotdetails.jsp"))
					.andExpect(model().attributeExists("snapshot")).andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetSnapshotDetailsWithNullUser() {
		try {
			mockMvc.perform(get("/snapshot-details/1").param("id", "1")).andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/403"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetSnapshotDetailsWithWrongId() {
		CountryDTO countryDTO = new CountryDTO();
		countryDTO.setId(26);
		countryDTO.setName("Bulgaria");

		UserDTO currentUser = new UserDTO();
		StartupDTO currentStartupDTO = new StartupDTO();
		currentStartupDTO.setId(1);
		currentStartupDTO.setName("test1_startup");
		currentStartupDTO.setEmail("");
		currentStartupDTO.setVat("");
		currentStartupDTO.setWebsite("");
		currentStartupDTO.setCountry(countryDTO);

		try {
			mockMvc.perform(get("/snapshot-details/5").param("id", "5").sessionAttr("currentUser", currentUser)
					.sessionAttr("currentStartup", currentStartupDTO)).andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/403"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
