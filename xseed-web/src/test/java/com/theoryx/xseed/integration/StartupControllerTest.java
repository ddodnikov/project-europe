package com.theoryx.xseed.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.sql.DataSource;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.Assert;

import com.theoryx.xseed.XseedWebApplication;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Startup;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;
import com.theoryx.xseed.service.CountryService;
import com.theoryx.xseed.service.MembershipService;
import com.theoryx.xseed.service.StartupService;
import com.theoryx.xseed.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = XseedWebApplication.class)
@WebAppConfiguration
@ActiveProfiles("integration_test_h2")
public class StartupControllerTest {
	
	private MockMvc mockMvc;
	
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private StartupService startupService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private UserService userService;
	@Autowired
	private MembershipService membershipService;
	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private DataSource dataSource;
	
	private static Connection connection;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() {
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_editStartup() {
		StartupDTO newStartup = new StartupDTO("My name", "myname@gmail.com", "025147893", "www.myname.com", "7412589630");
		newStartup.setCountry(countryService.convertCountryToCountryDTO(countryService.getByName("Italy")));
		Startup newst = startupService.createStartup(newStartup);
		newStartup.setId(newst.getId());
		Assert.assertTrue(startupService.findByName("My name") != null);
		
		Integer numberOfCountries = countryService.getAllCountries().size();
		try {
			MockHttpServletRequestBuilder request = post("/editstartup")
					.param("name", "My startup po")
					.param("email", "newemail@gmail.com")
					.param("phone", "0874253961")
					.param("website", "www.newwebsite.com")
					.param("vat", "753024869")
					.param("country.name", "Bulgaria")
					.sessionAttr("currentStartup", newStartup);
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/editstartup"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("breadcrumbs", hasSize(greaterThanOrEqualTo(1))))
				.andExpect(model().attributeExists("countries"))
				.andExpect(model().attribute("countries", hasSize(numberOfCountries)))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(request().sessionAttribute("currentStartup", notNullValue()))
				.andExpect(request().sessionAttribute("currentStartup", hasProperty("name", equalTo("My startup po"))));
			
			Startup test = startupService.findByName("My startup po");
			Assert.assertTrue(test != null);
			startupService.deleteStartup(test.getId());
			Startup test2 = startupService.findByName("My startup po");
			Assert.assertTrue(test2 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_addUserToStartup() {
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("test1_startup"));
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
		try {
			MockHttpServletRequestBuilder request = post("/add-user-to-startup").principal(authToken)
					.param("email", "d.dodnikov@theoryx.com")
					.sessionAttr("currentStartup", currentStartup)
					.sessionAttr("currentUser", currentUserDTO);

			mockMvc.perform(request)
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(view().name("/adduser"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attributeExists("startupdto"))
				.andExpect(model().attribute("startupdto", isA(StartupDTO.class)))
				.andExpect(model().attributeExists("success"))
				.andExpect(model().attribute("success", messageSource.getMessage("success.email", new Object[0], Locale.getDefault())));
			
			User user = userService.getByEmail("d.dodnikov@theoryx.com");
			Assert.assertTrue(user != null);
			membershipService.deleteMembership(user.getMemberships().get(0));
			userService.deleteUserByEmail("d.dodnikov@theoryx.com");
			User user2 = userService.getByEmail("d.dodnikov@theoryx.com");
			Assert.assertTrue(user2 == null);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEditStartupWithUserRole(){
		//StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("test1_startup"));
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    currentUserDTO.getMembership().setRole(UserRole.USER);
	    
	    try {
			mockMvc.perform(get("/editstartup")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEditStartupWithAdminRole(){
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("test1_startup"));
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(get("/editstartup")
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", currentStartup))
			.andExpect(status().isOk())
			.andExpect(view().name("/editstartup"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attributeExists("startupdto"))
			.andExpect(model().attributeExists("countries"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	}
	
	@Test
	public void testEditStartupWithNullCountry(){
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("test1_startup"));
		currentStartup.setCountry(null);
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(get("/editstartup")
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", currentStartup))
			.andExpect(status().isOk())
			.andExpect(view().name("/editstartup"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/editstartup.jsp"))
			.andExpect(model().attributeExists("breadcrumbs"))
			.andExpect(model().attributeExists("startupdto"))
			.andExpect(model().attributeExists("countries"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddUserToStartupGetWithAdminRole(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(get("/add-user-to-startup")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().isOk())
					.andExpect(view().name("/adduser"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/adduser.jsp"))
					.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddUserToStartupGetWithUserRole(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    currentUserDTO.getMembership().setRole(UserRole.USER);
	    
	    try {
			mockMvc.perform(get("/add-user-to-startup")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testShowStartupUsersGetWithAdminRole(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(get("/showusers")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().isOk())
					.andExpect(view().name("/startupusers"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/startupusers.jsp"))
					.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testShowStartupUsersGetWithUserRole(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    currentUserDTO.getMembership().setRole(UserRole.USER);
	    
	    try {
			mockMvc.perform(get("/showusers")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupWithAdminRole(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(get("/remove-user-from-startup")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().isOk())
					.andExpect(view().name("/removeusers"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/removeusers.jsp"))
					.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveUserFromStartupWithUserRole(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    currentUserDTO.getMembership().setRole(UserRole.USER);
	    
	    try {
			mockMvc.perform(get("/remove-user-from-startup")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/home"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testShowUsersInStartup(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(get("/startups")
					.sessionAttr("currentUser", currentUserDTO))
					.andExpect(status().isOk())
					.andExpect(view().name("/startups"))
					.andExpect(forwardedUrl("/WEB-INF/jsp/startups.jsp"))
					.andExpect(model().attributeExists("defaultStartup"))
					.andExpect(model().attributeExists("userStartups"))
					.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSetDafaultAndCurrentStartupChangeDefault(){
		User currentUser = userService.getByEmail("test2@abv.bg");
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("Test2Startup"));
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(post("/setstartups")
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", currentStartup)
					.param("current", "2")
					.param("default", "1"))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/startups"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSetDafaultAndCurrentStartupChangeCurrent(){
		User currentUser = userService.getByEmail("test2@abv.bg");
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("Test2Startup"));
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(post("/setstartups")
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", currentStartup)
					.param("current", "1")
					.param("default", "2"))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/startups"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSetDafaultAndCurrentStartupChangeBoth(){
		User currentUser = userService.getByEmail("test2@abv.bg");
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("Test2Startup"));
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(post("/setstartups")
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", currentStartup)
					.param("current", "1")
					.param("default", "1"))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/startups"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSetDafaultAndCurrentStartupChangeNo(){
		User currentUser = userService.getByEmail("test2@abv.bg");
		StartupDTO currentStartup = startupService.convertStartupToStartupDTO(startupService.findByName("Test2Startup"));
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
	    
	    UserDTO currentUserDTO = userService.getCurrentUserDTO();
	    
	    try {
			mockMvc.perform(post("/setstartups")
					.sessionAttr("currentUser", currentUserDTO)
					.sessionAttr("currentStartup", currentStartup)
					.param("current", "2")
					.param("default", "2"))
					.andExpect(status().is3xxRedirection())
					.andExpect(view().name("redirect:/startups"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<GrantedAuthority> getGrantedAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		return authorities;
	}
}
