package com.theoryx.xseed.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.sql.DataSource;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.Assert;

import com.theoryx.xseed.XseedWebApplication;
import com.theoryx.xseed.dto.ReportRow;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.service.StartupService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = XseedWebApplication.class)
@WebAppConfiguration
@ActiveProfiles("integration_test_h2")
public class ReportsControllerTest {

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private StartupService startupService;
	
	@Autowired
	private DataSource dataSource;
	
	private MockMvc mockMvc;
	private static Connection connection;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() {
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetAllReports(){
		try {
			mockMvc.perform(get("/reports"))
				.andExpect(status().isOk())	
				.andExpect(view().name("/reports"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/reports.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetReport(){
		StartupDTO startupDTO = startupService.convertStartupToStartupDTO(startupService.findByName("Test2Startup"));
		
		try {
			MvcResult result = mockMvc.perform(get("/progressreport")
					.sessionAttr("currentStartup", startupDTO))
			.andExpect(status().isOk())
			.andExpect(view().name("/report-details"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/report-details.jsp"))
			.andExpect(model().attributeExists("data"))
			.andExpect(model().attributeExists("months"))
			.andExpect(model().attributeExists("phaseRows"))
			.andReturn();
			
			int months = (int) result.getModelAndView().getModel().get("months");
			@SuppressWarnings("unchecked")
			HashMap<String, Integer> phaseRows = (HashMap<String, Integer>) result.getModelAndView().getModel().get("phaseRows");
			@SuppressWarnings("unchecked")
			LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>> rows = (LinkedHashMap<String, LinkedHashMap<String, LinkedList<ReportRow>>>) result.getModelAndView().getModel().get("data");
			
			Assert.assertEquals(1, months);
			Assert.assertTrue(phaseRows.containsKey("What is your Venture about?"));
			Assert.assertTrue(phaseRows.containsKey("Who is your Team"));
			Assert.assertTrue(phaseRows.containsKey("How do you design and build your Product"));
			Assert.assertTrue(phaseRows.containsKey("Who is your Customer?"));
			Assert.assertEquals(phaseRows.get("How do you design and build your Product"), new Integer(2));
			Assert.assertEquals(phaseRows.get("Who is your Customer?"), new Integer(3));
						
			Assert.assertEquals(rows.get("What is your Venture about?").get("General Venture Info").size(), 3);
			Assert.assertEquals(rows.get("What is your Venture about?").get("General Venture Info").get(0).getQuestionTitle(), "What is your monthly revenue (Approximately in Euros)?");
			Assert.assertEquals(rows.get("What is your Venture about?").get("General Venture Info").get(0).getTargetValue(), "3K - 4K");
			Assert.assertTrue(rows.get("What is your Venture about?").get("General Venture Info").get(0).isDelta());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetReportErrors(){
		StartupDTO startupDTO = new StartupDTO();
		try {
			mockMvc.perform(get("/progressreport")
					.sessionAttr("currentStartup", startupDTO))
			.andExpect(status().isOk())
			.andExpect(view().name("/report-details"))
			.andExpect(forwardedUrl("/WEB-INF/jsp/report-details.jsp"))
			.andExpect(model().attributeExists("errors"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
