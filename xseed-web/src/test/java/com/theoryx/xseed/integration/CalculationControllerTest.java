package com.theoryx.xseed.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.Assert;

import com.google.gson.Gson;
import com.theoryx.xseed.XseedWebApplication;
import com.theoryx.xseed.dto.AjaxQuestion;
import com.theoryx.xseed.dto.CalculationDTO;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.AnswerOption;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.service.CalculationService;
import com.theoryx.xseed.service.QuestionService;
import com.theoryx.xseed.service.UserService;
import com.theoryx.xseed.utils.DateUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = XseedWebApplication.class)
@WebAppConfiguration
@ActiveProfiles("integration_test_h2")
public class CalculationControllerTest {
	
	private MockMvc mockMvc;
	
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private CalculationService calculationService;
	@Autowired
	private UserService userService;
	
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private DataSource dataSource;

	private static Connection connection;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() {
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getCalculationPage() {
		String name = DateUtils.getCurrentDateDefaultName();
		Integer kpi = questionService.findByKpi(true).size();
		Integer filter = questionService.findByFilter(true).size();
		try {
			MockHttpServletRequestBuilder request = get("/calculate")
						.sessionAttr("currentUser", new UserDTO());
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/calculate"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculate.jsp"))
				.andExpect(model().attributeExists("filterQuestions"))
				.andExpect(model().attributeExists("kpiQuestions"))
				.andExpect(model().attributeExists("defaultName"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("defaultName", equalTo(name)))
				.andExpect(model().attribute("filterQuestions", isA(List.class)))
				.andExpect(model().attribute("filterQuestions", hasSize(filter)))
				.andExpect(model().attribute("kpiQuestions", isA(List.class)))
				.andExpect(model().attribute("kpiQuestions", hasSize(kpi)))
				.andExpect(model().attribute("breadcrumbs", isA(List.class)))
				.andExpect(model().attribute("breadcrumbs", hasSize(greaterThanOrEqualTo(1))));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getDetails() {
		try {
			MockHttpServletRequestBuilder request = get("/calculation-details")
					.param("calculationid", "1");
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/calculation-details"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-details.jsp"))
				.andExpect(model().attributeExists("questionCalculations"))
				.andExpect(model().attributeExists("groupCalculations"))
				.andExpect(model().attributeExists("startupCalculations"))
				.andExpect(model().attributeExists("calculation"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("questionCalculations", isA(List.class)))
				.andExpect(model().attribute("groupCalculations", isA(List.class)))
				.andExpect(model().attribute("startupCalculations", isA(List.class)))
				.andExpect(model().attribute("calculation", isA(CalculationDTO.class)))
				.andExpect(model().attribute("breadcrumbs", isA(List.class)))
				.andExpect(model().attribute("breadcrumbs", hasSize(greaterThanOrEqualTo(1))))
				.andExpect(model().attribute("startupCalculations", hasSize(13)))
				.andExpect(model().attribute("groupCalculations", hasSize(12)));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getCalculations() {
		Integer calculations = calculationService.findAll().size();
		try {
			MockHttpServletRequestBuilder request = get("/calculations");
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(view().name("/calculations"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculations.jsp"))
				.andExpect(model().attributeExists("calculations"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("calculations", hasSize(calculations)))
				.andExpect(model().attribute("calculations", isA(List.class)))
				.andExpect(model().attribute("breadcrumbs", hasSize(greaterThan(1))))
				.andExpect(model().attribute("breadcrumbs", isA(List.class)));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_checkFilter_empty() {
		List<AjaxQuestion> body = Arrays.asList(new AjaxQuestion());
		Gson gson = new Gson();
		String json = gson.toJson(body);
		String response = "{\"result\":\"missingInput\"}";
		try {
			MockHttpServletRequestBuilder request = post("/checkfilter").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON).content(json);
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(content().string(response));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_checkFilter_results() {
		List<AjaxQuestion> body = getajaxquestions();
		Gson gson = new Gson();
		String json = gson.toJson(body);
		String response = "{\"result\":\"resultsFound\"}";
		try {
			MockHttpServletRequestBuilder request = post("/checkfilter").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON).content(json);
			
			mockMvc.perform(request)
				.andExpect(status().isOk())
				.andExpect(content().string(response));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<AjaxQuestion> getajaxquestions(){
		List<AjaxQuestion> body = new ArrayList<AjaxQuestion>();
		AjaxQuestion q = new AjaxQuestion();
		q.setQuestionId(1);
		q.setAnswerIds(Arrays.asList(9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33));
		AjaxQuestion q1 = new AjaxQuestion();
		q1.setQuestionId(2);
		q1.setAnswerIds(Arrays.asList(34,35,36,37,38,39));
		AjaxQuestion q2 = new AjaxQuestion();
		q2.setQuestionId(3);
		q2.setAnswerIds(Arrays.asList(40,41,42,43,44,45,46,47,48,49,50,51,52,53));
		AjaxQuestion q3 = new AjaxQuestion();
		q3.setQuestionId(6);
		q3.setAnswerIds(Arrays.asList(54,55,56,57,58,59));
		AjaxQuestion q4 = new AjaxQuestion();
		q4.setQuestionId(7);
		q4.setAnswerIds(Arrays.asList(60,61,62,63,64,65,66));
		AjaxQuestion q5 = new AjaxQuestion();
		q5.setQuestionId(8);
		q5.setAnswerIds(Arrays.asList(67,68,69,70,71,72));
		AjaxQuestion q6 = new AjaxQuestion();
		q6.setQuestionId(9);
		q6.setAnswerIds(Arrays.asList(73,74,75,76,77,78,79));
		AjaxQuestion q7 = new AjaxQuestion();
		q7.setQuestionId(90);
		q7.setAnswerIds(Arrays.asList(87,88,89,90,91,92));
		body.addAll(Arrays.asList(q,q1,q2,q3,q4,q5,q6,q7));
		return body;
	}

	private List<GrantedAuthority> getGrantedAuthorities() {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		return authorities;
	}
	
	@Test
	public void test_filter(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
		
		try {
			MvcResult result = mockMvc.perform(post("/calculate")
							.param("calculationName", "CalculationTest1")
							.param("multiple_2_34", "Concept")
							.param("multiple_2_35", "In Development")
							.param("multiple_2_36", "Working Prototype")
							.param("multiple_6_54", "I prefer not to disclose")
							.param("multiple_6_55", "Not profitable yet - expected in less than 6 months")
							.param("multiple_7_62", "Validating that our customers actually want our product")
							.param("multiple_7_63", "Making our customer acquisition process more efficient / streamlining the product")
							.param("multiple_8_67", "Less than 1 month")
							.param("multiple_8_68", "1 - 3 months")
							.param("multiple_3_41", "Web")
							.param("multiple_3_46", "Consulting")
							.param("multiple_1_9", "Advertising")
							.param("multiple_1_10", "Analytics")
							.param("multiple_1_11", "Apps (e.g. Mobile or Social)")
							.param("multiple_9_76", "You want to build a great product (iPhone)")
							.param("multiple_9_77", "You have a technology")
							.param("multiple_90_89", "About 3 months")
							.param("multiple_90_91", "About 12 months")
							.param("kpi", "4"))
				.andExpect(status().isOk())
				.andExpect(view().name("/calculation-summary"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-summary.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeExists("calculation"))
				.andReturn();
			
			Map<String, Object> model = result.getModelAndView().getModel();
			CalculationDTO calculation = (CalculationDTO)model.get("calculation");
			Assert.assertEquals(calculation.getKpi().getId(), new Integer(4));
			Assert.assertEquals(calculation.getNumberOfStartups(), new Integer(7));
			Assert.assertEquals(calculation.getT(), new Double(100));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_filter_with_null_kpi(){
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
		
		try {
			MvcResult result = mockMvc.perform(post("/calculate")
							.param("calculationName", "CalculationTest1")
							.param("multiple_2_34", "Concept")
							.param("multiple_2_35", "In Development")
							.param("multiple_2_36", "Working Prototype")
							.param("multiple_6_54", "I prefer not to disclose")
							.param("multiple_6_55", "Not profitable yet - expected in less than 6 months")
							.param("multiple_7_62", "Validating that our customers actually want our product")
							.param("multiple_7_63", "Making our customer acquisition process more efficient / streamlining the product")
							.param("multiple_8_67", "Less than 1 month")
							.param("multiple_8_68", "1 - 3 months")
							.param("multiple_3_41", "Web")
							.param("multiple_3_46", "Consulting")
							.param("multiple_1_9", "Advertising")
							.param("multiple_1_10", "Analytics")
							.param("multiple_1_11", "Apps (e.g. Mobile or Social)")
							.param("multiple_9_76", "You want to build a great product (iPhone)")
							.param("multiple_9_77", "You have a technology")
							.param("multiple_90_89", "About 3 months")
							.param("multiple_90_91", "About 12 months"))
				.andExpect(status().isOk())
				.andExpect(view().name("/calculation-summary"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-summary.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andReturn();
			
			Map<String, Object> model = result.getModelAndView().getModel();
			Assert.assertTrue(model.containsKey("noResultsFound"));
			Assert.assertEquals(model.get("noResultsFound"), messageSource.getMessage("message.filter", new Object[0], Locale.getDefault()));			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_filter_with_empty_snapshotlines(){
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data_all_filter_questions_false.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
		
		try {
			MvcResult result = mockMvc.perform(post("/calculate")
					.param("calculationName", "CalculationTest1")
					.param("multiple_2_34", "Concept")
					.param("multiple_2_35", "In Development")
					.param("multiple_2_36", "Working Prototype")
					.param("multiple_6_54", "I prefer not to disclose")
					.param("multiple_6_55", "Not profitable yet - expected in less than 6 months")
					.param("multiple_7_62", "Validating that our customers actually want our product")
					.param("multiple_7_63", "Making our customer acquisition process more efficient / streamlining the product")
					.param("multiple_8_67", "Less than 1 month")
					.param("multiple_8_68", "1 - 3 months")
					.param("multiple_3_41", "Web")
					.param("multiple_3_46", "Consulting")
					.param("multiple_1_9", "Advertising")
					.param("multiple_1_10", "Analytics")
					.param("multiple_1_11", "Apps (e.g. Mobile or Social)")
					.param("multiple_9_76", "You want to build a great product (iPhone)")
					.param("multiple_9_77", "You have a technology")
					.param("multiple_90_89", "About 3 months")
					.param("multiple_90_91", "About 12 months")
					.param("kpi", "4"))
				.andExpect(status().isOk())
				.andExpect(view().name("/calculation-summary"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-summary.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andReturn();
			
			Map<String, Object> model = result.getModelAndView().getModel();
			Assert.assertTrue(model.containsKey("noResultsFound"));
			Assert.assertEquals(model.get("noResultsFound"), messageSource.getMessage("message.filter", new Object[0], Locale.getDefault()));			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_filter_with_empty_filteredSnapshots(){
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data_1_snapshot.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
		
		try {
			MvcResult result = mockMvc.perform(post("/calculate")
					.param("calculationName", "CalculationTest1")
					.param("multiple_2_34", "Concept")
					.param("multiple_2_35", "In Development")
					.param("multiple_2_36", "Working Prototype")
					.param("multiple_6_54", "I prefer not to disclose")
					.param("multiple_6_55", "Not profitable yet - expected in less than 6 months")
					.param("multiple_7_62", "Validating that our customers actually want our product")
					.param("multiple_7_63", "Making our customer acquisition process more efficient / streamlining the product")
					.param("multiple_8_67", "Less than 1 month")
					.param("multiple_8_68", "1 - 3 months")
					.param("multiple_3_41", "Web")
					.param("multiple_3_46", "Consulting")
					.param("multiple_1_9", "Advertising")
					.param("multiple_1_10", "Analytics")
					.param("multiple_1_11", "Apps (e.g. Mobile or Social)")
					.param("multiple_9_76", "You want to build a great product (iPhone)")
					.param("multiple_9_77", "You have a technology")
					.param("multiple_90_89", "About 3 months")
					.param("multiple_90_91", "About 12 months")
					.param("kpi", "4"))
				.andExpect(status().isOk())
				.andExpect(view().name("/calculation-summary"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculation-summary.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andReturn();
			
			Map<String, Object> model = result.getModelAndView().getModel();
			Assert.assertTrue(model.containsKey("noResultsFound"));
			Assert.assertEquals(model.get("noResultsFound"), messageSource.getMessage("message.no-startups-found", new Object[0], Locale.getDefault()));			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_filter_with_null_calculation(){
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data_1_snapshot.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		User currentUser = userService.getByEmail("test1@abv.bg");
		
		UserDetails userdetails = new org.springframework.security.core.userdetails.User(currentUser.getEmail(),
				currentUser.getHashedPassword(), currentUser.getIsActive(), true, true, true, getGrantedAuthorities());
		
		Authentication authToken = new UsernamePasswordAuthenticationToken (userdetails.getUsername(), userdetails.getPassword(), userdetails.getAuthorities());
	    SecurityContextHolder.getContext().setAuthentication(authToken);
		
		try {
			MockHttpServletRequestBuilder request = post("/calculate").param("calculationName", "CalculationTest1").param("kpi", "4");
			Map<String, String> multipleAnswers = multipleAnswers();
			for (Entry<String, String> entry : multipleAnswers.entrySet()) {
				request.param(entry.getKey(), entry.getValue());
			}
			String error = messageSource.getMessage("calculate.calculation-error", new Object[0], Locale.getDefault());

			mockMvc.perform(request)
			.andDo(print())
				.andExpect(status().isOk())
				.andExpect(view().name("/calculations"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/calculations.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeExists("calculationError"))
				.andExpect(model().attribute("calculationError", equalTo(error)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Map<String, String> multipleAnswers(){
		Map<String, String> multipleAnswers = new HashMap<String, String>();
		List<Question> filterQuestions = questionService.findByFilter(true);
		System.out.println(filterQuestions.size());
		for (Question question : filterQuestions) {
			for (AnswerOption answerOption : question.getAnswergroup().getOptions()) {
				String name = "multiple_" + question.getId() + "_" + answerOption.getId();
				String value = answerOption.getText();
				multipleAnswers.put(name, value);
			}
		}
		return multipleAnswers;
	}
}
