package com.theoryx.xseed.integration;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.sql.DataSource;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.Assert;

import com.theoryx.xseed.XseedWebApplication;
import com.theoryx.xseed.dto.StartupDTO;
import com.theoryx.xseed.dto.SurveyDTO;
import com.theoryx.xseed.dto.UrlLabelMapping;
import com.theoryx.xseed.dto.UserDTO;
import com.theoryx.xseed.model.Question;
import com.theoryx.xseed.model.Survey;
import com.theoryx.xseed.model.SurveyQuestion;
import com.theoryx.xseed.model.User;
import com.theoryx.xseed.service.SurveyService;
import com.theoryx.xseed.service.UserService;
import com.theoryx.xseed.utils.DateUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = XseedWebApplication.class)
@WebAppConfiguration
@ActiveProfiles("integration_test_h2")
public class SurveyControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private UserService userService;
	@Autowired
	private SurveyService serveyService;
	@Autowired
	private DataSource dataSource;
	@Autowired
	private SurveyService surveyService;
	@Autowired
	private MessageSource messageSource;
	
	private static Connection connection;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_1_create_db.sql")));
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_2_insert_data.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void tearDown() {
		try {
			connection = dataSource.getConnection();
			ScriptUtils.executeSqlScript(connection,
					new FileSystemResource(new File("../xseed-data/scripts/h2_3_drop_tables.sql")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test_getSurveysPage_user() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		try {
			mockMvc.perform(get("/surveys").
					requestAttr("breadCrumbs", breadcrumbs)
					.sessionAttr("currentUser", new UserDTO()))
				.andExpect(status().isOk())
				.andExpect(view().name("/surveys"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/surveys.jsp"))
				.andExpect(model().attributeExists("surveys"))
				.andExpect(model().attribute("surveys", hasSize(greaterThanOrEqualTo(1))))
				.andExpect(model().attribute("surveys", isA(List.class)))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attribute("surveys", hasSize(greaterThanOrEqualTo(1))));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveysPage_nouser() {
		try {
			mockMvc.perform(get("/surveys"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage() {
		List<UrlLabelMapping> breadcrumbs = new LinkedList<UrlLabelMapping>();
		String snapshotName = DateUtils.getCurrentDateAsName();
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.requestAttr("breadCrumbs", breadcrumbs)
					.param("id", "1")
					.sessionAttr("currentUser", new UserDTO()))
				.andExpect(status().isOk())
				.andExpect(view().name("/startsurvey"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/startsurvey.jsp"))
				.andExpect(model().attributeExists("breadcrumbs"))
				.andExpect(model().attributeExists("currentSurvey"))
				.andExpect(model().attribute("currentSurvey", hasProperty("name", is("Survey N1"))))
				.andExpect(model().attributeExists("surveyQuestionDtosPaged"))
				.andExpect(model().attributeExists("snapshotName"))
				.andExpect(model().attribute("snapshotName", is(snapshotName)))
				.andExpect(model().attributeExists("surveyQuestionsDTOSNumberOfPages"))
				.andDo(print())
				.andExpect(model().attribute("surveyQuestionsDTOSNumberOfPages", is(greaterThan(1))));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage_nouser() {
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.param("id", "1"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/login"))
				.andExpect(redirectedUrl("/login"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_getSurveyPage_noquestions() {
		Survey newSurvey = new Survey();
		newSurvey.setName("New Survey");
		newSurvey = surveyService.save(newSurvey);
		Assert.assertNotNull(surveyService.getById(newSurvey.getId()));
		List<SurveyDTO> surveys = surveyService.getAllSurveyDtos();
		String error = messageSource.getMessage("error.survey-no-questions", new Object[0], Locale.getDefault());
		try {
			mockMvc.perform(get("/startsurvey/survey")
					.param("id", newSurvey.getId().toString())
					.sessionAttr("currentUser", new UserDTO()))
				.andExpect(status().isOk())
				.andExpect(view().name("/surveys"))
				.andExpect(forwardedUrl("/WEB-INF/jsp/surveys.jsp"))
				.andExpect(model().attributeExists("errors"))
				.andExpect(model().attribute("errors", hasItem(hasProperty("message", equalTo(error)))))
				.andExpect(model().attributeExists("surveys"))
				.andExpect(model().attribute("surveys", hasSize(surveys.size())));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_submitSurvey_nonsurvey() {
		try {
			mockMvc.perform(post("/submit/survey")
					.param("id", "10"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/snapshots"))
				.andExpect(redirectedUrl("/snapshots"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_submitSurvey_missinginput() {
		String json = "{\"message\":\"Something went wrong , check if you filled all the questions or try again later.\",\"status\":\"error\"}";
		try {
			mockMvc.perform(post("/submit/survey")
					.param("id", "1"))
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/snapshots"))
				.andExpect(redirectedUrl("/snapshots"))
				.andExpect(flash().attributeExists("message"))
				.andDo(print())
				.andExpect(flash().attribute("message", isA(JSONObject.class)))
				.andExpect(flash().attribute("message", hasToString(json)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void test_submitSurvey_ok() {
		String json = "{\"message\":\"Snapshot was saved\",\"status\":\"success\"}";
		User user = userService.getByEmail("test1@abv.bg");
		UserDTO currentUser = userService.convertUserToUserDTO(user);
		StartupDTO currentStartup = currentUser.getStartup();
		Map<String, String> parameters = parameters(serveyService.getById(1));
		try {
			MockHttpServletRequestBuilder request = post("/submit/survey");
			for (Entry<String, String> entry : parameters.entrySet()) {
				request.param(entry.getKey(), entry.getValue());
			}
			request.param("id", "1")
			.param("snapshotName", "snapshotName")
			.sessionAttr("currentUser", currentUser)
			.sessionAttr("currentStartup", currentStartup);
			
			mockMvc.perform(request)
				.andDo(print())
				.andExpect(status().isFound())
				.andExpect(view().name("redirect:/snapshots"))
				.andExpect(redirectedUrl("/snapshots"))
				.andExpect(flash().attributeExists("message"))
				.andExpect(flash().attribute("message", isA(JSONObject.class)))
				.andExpect(flash().attribute("message", hasToString(json)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Map<String, String> parameters(Survey survey){
		Map<String, String> parameters = new HashMap<String, String>();
		for (SurveyQuestion surveyQuestion : survey.getQuestions()) {
			Question question = surveyQuestion.getQuestion();
			if(question.getCategory().getType().toString().equals("SINGLE_CHOICE")){
				String value = question.getAnswergroup().getOptions().get(0).getId().toString();
				String key = "radio_" + question.getId();
				parameters.put(key, value);
				System.out.println(key + " - " + value);
			}
			if(question.getCategory().getType().toString().equals("MULTIPLE_CHOICE")){
				String value = question.getAnswergroup().getOptions().get(0).getId().toString();
				String key = "multiple_" + question.getId() + "_" + value;
				parameters.put(key, value);
				System.out.println(key + " - " + value);
			}
			if(question.getCategory().getType().toString().equals("YES_NO")){
				String value = "TRUE";
				String key = "yesno_" + question.getId();
				parameters.put(key, value);
				System.out.println(key + " - " + value);
			}
			if(question.getCategory().getType().toString().equals("TEXT")){
				String value = "Text answer";
				String key = "text_" + question.getId();
				parameters.put(key, value);
				System.out.println(key + " - " + value);
			}
		}
		return parameters;
	}

}
