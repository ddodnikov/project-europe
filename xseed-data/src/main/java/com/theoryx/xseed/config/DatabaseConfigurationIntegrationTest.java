package com.theoryx.xseed.config;

import java.net.URISyntaxException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@Profile(value = {"integration_test"})
public class DatabaseConfigurationIntegrationTest {
	
	@Autowired
	private DataSource dataSource;
	
	/**
	 * DataSource definition for database connection.
	 * @throws URISyntaxException 
	 */
	@Bean
	public DataSource dataSource() throws URISyntaxException {
		String dbUrl = "jdbc:sqlite:../xseed-data/src/main/resources/xseed-test.sqlite";
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.sqlite.JDBC");
		dataSource.setUrl(dbUrl);
		return dataSource;
	}
	
	@Bean
	@Autowired
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setDataSource(dataSource);
		entityManagerFactory.setPackagesToScan("com.theoryx.xseed");
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
		entityManagerFactory.setJpaProperties(getHibernateAndSpringProperties());
		return entityManagerFactory;
	}
	
	private Properties getHibernateAndSpringProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.format_sql", "true");
		properties.put("hibernate.show_sql", "false");
		properties.put("hibernate.dialect", "com.theoryx.xseed.config.ArrayPostgreSQLDialect");
		properties.put("spring.jpa.hibernate.naming-strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
		properties.put("spring.jpa.properties.hibernate.dialect", "org.hibernate.dialect.SQLiteDialect");
		properties.put("hibernate.enable_lazy_load_no_trans", "true");
		return properties;
	}

}
