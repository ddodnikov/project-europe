package com.theoryx.xseed.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.theoryx.xseed.model.Startup;

@Repository
@Transactional
public interface StartupRepository extends PagingAndSortingRepository<Startup, Integer> {

	@Query(value = "SELECT * FROM startups ORDER BY id ASC LIMIT :limit OFFSET :offset", nativeQuery = true)
	public List<Startup> findPaginationStartups(@Param("limit") Integer limit, @Param("offset") Integer offset);

	public Startup findByName(String name);
	
}
