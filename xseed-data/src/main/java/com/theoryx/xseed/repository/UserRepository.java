package com.theoryx.xseed.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.theoryx.xseed.model.User;
import com.theoryx.xseed.model.UserRole;

@Repository
@Transactional
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
	
	public User findByEmail(String email);
	public List<User> findByName(String name);
	public List<User> findByIsActive(Boolean isActive);
	public List<User> findByRole(UserRole role);
	
	@Query(value = "SELECT * FROM users WHERE role LIKE 'USER' ORDER BY id ASC LIMIT :limit OFFSET :offset", nativeQuery = true)
	public List<User> findPaginationUsers(@Param("limit") Integer limit, @Param("offset") Integer offset);
	
	@Query(value = "SELECT COUNT(*) FROM users WHERE role LIKE 'USER'", nativeQuery = true)
	public Integer findUsersCount();
}
