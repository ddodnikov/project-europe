CREATE TABLE countries
(
  id integer NOT NULL AUTO_INCREMENT,
  name character varying(150) NOT NULL,
  CONSTRAINT countries_pkey PRIMARY KEY (id),
  CONSTRAINT uk_1pyiwrqimi3hnl3vtgsypj5r UNIQUE (name)
);

CREATE TABLE answer_groups
(
  id integer NOT NULL AUTO_INCREMENT,
  identifier character varying(10) NOT NULL,
  name character varying(150),
  type character varying(255),
  CONSTRAINT answer_groups_pkey PRIMARY KEY (id),
  CONSTRAINT uk_1b3hdnm5va7vn2qd730268d8m UNIQUE (identifier)
);

CREATE TABLE answer_options
(
  id integer NOT NULL AUTO_INCREMENT,
  identifier character varying(10) NOT NULL,
  text character varying(255) NOT NULL,
  group_id integer NOT NULL,
  CONSTRAINT answer_options_pkey PRIMARY KEY (id),
  CONSTRAINT fk_3no1ofpf0v4pai9eowjcqfle2 FOREIGN KEY (group_id)
      REFERENCES answer_groups (id),
  CONSTRAINT uk_gyo9mjlh5coopdnh89w865uwd UNIQUE (identifier)
);

CREATE TABLE question_categories
(
  id integer NOT NULL AUTO_INCREMENT,
  type character varying(255) NOT NULL,
  CONSTRAINT question_categories_pkey PRIMARY KEY (id)
);

CREATE TABLE phase_categories
(
  id integer NOT NULL AUTO_INCREMENT,
  name character varying(255) NOT NULL,
  CONSTRAINT phase_categories_pkey PRIMARY KEY (id),
  CONSTRAINT uk_jm1lh6th7btn3b46s7j5ch85f UNIQUE (name)
);

CREATE TABLE group_categories
(
  id integer NOT NULL AUTO_INCREMENT,
  name character varying(255) NOT NULL,
  phase_category_id integer NOT NULL,
  CONSTRAINT group_categories_pkey PRIMARY KEY (id),
  CONSTRAINT fk_qs0e7yrq3f6es1bndfkbys4x FOREIGN KEY (phase_category_id)
      REFERENCES phase_categories (id),
  CONSTRAINT uk_ecf4bcgxct8ury2b08drbjwpe UNIQUE (name)
);

CREATE TABLE questions
(
  id integer NOT NULL AUTO_INCREMENT,
  algo boolean,
  filter boolean,
  hasother boolean NOT NULL,
  kpi boolean,
  text character varying(255) NOT NULL,
  answer_group_id integer NOT NULL,
  question_category_id integer NOT NULL,
  CONSTRAINT questions_pkey PRIMARY KEY (id),
  CONSTRAINT fk_e8qrunndacp3iubtf8u16elah FOREIGN KEY (question_category_id)
      REFERENCES question_categories (id),
  CONSTRAINT fk_lfff31gcar80nuojpjvec6g5u FOREIGN KEY (answer_group_id)
      REFERENCES answer_groups (id)
);

CREATE TABLE users
(
  id integer NOT NULL AUTO_INCREMENT,
  email character varying(70) NOT NULL,
  hashed_password character varying(255) NOT NULL,
  is_active boolean NOT NULL,
  name character varying(70) NOT NULL,
  role character varying(255) NOT NULL,
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email)
);

CREATE TABLE startups
(
  id integer NOT NULL AUTO_INCREMENT,
  email character varying(70),
  name character varying(70) NOT NULL,
  phone character varying(70),
  vat character varying(70),
  website character varying(70),
  country_id integer,
  CONSTRAINT startups_pkey PRIMARY KEY (id),
  CONSTRAINT fk_d4ycm868f9t1edb838a1fove9 FOREIGN KEY (country_id)
      REFERENCES countries (id)
);

CREATE TABLE surveys
(
  id integer NOT NULL AUTO_INCREMENT,
  name character varying(70) NOT NULL,
  CONSTRAINT surveys_pkey PRIMARY KEY (id),
  CONSTRAINT uk_q0jexv1nxyllbus3sqjcxpr0h UNIQUE (name)
);

CREATE TABLE snapshots
(
  id integer NOT NULL AUTO_INCREMENT,
  date timestamp NOT NULL,
  name character varying(255) NOT NULL,
  startup_id integer,
  survey_id integer,
  user_id integer,
  CONSTRAINT snapshots_pkey PRIMARY KEY (id),
  CONSTRAINT fk_4ufaitm3n3qnn3wvdqw2klok3 FOREIGN KEY (survey_id)
      REFERENCES surveys (id),
  CONSTRAINT fk_7fug1davt7hroxnclxi1xh84f FOREIGN KEY (startup_id)
      REFERENCES startups (id),
  CONSTRAINT fk_rpg17rexjfvdc07apij1aw6qm FOREIGN KEY (user_id)
      REFERENCES users (id)
);

CREATE TABLE snapshotlines
(
  id integer NOT NULL AUTO_INCREMENT,
  multiple_answers_id array,
  text_response character varying(255),
  question_id integer,
  selected_answer_id integer,
  snapshot_id integer,
  CONSTRAINT snapshotlines_pkey PRIMARY KEY (id),
  CONSTRAINT fk_19g5kpndstor9wcvmme1as8yk FOREIGN KEY (selected_answer_id)
      REFERENCES answer_options (id),
  CONSTRAINT fk_dbj6choxg446nwn9spuxyhlnq FOREIGN KEY (question_id)
      REFERENCES questions (id),
  CONSTRAINT fk_qxnj5si6ywhlja7ela6kxm2og FOREIGN KEY (snapshot_id)
      REFERENCES snapshots (id)
);

CREATE TABLE survey_questions
(
  id integer NOT NULL AUTO_INCREMENT,
  pagenumber integer NOT NULL,
  position integer NOT NULL,
  question_id integer NOT NULL,
  survey_id integer NOT NULL,
  CONSTRAINT survey_questions_pkey PRIMARY KEY (id),
  CONSTRAINT fk_2dqpxn7bp92w6wskj9k15s4hw FOREIGN KEY (question_id)
      REFERENCES questions (id),
  CONSTRAINT fk_4gyaeisulytk8nmmh9s5uj5kr FOREIGN KEY (survey_id)
      REFERENCES surveys (id)
);

CREATE TABLE memberships
(
  id integer NOT NULL AUTO_INCREMENT,
  role character varying(255) NOT NULL,
  startup_id integer NOT NULL,
  user_id integer NOT NULL,
  is_default boolean NOT NULL,
  CONSTRAINT memberships_pkey PRIMARY KEY (id),
  CONSTRAINT fk_4wsdwx7qwcveuab1pcvrcq6ac FOREIGN KEY (startup_id)
      REFERENCES startups (id),
  CONSTRAINT fk_k5hwi5h5bmxwv13dw14bnfxvc FOREIGN KEY (user_id)
      REFERENCES users (id)
);

CREATE TABLE kpi_adjustments
(
  id integer NOT NULL AUTO_INCREMENT,
  value integer NOT NULL,
  answer_id integer NOT NULL,
  question_id integer NOT NULL,
  CONSTRAINT kpi_adjustments_pkey PRIMARY KEY (id),
  CONSTRAINT fk_dysjrgdlg7s9bkq6qa14jedv0 FOREIGN KEY (answer_id)
      REFERENCES answer_options (id),
  CONSTRAINT fk_t71gptt3ssaxbhyx0rknr957c FOREIGN KEY (question_id)
      REFERENCES questions (id)
);

CREATE TABLE question_group_category
(
  question_id integer NOT NULL,
  group_category_id integer NOT NULL,
  CONSTRAINT fk_9ha0caqyllo84ed7fusn2qq9u FOREIGN KEY (group_category_id)
      REFERENCES group_categories (id),
  CONSTRAINT fk_l3lnjufj7cerg1tgh42n4j3jo FOREIGN KEY (question_id)
      REFERENCES questions (id)
);

CREATE TABLE calculations
(
  id integer NOT NULL AUTO_INCREMENT,
  date timestamp NOT NULL,
  name character varying(255) NOT NULL,
  number_of_startups integer NOT NULL,
  t double precision,
  tknown double precision,
  tknown_interaction double precision,
  tknown_straight double precision,
  tunknown double precision,
  question_id integer NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT calculations_pkey PRIMARY KEY (id),
  CONSTRAINT fk_1m88bd6yf5wo282aja5yp65q8 FOREIGN KEY (user_id)
      REFERENCES users (id),
  CONSTRAINT fk_htaxj14niouch1km047dy6vdq FOREIGN KEY (question_id)
      REFERENCES questions (id)
);

CREATE TABLE group_calculations
(
  id integer NOT NULL AUTO_INCREMENT,
  formula_a double precision NOT NULL,
  formula_b double precision NOT NULL,
  formula_c double precision NOT NULL,
  formula_d double precision NOT NULL,
  formula_e double precision NOT NULL,
  formula_f double precision NOT NULL,
  number_of_startups integer NOT NULL,
  calculation_id integer NOT NULL,
  CONSTRAINT group_calculations_pkey PRIMARY KEY (id),
  CONSTRAINT fk_igsux1jhgsjeot9199kcyr6gd FOREIGN KEY (calculation_id)
      REFERENCES calculations (id)
);

CREATE TABLE question_calculations
(
  id integer NOT NULL AUTO_INCREMENT,
  average double precision,
  formula_a double precision NOT NULL,
  formula_b double precision NOT NULL,
  formula_c double precision NOT NULL,
  formula_d double precision NOT NULL,
  formula_e double precision NOT NULL,
  formula_f double precision NOT NULL,
  number_of_startups integer NOT NULL,
  specific double precision,
  answer_option_id integer NOT NULL,
  calculation_id integer NOT NULL,
  question_id integer NOT NULL,
  CONSTRAINT question_calculations_pkey PRIMARY KEY (id),
  CONSTRAINT fk_cng2vfcrt8kumhb08us8rdu42 FOREIGN KEY (question_id)
      REFERENCES questions (id),
  CONSTRAINT fk_ggnnmbqvd5jlymtjbet8qq2l3 FOREIGN KEY (calculation_id)
      REFERENCES calculations (id),
  CONSTRAINT fk_pdjqmhocjreu2djfy05o2whk6 FOREIGN KEY (answer_option_id)
      REFERENCES answer_options (id)
);

CREATE TABLE startup_calculations
(
  id integer NOT NULL AUTO_INCREMENT,
  formula_a double precision NOT NULL,
  formula_b double precision NOT NULL,
  formula_c double precision NOT NULL,
  formula_d double precision NOT NULL,
  kpi integer NOT NULL,
  calculation_id integer NOT NULL,
  snapshot_id integer NOT NULL,
  startup_id integer NOT NULL,
  CONSTRAINT startup_calculations_pkey PRIMARY KEY (id),
  CONSTRAINT fk_4d8qbsrtioodo3hef84nog34 FOREIGN KEY (calculation_id)
      REFERENCES calculations (id),
  CONSTRAINT fk_gxnf4r2l8kt5da3xl023w5xxu FOREIGN KEY (startup_id)
      REFERENCES startups (id),
  CONSTRAINT fk_hak4c6tfubf3wblsimb267xk2 FOREIGN KEY (snapshot_id)
      REFERENCES snapshots (id)
);

