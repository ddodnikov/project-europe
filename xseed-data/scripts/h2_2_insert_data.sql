INSERT INTO countries SELECT * FROM CSVREAD('../xseed-data/scripts/countries.csv', null, 'fieldSeparator=|');

INSERT INTO answer_groups SELECT * FROM CSVREAD('../xseed-data/scripts/answer_groups.csv', null, 'fieldSeparator=|');

INSERT INTO answer_options SELECT * FROM CSVREAD('../xseed-data/scripts/answer_options.csv', null, 'fieldSeparator=|');

INSERT INTO question_categories SELECT * FROM CSVREAD('../xseed-data/scripts/question_categories.csv', null, 'fieldSeparator=|');

INSERT INTO phase_categories SELECT * FROM CSVREAD('../xseed-data/scripts/phase_categories.csv', null, 'fieldSeparator=|');

INSERT INTO group_categories SELECT * FROM CSVREAD('../xseed-data/scripts/group_categories.csv', null, 'fieldSeparator=|');
	
INSERT INTO questions SELECT * FROM CSVREAD('../xseed-data/scripts/questions.csv', null, 'fieldSeparator=|');

INSERT INTO users SELECT * FROM CSVREAD('../xseed-data/scripts/users.csv', null, 'fieldSeparator=|');

INSERT INTO startups SELECT * FROM CSVREAD('../xseed-data/scripts/startups.csv', null, 'fieldSeparator=|');

INSERT INTO surveys SELECT * FROM CSVREAD('../xseed-data/scripts/surveys.csv', null, 'fieldSeparator=|');

INSERT INTO snapshots SELECT * FROM CSVREAD('../xseed-data/scripts/snapshots.csv', null, 'fieldSeparator=|');

INSERT INTO snapshotlines SELECT * FROM CSVREAD('../xseed-data/scripts/snapshotlines.csv', null, 'fieldSeparator=|');

INSERT INTO survey_questions SELECT * FROM CSVREAD('../xseed-data/scripts/survey_questions.csv', null, 'fieldSeparator=|');

INSERT INTO memberships SELECT * FROM CSVREAD('../xseed-data/scripts/memberships.csv', null, 'fieldSeparator=|');

INSERT INTO kpi_adjustments SELECT * FROM CSVREAD('../xseed-data/scripts/kpi_adjustments.csv', null, 'fieldSeparator=|');

INSERT INTO question_group_category SELECT * FROM CSVREAD('../xseed-data/scripts/question_group_category.csv', null, 'fieldSeparator=|');

INSERT INTO calculations SELECT * FROM CSVREAD('../xseed-data/scripts/calculations.csv', null, 'fieldSeparator=|');

INSERT INTO group_calculations SELECT * FROM CSVREAD('../xseed-data/scripts/group_calculations.csv', null, 'fieldSeparator=|');

INSERT INTO question_calculations SELECT * FROM CSVREAD('../xseed-data/scripts/question_calculations.csv', null, 'fieldSeparator=|');

INSERT INTO startup_calculations SELECT * FROM CSVREAD('../xseed-data/scripts/startup_calculations.csv', null, 'fieldSeparator=|');
