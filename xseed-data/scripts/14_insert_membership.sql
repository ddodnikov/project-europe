ALTER SEQUENCE memberships_id_seq RESTART WITH 1;
INSERT INTO memberships(role, startup_id, user_id, is_default) VALUES
('ADMIN', 
	(SELECT id FROM startups WHERE name LIKE 'test1_startup'),
	(SELECT id FROM users WHERE name LIKE 'test1_user'),
	TRUE
);